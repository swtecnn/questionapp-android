const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

exports.sendQuestion = functions.https.onCall((data, context) => {

	const questionText = data.questionText;
	//TODO: change this, number of the lecture/event should be fetched from the database
	const lectureNum = data.eventId;
	const token = data.token;

    const payload = {
        notification: {
            title: 'You have a new question!',
            body: `${questionText}`/*,
            icon: follower.photoURL*/
        }
    };

    const options = {
        priority: "high",
        timeToLive: 60*60*2
    };

    const topic = `lecture-${lectureNum}`;

    return admin.messaging().sendToDevice(token, payload, options);
    // return admin.messaging().sendToTopic(topic, payload, options);

    // response.results.forEach((result, index) => {
    //     const error = result.error;
    //     if (error) {
    //       console.error('Failure sending notification to', topic, error);
    //     }
    // }
});
