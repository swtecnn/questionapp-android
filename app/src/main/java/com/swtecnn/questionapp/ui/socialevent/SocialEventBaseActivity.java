package com.swtecnn.questionapp.ui.socialevent;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;

import com.swtecnn.questionapp.presentation.presenters.BasePresenter;
import com.swtecnn.questionapp.ui.base.BaseActivity;

import org.apache.commons.lang3.StringUtils;

import java.util.Set;

public class SocialEventBaseActivity extends BaseActivity {

    public static final String SOCIAL_EVENT_ID_EXTRA = "social_event_id";

    protected String mSocialEventId = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parseIntent();
    }

    @Override
    protected Set<BasePresenter> getPresenters() {
        return presenters;
    }

    protected void setScreenTitle(int resId) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(resId);
        }
    }

    private boolean parseIntent() {
        mSocialEventId = getIntent().getStringExtra(SOCIAL_EVENT_ID_EXTRA);
        return !StringUtils.isEmpty(mSocialEventId);
    }
}
