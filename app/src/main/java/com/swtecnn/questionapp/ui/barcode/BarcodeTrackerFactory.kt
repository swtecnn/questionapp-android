package com.swtecnn.questionapp.ui.barcode

import com.google.android.gms.vision.MultiProcessor
import com.google.android.gms.vision.Tracker
import com.google.android.gms.vision.barcode.Barcode
import com.swtecnn.questionapp.ui.barcode.camera.BarcodeGraphic
import com.swtecnn.questionapp.ui.barcode.camera.BarcodeGraphicTracker
import com.swtecnn.questionapp.ui.barcode.camera.GraphicOverlay

class BarcodeTrackerFactory(private val mGraphicOverlay: GraphicOverlay<BarcodeGraphic>,
                            private val mListener: BarcodeGraphicTracker.BarcodeUpdateListener) : MultiProcessor.Factory<Barcode> {

    override fun create(barcode: Barcode): Tracker<Barcode> {
        return BarcodeGraphicTracker(mGraphicOverlay, BarcodeGraphic(mGraphicOverlay), mListener)
    }

}