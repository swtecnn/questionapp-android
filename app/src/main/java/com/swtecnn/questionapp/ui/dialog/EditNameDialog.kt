package com.swtecnn.questionapp.ui.dialog

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.swtecnn.questionapp.R
import org.androidannotations.annotations.EFragment
import org.androidannotations.annotations.FragmentArg

@EFragment
open class EditNameDialog : DialogFragment() {

    private var mListener: SetNameListener? = null

    @FragmentArg
    lateinit var mExistingName: String

    lateinit var mNameView: EditText

    interface SetNameListener {
        fun onNameSet(name: String)
    }

    fun setEditNameListener(listener: SetNameListener) {
        mListener = listener
    }

    fun initViews(dialogView: View) {
        mNameView = dialogView.findViewById(R.id.editText)
        mNameView.setText(mExistingName)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!)
                .setTitle(getString(R.string.your_name))
                .setPositiveButton(android.R.string.ok
                ) { _, _ ->
                    mListener?.onNameSet(mNameView.text.toString())
                }
                .setNegativeButton(android.R.string.cancel
                ) { dialog, _ ->
                    dialog.dismiss()
                }

        val inflater = activity!!.layoutInflater

        val dialogView = inflater.inflate(R.layout.edit_name_dialog_layout, null)

        initViews(dialogView)

        builder.setView(dialogView)
        return builder.create()
    }
}