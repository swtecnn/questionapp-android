package com.swtecnn.questionapp.ui.utils;

import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.swtecnn.questionapp.R;
import com.swtecnn.questionapp.ui.base.BaseActivity;

public class ActivityUIBuilder {

    private BaseActivity mContext;
    private RelativeLayout mContextLayout;

    public ActivityUIBuilder(BaseActivity aActivity) {
        mContext = aActivity;
    }

    public ProgressBar createProgressBar() {
        ViewGroup rootView = mContext.findViewById(R.id.root_layout);

        ProgressBar progressBar = new ProgressBar(mContext, null, android.R.attr.progressBarStyle);
        progressBar.setIndeterminate(true);
        progressBar.setIndeterminateDrawable(mContext.getResources().getDrawable(R.drawable.progress_bar));
        progressBar.setPadding(0, 0, 0, (int) mContext.getResources().getDimension(R.dimen.activity_vertical_margin));

        final RelativeLayout.LayoutParams params = new
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        mContextLayout = new RelativeLayout(mContext);

        mContextLayout.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        mContextLayout.addView(progressBar);

        rootView.addView(mContextLayout, params);

        progressBar.setVisibility(View.GONE);
        return progressBar;
    }

    public RelativeLayout getContextLayout() {
        return mContextLayout;
    }
}
