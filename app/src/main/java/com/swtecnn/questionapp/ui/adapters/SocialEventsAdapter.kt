package com.swtecnn.questionapp.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.swtecnn.questionapp.R
import com.swtecnn.questionapp.domain.Role
import com.swtecnn.questionapp.domain.model.SocialEvent
import com.swtecnn.questionapp.ui.utils.DateFormatter

class SocialEventsAdapter : RecyclerView.Adapter<SocialEventsAdapter.SocialEventViewHolder>() {

    companion object {
        private const val VISIBLE_EVENTS_COUNT = 2
    }

    private var mSocialEvents: List<SocialEvent>? = null
    private lateinit var mCallback: Callback
    private var mShowRoleLabels: Boolean = false

    fun updateData(aSocialEvents: List<SocialEvent>) {
        mSocialEvents = aSocialEvents
        notifyDataSetChanged()
    }

    fun setCallback(aCallback: Callback) {
        mCallback = aCallback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SocialEventViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.social_event_list_item, parent, false)
        return SocialEventViewHolder(view)
    }

    override fun getItemCount(): Int {
        if (mSocialEvents == null) {
            return 0
        }
        return if (mSocialEvents!!.size > VISIBLE_EVENTS_COUNT)
            VISIBLE_EVENTS_COUNT else mSocialEvents!!.size
    }

    fun setShowRoleLabels(aShowRoleLabels: Boolean) {
        mShowRoleLabels = aShowRoleLabels
    }

    override fun onBindViewHolder(aHolder: SocialEventViewHolder, aPosition: Int) {
        aHolder.initViews(mSocialEvents!![aPosition])
    }

    inner class SocialEventViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        private var mLectureTitleView: TextView = mView.findViewById(R.id.lecture_header)
        private var mLectureDateView: TextView = mView.findViewById(R.id.lecture_date_label)
        private var mRoleView: TextView = mView.findViewById(R.id.role_label)

        fun initViews(aSocialEvent: SocialEvent) {
            itemView.setOnClickListener {
                mCallback.onSocialEventClicked(aSocialEvent.id)
            }
            mLectureTitleView.text = aSocialEvent.title
            mLectureDateView.text = DateFormatter.formatDateForSocialEvent(aSocialEvent.date)
            if (mShowRoleLabels) {
                mRoleView.visibility = View.VISIBLE
                when (Role.getRoleById(aSocialEvent.role)) {
                    Role.CLIENT -> mRoleView.text = itemView.context.getText(R.string.listener)
                    Role.ADMIN -> mRoleView.text = itemView.context.getText(R.string.lecturer)
                    else -> mRoleView.visibility = View.GONE
                }
            } else {
                mRoleView.visibility = View.GONE
            }
        }
    }

    interface Callback {
        fun onSocialEventClicked(aSocialEventId: String)
    }
}