package com.swtecnn.questionapp.ui.questions

import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.swtecnn.questionapp.R
import com.swtecnn.questionapp.domain.Role
import com.swtecnn.questionapp.domain.model.Question
import com.swtecnn.questionapp.presentation.presenters.BasePresenter
import com.swtecnn.questionapp.presentation.presenters.PresenterFactory
import com.swtecnn.questionapp.presentation.presenters.socialevent.question.SocialEventQuestionsPresenter
import com.swtecnn.questionapp.ui.adapters.QuestionsAdapter
import com.swtecnn.questionapp.ui.base.BaseActivity
import com.swtecnn.questionapp.ui.utils.MarginItemDecoration
import org.androidannotations.annotations.*

@EActivity(R.layout.activity_question_list)
open class QuestionListActivity : BaseActivity(),
        SocialEventQuestionsPresenter.View,
        QuestionsAdapter.Callback {

    @Extra
    lateinit var mSocialEventId: String

    @Extra
    lateinit var mRole: Role

    @ViewById(R.id.questions_list)
    lateinit var mQuestionsList: RecyclerView

    private lateinit var mSocialEventQuestionsPresenter: SocialEventQuestionsPresenter
    private lateinit var mQuestionsAdapter: QuestionsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.setTitle(R.string.questions_list_activity_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mSocialEventQuestionsPresenter = PresenterFactory.getSocialEventQuestionsPresenter(this, this)
    }

    @AfterViews
    fun onExtrasInjected() {
        mSocialEventQuestionsPresenter.setRole(mRole)
        mSocialEventQuestionsPresenter.fetchQuestions(mSocialEventId)

        mQuestionsAdapter = QuestionsAdapter(mRole)
        mQuestionsAdapter.setCallback(this)
        mQuestionsList.addItemDecoration(MarginItemDecoration(
                resources.getDimensionPixelSize(R.dimen.small_margin)))
        mQuestionsList.adapter = mQuestionsAdapter
        val linearLayoutManager = LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false)
        mQuestionsList.layoutManager = linearLayoutManager
    }

    override fun getPresenters(): MutableSet<BasePresenter> {
        presenters.add(mSocialEventQuestionsPresenter)
        return presenters
    }

    override fun onQuestionsDataReceived(aQuestions: List<Question>, aUserQuestions: Set<String>,
                                         aUserVotes: Set<String>) {
        mQuestionsAdapter.updateData(aQuestions, aUserQuestions, aUserVotes)
    }

    override fun onDeleteQuestion(aQuestion: Question) {
        mSocialEventQuestionsPresenter.deleteQuestion(aQuestion)
    }

    override fun onAnswerGiven(aQuestion: Question) {
        mSocialEventQuestionsPresenter.answerGiven(aQuestion)
    }

    override fun onWithdrawQuestion(aQuestion: Question) {
        mSocialEventQuestionsPresenter.withdrawQuestion(aQuestion)
    }

    override fun onVoteQuestion(aQuestion: Question) {
        mSocialEventQuestionsPresenter.voteQuestion(aQuestion)
    }

    override fun onUnVoteQuestion(aQuestion: Question) {
        mSocialEventQuestionsPresenter.unVoteQuestion(aQuestion)
    }

    override fun onFailedToDeleteQuestion() {
        Toast.makeText(this,
                resources.getText(R.string.failed_to_delete_question),
                Toast.LENGTH_LONG)
                .show()
    }

    override fun onFailedToVoteQuestion() {
        Toast.makeText(this,
                resources.getText(R.string.failed_to_vote_question),
                Toast.LENGTH_LONG)
                .show()
    }
}