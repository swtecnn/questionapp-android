package com.swtecnn.questionapp.ui;

import com.swtecnn.questionapp.presentation.services.ProgressHUDTrackingService;

public interface BaseView {
    void showProgress();
    void hideProgress();

    ProgressHUDTrackingService getProgressTrackingService();
    void showNoInternetAlert();
    boolean isNetworkAvailable();
    void showMessage(String message);
    void showMessage(Integer stringID);
}
