package com.swtecnn.questionapp.ui.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class NetworkAvailabilityManager {

    public boolean isNetworkAvailable(final Context aContext) {
        final ConnectivityManager cm =
                (ConnectivityManager) aContext.getSystemService(CONNECTIVITY_SERVICE);

        final NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}
