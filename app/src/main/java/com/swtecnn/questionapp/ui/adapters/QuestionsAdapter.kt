package com.swtecnn.questionapp.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.swtecnn.questionapp.R
import com.swtecnn.questionapp.domain.Role
import com.swtecnn.questionapp.domain.Role.*
import com.swtecnn.questionapp.domain.model.Question

class QuestionsAdapter(private val mRole: Role) : RecyclerView.Adapter<QuestionsAdapter.QuestionViewHolder>() {

    private var mQuestions: List<Question> = listOf()
    private lateinit var mUserQuestions: Set<String>
    private lateinit var mUserVotes: Set<String>
    private lateinit var mCallback: Callback

    fun updateData(aQuestions: List<Question>, aUserQuestions: Set<String>,
                   aUserVotes: Set<String>) {
        mQuestions = aQuestions
        mUserQuestions = aUserQuestions
        mUserVotes = aUserVotes
        notifyDataSetChanged()
    }

    fun setCallback(aCallback: Callback) {
        mCallback = aCallback
    }

    override fun getItemCount(): Int {
        return mQuestions.size
    }

    override fun onBindViewHolder(aHolder: QuestionViewHolder, aPosition: Int) {
        aHolder.initViews(mQuestions[aPosition])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuestionViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.question_list_item, parent, false)
        return QuestionViewHolder(view)
    }

    inner class QuestionViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        private var mNameView: TextView = mView.findViewById(R.id.listener_name)
        private var mQuestionBodyView: TextView = mView.findViewById(R.id.question_text)
        private var mLikeCountView: TextView = mView.findViewById(R.id.like_count)
        private var mLikeImgView: ImageView = mView.findViewById(R.id.like_img)
        private var mLecturerButtons: LinearLayout = mView.findViewById(R.id.lecturer_buttons)
        private val mVoteButton: Button = mView.findViewById(R.id.vote_btn)
        private val mWithdrawButton: Button = mView.findViewById(R.id.withdraw_btn)

        fun initViews(aQuestion: Question) {
            when (mRole) {
                VIEWER -> {
                    setViewerMode()
                }
                ADMIN -> {
                    mLecturerButtons.visibility = View.VISIBLE
                    mVoteButton.visibility = View.GONE
                    mWithdrawButton.visibility = View.GONE
                    val deleteBtn = mLecturerButtons.findViewById<Button>(R.id.delete_btn)
                    deleteBtn.setOnClickListener {
                        mCallback.onDeleteQuestion(aQuestion)
                    }
                    if (aQuestion.archived) {
                        setViewerMode()
                    } else {
                        val answerGivenBtn = mLecturerButtons.findViewById<Button>(R.id.has_answer_btn)
                        answerGivenBtn.setOnClickListener {
                            mCallback.onAnswerGiven(aQuestion)
                        }
                        answerGivenBtn.isEnabled = true
                    }
                }
                CLIENT -> {
                    val isOurVote = mUserVotes.contains(aQuestion.id)
                    val isOurQuestion = mUserQuestions.contains(aQuestion.id)
                    mLecturerButtons.visibility = View.GONE
                    mVoteButton.visibility = View.VISIBLE
                    if (isOurQuestion) {
                        mVoteButton.visibility = View.GONE
                        mWithdrawButton.visibility = View.VISIBLE
                    } else {
                        mVoteButton.visibility = View.VISIBLE
                        mWithdrawButton.visibility = View.GONE
                        if (isOurVote) {
                            mVoteButton.background = itemView.context
                                    .getDrawable(R.drawable.blue_button_background_disabled)
                            if (aQuestion.votes > 0) {
                                // just an additional check
                                mVoteButton.text = itemView.context.getString(R.string.unvote)
                                mVoteButton.setOnClickListener {
                                    mCallback.onUnVoteQuestion(aQuestion)
                                }
                            }
                        } else {
                            mVoteButton.text = itemView.context.getString(R.string.vote)
                            mVoteButton.background = itemView.context
                                    .getDrawable(R.drawable.blue_button_background_enabled)
                            mVoteButton.setOnClickListener {
                                mCallback.onVoteQuestion(aQuestion)
                            }
                        }
                    }
                    mWithdrawButton.setOnClickListener {
                        mCallback.onWithdrawQuestion(aQuestion)
                    }
                    if (aQuestion.archived) {
                        mVoteButton.visibility = View.GONE
                        mWithdrawButton.visibility = View.GONE
                    }
                }
            }
            mNameView.text = aQuestion.name
            mQuestionBodyView.text = aQuestion.description

            when {
                mUserVotes.contains(aQuestion.id) && mRole != VIEWER -> mLikeImgView.setImageResource(R.drawable.ic_like_active)
                aQuestion.votes > 0 -> mLikeImgView.setImageResource(R.drawable.ic_like_unlike)
                else -> mLikeImgView.setImageResource(R.drawable.ic_like_inactive)
            }
            mLikeCountView.text = aQuestion.votes.toString()
        }

        private fun setViewerMode() {
            mLecturerButtons.visibility = View.GONE
            mVoteButton.visibility = View.GONE
            mWithdrawButton.visibility = View.GONE
        }
    }

    interface Callback {
        fun onDeleteQuestion(aQuestion: Question)
        fun onAnswerGiven(aQuestion: Question)
        fun onWithdrawQuestion(aQuestion: Question)
        fun onVoteQuestion(aQuestion: Question)
        fun onUnVoteQuestion(aQuestion: Question)
    }
}