package com.swtecnn.questionapp.ui.socialevent;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.swtecnn.questionapp.R;
import com.swtecnn.questionapp.domain.Role;
import com.swtecnn.questionapp.domain.model.Question;
import com.swtecnn.questionapp.domain.model.SocialEvent;
import com.swtecnn.questionapp.domain.routers.socialevent.client.SocialEventClientRouter;
import com.swtecnn.questionapp.presentation.presenters.BasePresenter;
import com.swtecnn.questionapp.presentation.presenters.PresenterFactory;
import com.swtecnn.questionapp.presentation.presenters.socialevent.admin.SocialEventAdminPresenter;
import com.swtecnn.questionapp.ui.questions.QuestionListActivity_;
import com.swtecnn.questionapp.ui.raisehand.RaiseHandActivity_;

import org.jetbrains.annotations.NotNull;

import java.util.Set;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

import static com.swtecnn.questionapp.domain.routers.socialevent.client.SocialEventClientRouter.RAISE_HAND_REQUEST_CODE;

public class SocialEventAdminActivity extends SocialEventBaseActivity implements SocialEventAdminPresenter.View {

    private SocialEventAdminPresenter mSocialEventAdminPresenter;

    @BindView(R.id.titleOfSocialEventText)
    TextView mEventTitleTextView;

    @BindView(R.id.speakerName)
    TextView mSpeakerNameTextView;

    @BindView(R.id.endEventButton)
    Button mEndEventButton;

    @BindView(R.id.showAllQuestionsButton)
    Button mShowQuestionButton;

    @BindView(R.id.listenQuestionButton)
    Button mListenQuestionButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_social_event_admin);
        setScreenTitle(R.string.social_event_admin_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        mSocialEventAdminPresenter = PresenterFactory.Companion.getSocialEventAdminPresenter(this, this);
        mSocialEventAdminPresenter.getSocialEventInfo(mSocialEventId);
        mSocialEventAdminPresenter.saveSocialEvent(mSocialEventId);
    }

    @Override
    protected Set<BasePresenter> getPresenters() {
        presenters.add(mSocialEventAdminPresenter);
        return presenters;
    }

    @Override
    public void onSocialEventArchiveFailed() {
        Toast.makeText(this,
                getResources().getText(R.string.social_event_admin_event_end_error),
                Toast.LENGTH_LONG)
                .show();
    }

    @Override
    public void onSocialEventArchived() {
        mEndEventButton.setVisibility(View.INVISIBLE);
        Toast.makeText(this,
                getResources().getText(R.string.social_event_admin_event_ended),
                Toast.LENGTH_LONG)
                .show();
    }

    @Override
    protected void onActivityResult(int aRequestCode, int aResultCode, @Nullable Intent aData) {
        super.onActivityResult(aRequestCode, aResultCode, aData);
        switch (aRequestCode) {
            case RAISE_HAND_REQUEST_CODE:
                if (aResultCode == SocialEventClientRouter.RESULT_CODE_DELETE_RAISE_HAND_REQUEST && aData != null) {
                    mSocialEventAdminPresenter.deleteRaiseHandRequest(aData.getStringExtra(
                            SocialEventClientRouter.RAISE_HAND_REQUEST_ID_EXTRA));
                }
                break;
        }
    }

    @Override
    public void onSocialEventLoaded(@NotNull SocialEvent event) {
        mEventTitleTextView.setText(event.title);
        mSpeakerNameTextView.setText(event.lector);
    }

    @OnClick(R.id.eventInfoButton)
    public void onEventInfoClicked() {
        mSocialEventAdminPresenter.showLectureInfo(mSocialEventId);
    }

    @OnClick(R.id.endEventButton)
    public void onArchiveEventClicked() {
        mSocialEventAdminPresenter.archiveSocialEvent(mSocialEventId);
    }

    @OnClick(R.id.listenQuestionButton)
    public void onListenQuestionClicked() {
        mSocialEventAdminPresenter.onListenQuestionClicked();
    }

    @OnClick(R.id.showAllQuestionsButton)
    public void onShowAllQuestionClicked() {
        mSocialEventAdminPresenter.onShowAllQuestionClicked();
    }

    @SuppressLint("StringFormatMatches")
    @Override
    public void onQuestionCountChanged(int count) {
        mShowQuestionButton.setText(getResources()
                .getString(R.string.social_event_admin_questions, count));
    }

    @SuppressLint("StringFormatMatches")
    @Override
    public void onRaiseHandRequestsCountChanged(int count) {
        mListenQuestionButton.setText(getResources()
                .getString(R.string.social_event_admin_listen_questions, count));
    }

    @Override
    public void showAllQuestions(@NotNull String aSocialInfoId) {
        QuestionListActivity_.intent(this).mSocialEventId(aSocialInfoId).start();
    }

    @Override
    public void showNoQuestionsToListen() {
        new AlertDialog.Builder(this)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                .setTitle("Error!")
                .setMessage("No questions to listen")
                .show();
    }

    @Override
    public void showLecturerRaiseHand(@NotNull String aListenerName,
                                      @NotNull String aRaiseHandRequestId, long aTimestamp) {
        RaiseHandActivity_.intent(this)
                .mRole(Role.ADMIN)
                .mListenerName(aListenerName)
                .mRequestTimestamp(String.valueOf(aTimestamp))
                .mRaiseHandRequestId(aRaiseHandRequestId)
                .startForResult(RAISE_HAND_REQUEST_CODE);
    }
}
