package com.swtecnn.questionapp.ui.dashboard;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Parcelable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.barcode.Barcode;
import com.swtecnn.questionapp.R;
import com.swtecnn.questionapp.domain.Role;
import com.swtecnn.questionapp.domain.model.SocialEvent;
import com.swtecnn.questionapp.domain.repository.socialevent.SocialEventLocalRepositoryImpl;
import com.swtecnn.questionapp.domain.routers.authentication.AuthenticationRouter;
import com.swtecnn.questionapp.domain.routers.dashboard.DashboardRouter;
import com.swtecnn.questionapp.presentation.presenters.BasePresenter;
import com.swtecnn.questionapp.presentation.presenters.PresenterFactory;
import com.swtecnn.questionapp.presentation.presenters.authentication.AuthenticationPresenter;
import com.swtecnn.questionapp.presentation.presenters.dashboard.DashboardPresenter;
import com.swtecnn.questionapp.ui.adapters.SocialEventsAdapter;
import com.swtecnn.questionapp.ui.base.BaseActivity;
import com.swtecnn.questionapp.ui.dialog.EditNameDialog;
import com.swtecnn.questionapp.ui.dialog.EditNameDialog_;
import com.swtecnn.questionapp.ui.lections.LectionsActivity_;
import com.swtecnn.questionapp.ui.utils.MarginItemDecoration;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

@SuppressLint("Registered")
@EActivity(R.layout.activity_main)
public class DashboardActivity extends BaseActivity implements AuthenticationPresenter.View,
        DashboardPresenter.View, SocialEventsAdapter.Callback {

    @ViewById(R.id.edit_link)
    ImageView mEditUserNameButton;

    @ViewById(R.id.username_label)
    TextView mUserNameLabel;

    @ViewById(R.id.view_all_events_btn)
    TextView mViewAllButton;

    @ViewById(R.id.last_lections_label)
    TextView mLastLectionsLabel;

    @ViewById(R.id.last_lections_list)
    RecyclerView mLastLectionsList;

    private AuthenticationPresenter mAuthenticationPresenter;
    private DashboardPresenter mDashboardPresenter;

    private SocialEventsAdapter mSocialEventsAdapter;

    @AfterViews
    void init() {
        mAuthenticationPresenter = PresenterFactory.Companion.getAuthenticationPresenter(this, this);
        mDashboardPresenter = PresenterFactory.Companion.getDashboardPresenter(this, this);

        getSupportActionBar().setTitle(R.string.greeting);

        mSocialEventsAdapter = new SocialEventsAdapter();
        mSocialEventsAdapter.setCallback(this);
        mLastLectionsList.addItemDecoration(new MarginItemDecoration(
                getResources().getDimensionPixelSize(R.dimen.small_margin)));
        mLastLectionsList.setAdapter(mSocialEventsAdapter);
        mLastLectionsList.hasFixedSize();
        mLastLectionsList.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
    }

    @Override
    protected Set<BasePresenter> getPresenters() {
        presenters.add(mAuthenticationPresenter);
        presenters.add(mDashboardPresenter);
        return presenters;
    }

    @Click(R.id.scan_qr_button)
    public void scanQrClicked(View view) {
        mAuthenticationPresenter.scanQrCodeClicked();
    }

    @Click(R.id.view_all_events_btn)
    public void viewAllClicked(View view) {
        mDashboardPresenter.onViewAllUserEvents();
    }

    @Override
    protected void onActivityResult(int aRequestCode, int aResultCode, @Nullable Intent aData) {
        super.onActivityResult(aRequestCode, aResultCode, aData);
        switch (aRequestCode) {
            case AuthenticationRouter.BARCODE_SCANNER_REQUEST_CODE:
                if (aResultCode == Activity.RESULT_OK && aData != null) {
                    final Parcelable parcel = aData.getParcelableExtra("barcode");
                    if (parcel != null) {
                        mAuthenticationPresenter.onBarcode((Barcode) parcel);
                    }
                }
                break;
            case AuthenticationRouter.ENTER_PIN_CODE_REQUEST_CODE:
                if (aResultCode == Activity.RESULT_OK && aData != null) {
                    mAuthenticationPresenter.onPinCodeEntered(aData.getStringExtra("lecturer_pin"));
                }
                break;
            case DashboardRouter.ALL_LECTIONS_REQUEST_CODE:
                if (aResultCode == Activity.RESULT_OK && aData != null) {
                    mDashboardPresenter.onSocialEventSelected(aData.getStringExtra("social_event_id"));
                }
                break;
        }
    }

    @Click(R.id.edit_link)
    public void editNameClicked(View view) {
        mAuthenticationPresenter.onEditNameClicked();
    }

    @Override
    public void showEditNameDialog() {
        final EditNameDialog dialog = EditNameDialog_.builder()
                .mExistingName(mUserNameLabel.getText().toString())
                .build();
        dialog.setEditNameListener(aName ->
                mAuthenticationPresenter.saveUserName(aName, getResources().getString(R.string.incognito)));
        dialog.show(getSupportFragmentManager(), "edit_name_dialog");
    }

    @Override
    public void updateUserNameView(@NotNull String aUserName) {
        mUserNameLabel.setText(aUserName);
    }

    @Override
    public void onLecturerPinNotValid() {
        Toast.makeText(this, getString(R.string.pin_not_valid), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClientSocialEventsLoaded(@NotNull List<? extends SocialEvent> aSocialEvents) {
        mViewAllButton.setVisibility(View.VISIBLE);
        mLastLectionsLabel.setVisibility(View.VISIBLE);
        mLastLectionsList.setVisibility(View.VISIBLE);
        mSocialEventsAdapter.updateData(aSocialEvents);
    }

    @Override
    public void onSocialEventClicked(@NotNull String aSocialEventId) {
        mDashboardPresenter.onSocialEventSelected(aSocialEventId);
    }

    @Override
    public void onInvalidQrCodeScanned() {
        Toast.makeText(this, getString(R.string.invalid_qr), Toast.LENGTH_SHORT).show();
    }
}
