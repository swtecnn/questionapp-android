package com.swtecnn.questionapp.ui.base;

import android.content.IntentFilter;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.swtecnn.questionapp.R;
import com.swtecnn.questionapp.presentation.presenters.BasePresenter;
import com.swtecnn.questionapp.presentation.services.ProgressHUDTrackingService;
import com.swtecnn.questionapp.receiver.NetworkStateReceiver;
import com.swtecnn.questionapp.ui.BaseView;
import com.swtecnn.questionapp.ui.utils.ActivityKeyboardManager;
import com.swtecnn.questionapp.ui.utils.ActivityUIBuilder;
import com.swtecnn.questionapp.ui.utils.NetworkAvailabilityManager;

import java.util.HashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import timber.log.Timber;

public abstract class BaseActivity extends AppCompatActivity implements BaseView,
        ActivityKeyboardManager.View, NetworkStateReceiver.NetworkStateReceiverListener {

    private static final int PROGRESS_BAR_TIMEOUT_SEC = 20;

    public Set<BasePresenter> presenters;
    private Timer progressBarTimer;
    private ProgressBar progressBar;
    private RelativeLayout contextLayout;
    private ActivityUIBuilder activityUIBuilder;
    private ActivityKeyboardManager activityKeyboardManager;
    private NetworkStateReceiver mNetworkStateReceiver;
    private NetworkAvailabilityManager mNetworkAvailabilityManager;
    private ProgressHUDTrackingService mProgressTrackingService;

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityUIBuilder = new ActivityUIBuilder(this);

        activityKeyboardManager = new ActivityKeyboardManager(this);

        presenters = new HashSet<>();

        mNetworkAvailabilityManager = new NetworkAvailabilityManager();
        mProgressTrackingService = new ProgressHUDTrackingService();

        mNetworkStateReceiver = new NetworkStateReceiver();
        mNetworkStateReceiver.addListener(this);
        registerReceiver(mNetworkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        for (BasePresenter presenter : getPresenters()) {
            presenter.create();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        for (BasePresenter presenter : getPresenters()) {
            presenter.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        activityKeyboardManager.attachKeyboardListeners(this);

        for (BasePresenter presenter : getPresenters()) {
            presenter.resume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        for (BasePresenter presenter : getPresenters()) {
            presenter.pause();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        for (BasePresenter presenter : getPresenters()) {
            presenter.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        for (BasePresenter presenter : getPresenters()) {
            presenter.destroy();
        }

        mNetworkStateReceiver.removeListener(this);
        unregisterReceiver(mNetworkStateReceiver);
    }

    @Override
    public ProgressHUDTrackingService getProgressTrackingService() {
        return mProgressTrackingService;
    }

    @Override
    public void networkAvailable() {
        for (BasePresenter presenter : getPresenters()) {
            presenter.onNetworkAvailable();
        }
    }

    @Override
    public void networkUnavailable() {
        for (BasePresenter presenter : getPresenters()) {
            presenter.onNetworkUnavailable();
        }
    }

    protected abstract Set<BasePresenter> getPresenters();

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // ActivityKeyboardManager.View
    @Override
    public void onShowKeyboard() {

    }

    @Override
    public void onHideKeyboard() {

    }

    // BaseView
    @Override
    public void showProgress() {
        showProgressBar();
    }

    @Override
    public void hideProgress() {
        hideProgressBar();
    }

    @Override
    public void showMessage(String message) {
        hideProgress();
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean isNetworkAvailable() {
        return mNetworkAvailabilityManager.isNetworkAvailable(this);
    }

    @Override
    public void showNoInternetAlert() {
        showMessage(getString(R.string.no_connection_alert));
    }

    @Override
    public void showMessage(Integer stringId) {
        showMessage(getString(stringId));
    }

    // Protected
    protected void initProgressBar() {
        progressBar = activityUIBuilder.createProgressBar();
        contextLayout = activityUIBuilder.getContextLayout();
    }

    private void showProgressBar() {
        if (progressBar == null) {
            initProgressBar();
        }
        if (!progressBar.isShown()) {
            contextLayout.setBackground(getDrawable(R.color.dimmed_layout_color));
            progressBar.setVisibility(View.VISIBLE);
            contextLayout.setVisibility(View.VISIBLE);
            contextLayout.bringToFront();
            progressBar.bringToFront();
            scheduleProgressBarTimer();
        }
        enableUserInterface();
    }

    private void hideProgressBar() {
        if (progressBar == null) {
            initProgressBar();
        }
        progressBar.setVisibility(View.GONE);
        contextLayout.setVisibility(View.GONE);
        contextLayout.setBackground(null);
        disableUserInterface();
    }

    private void cancelProgressBarTimer() {
        if (progressBarTimer != null) {
            progressBarTimer.cancel();
            progressBarTimer = null;
        }
    }

    private void scheduleProgressBarTimer() {
        cancelProgressBarTimer();

        progressBarTimer = new Timer();
        progressBarTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(() -> {
                    Timber.w( "Hiding progress bar due to the timeout");
                    hideProgressBar();
                });
            }
        }, TimeUnit.SECONDS.toMillis(PROGRESS_BAR_TIMEOUT_SEC));
    }

    private void disableUserInterface() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void enableUserInterface() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}
