package com.swtecnn.questionapp.ui.pincode

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.view.inputmethod.InputMethodManager.SHOW_FORCED
import android.widget.EditText
import com.swtecnn.questionapp.R
import com.swtecnn.questionapp.presentation.presenters.BasePresenter
import com.swtecnn.questionapp.ui.base.BaseActivity
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.Click
import org.androidannotations.annotations.EActivity
import org.androidannotations.annotations.ViewById

@SuppressLint("Registered")
@EActivity(R.layout.activity_pincode)
open class EnterPinCodeActivity : BaseActivity() {

    @ViewById(R.id.enter_pin_input)
    lateinit var mPinInput: EditText

    @AfterViews
    fun init() {
        supportActionBar?.setTitle(R.string.confirm)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mPinInput.requestFocus()
        mPinInput.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                onConfirmClicked()
            }
            false
        }

        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(SHOW_FORCED, 0)
    }

    @Click(R.id.confirm_btn)
    fun onConfirmClicked() {
        val pinValue = mPinInput.text.toString()
        val result = Intent()
        result.putExtra("lecturer_pin", pinValue)
        setResult(Activity.RESULT_OK, result)
        finish()
    }

    override fun getPresenters(): Set<BasePresenter> {
        return presenters
    }
}