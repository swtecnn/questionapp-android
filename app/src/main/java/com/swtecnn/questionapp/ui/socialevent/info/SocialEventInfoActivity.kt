package com.swtecnn.questionapp.ui.socialevent.info

import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.swtecnn.questionapp.R
import com.swtecnn.questionapp.domain.Role
import com.swtecnn.questionapp.domain.model.SocialEvent
import com.swtecnn.questionapp.presentation.presenters.BasePresenter
import com.swtecnn.questionapp.presentation.presenters.PresenterFactory
import com.swtecnn.questionapp.presentation.presenters.socialevent.info.SocialEventInfoPresenter
import com.swtecnn.questionapp.ui.base.BaseActivity
import com.swtecnn.questionapp.ui.utils.DateFormatter
import org.androidannotations.annotations.*

@EActivity(R.layout.activity_social_event_info)
open class SocialEventInfoActivity : BaseActivity(), SocialEventInfoPresenter.View {

    @ViewById(R.id.lecture_name_field)
    lateinit var mLectureNameField: TextView

    @ViewById(R.id.date_field)
    lateinit var mDateField: TextView

    @ViewById(R.id.lecturer_field)
    lateinit var mLecturerNameField: TextView

    @ViewById(R.id.lecture_description_field)
    lateinit var mLectureDescriptionField: TextView

    @ViewById(R.id.agenda_field)
    lateinit var mAgendaField: TextView

    @ViewById(R.id.materials_field)
    lateinit var mMaterialsField: TextView

    @ViewById(R.id.fab_questions)
    lateinit var mQuestionButton: View

    @Extra
    lateinit var mSocialEventId: String

    private lateinit var mSocialEventInfoPresenter: SocialEventInfoPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mSocialEventInfoPresenter = PresenterFactory.getSocialEventInfoPresenter(this, this)
        presenters.add(mSocialEventInfoPresenter)
    }

    @AfterViews
    fun init() {
        mSocialEventInfoPresenter.getLectureInfo(mSocialEventId)
    }

    override fun onRoleReceived(aRole: Role) {
        mQuestionButton.visibility = if (aRole == Role.VIEWER) View.VISIBLE else View.GONE
    }

    override fun getPresenters(): MutableSet<BasePresenter> {
        return presenters
    }

    override fun onSocialEventReceived(aSocialEvent: SocialEvent) {
        supportActionBar?.title = aSocialEvent.title
        mLectureNameField.text = aSocialEvent.title
        mDateField.text = DateFormatter.formatDateForSocialEvent(aSocialEvent.date)
        mLecturerNameField.text = aSocialEvent.lector
        mLectureDescriptionField.text = aSocialEvent.description
        mAgendaField.text = aSocialEvent.agenda
        val builder = StringBuilder()
        aSocialEvent.usefulLinks.forEach {
            builder.append(it).append("\n")
        }
        mMaterialsField.text = builder.toString()
    }

    @Click(R.id.fab_questions)
    fun showQuestionsClicked() {
        mSocialEventInfoPresenter.showSocialEventQuestions(mSocialEventId)
    }

    override fun onFailedToGetSocialEvent() {
        //TODO: show a toast
    }
}