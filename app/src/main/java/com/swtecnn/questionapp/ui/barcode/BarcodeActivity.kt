package com.swtecnn.questionapp.ui.barcode

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.MultiProcessor
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import com.swtecnn.questionapp.R
import com.swtecnn.questionapp.presentation.presenters.BasePresenter
import com.swtecnn.questionapp.ui.barcode.camera.BarcodeGraphic
import com.swtecnn.questionapp.ui.barcode.camera.BarcodeGraphicTracker
import com.swtecnn.questionapp.ui.barcode.camera.CameraSourcePreview
import com.swtecnn.questionapp.ui.barcode.camera.GraphicOverlay
import com.swtecnn.questionapp.ui.base.BaseActivity
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.EActivity
import org.androidannotations.annotations.ViewById
import timber.log.Timber
import java.io.IOException

@SuppressLint("Registered")
@EActivity(R.layout.activity_barcode)
open class BarcodeActivity : BaseActivity(), BarcodeGraphicTracker.BarcodeUpdateListener {

    @ViewById(R.id.preview)
    lateinit var mPreview: CameraSourcePreview

    @ViewById(R.id.graphicOverlay)
    lateinit var mGraphicOverlay: GraphicOverlay<BarcodeGraphic>

    private var mCameraSource: CameraSource? = null

    // intent request code to handle updating play services if needed.
    private val RC_HANDLE_GMS = 9001
    // permission request codes need to be < 256
    private val RC_HANDLE_CAMERA_PERM = 2

    @AfterViews
    fun init() {
        val rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        if (rc == PackageManager.PERMISSION_GRANTED) {
            createCameraSource()
        } else {
            requestCameraPermission()
        }
    }

    override fun onResume() {
        super.onResume()
        startCameraSource()
    }

    /**
     * Stops the camera.
     */
    override fun onPause() {
        super.onPause()
        mPreview.stop()
    }

    /**
     * Releases the resources associated with the camera source, the associated detectors, and the
     * rest of the processing pipeline.
     */
    override fun onDestroy() {
        super.onDestroy()
        mPreview.release()
    }

    /**
     * Handles the requesting of the camera permission.  This includes
     * showing a "Snackbar" message of why the permission is needed then
     * sending the request.
     */
    private fun requestCameraPermission() {
        Timber.w("Camera permission is not granted. Requesting permission")

        val permissions = arrayOf(Manifest.permission.CAMERA)

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM)
            return
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            Timber.w("Got unexpected permission result: $requestCode")
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            return
        }

        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Timber.i("Camera permission granted - initialize the camera source")
            createCameraSource()
            return
        }

        Timber.w("Permission not granted: results len = ${grantResults.size} " +
                "Result code = %s", if (grantResults.isNotEmpty()) grantResults[0] else "(empty)")

        val listener = DialogInterface.OnClickListener { _, _ -> finish() }

        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.app_name)
                .setMessage("Camera was not allowed")
                .setPositiveButton(android.R.string.ok, listener)
                .show()
    }

    override fun getPresenters(): Set<BasePresenter> {
        return presenters
    }

    private fun createCameraSource() {
        val barcodeDetector = BarcodeDetector.Builder(applicationContext)
                .setBarcodeFormats(Barcode.QR_CODE)
                .build()
        val barcodeFactory = BarcodeTrackerFactory(mGraphicOverlay, this)
        barcodeDetector.setProcessor(MultiProcessor.Builder(barcodeFactory).build())

        if (!barcodeDetector.isOperational) {
            Timber.w("Detector dependencies are not yet available.")
        }

        mCameraSource = CameraSource.Builder(applicationContext, barcodeDetector)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(1600, 1300)
                .setRequestedFps(60.0f)
                .setAutoFocusEnabled(true)
                .build()
    }

    /**
     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    @Throws(SecurityException::class)
    private fun startCameraSource() {
        // check that the device has play services available.
        val code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                applicationContext)
        if (code != ConnectionResult.SUCCESS) {
            val dlg = GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS)
            dlg.show()
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay)
            } catch (e: IOException) {
                Timber.e(e, "Unable to start camera source.")
                mCameraSource?.release()
                mCameraSource = null
            }

        }
    }

    override fun onBarcodeDetected(barcode: Barcode?) {
        val result = Intent()
        result.putExtra("barcode", barcode)
        setResult(Activity.RESULT_OK, result)
        finish()
    }
}