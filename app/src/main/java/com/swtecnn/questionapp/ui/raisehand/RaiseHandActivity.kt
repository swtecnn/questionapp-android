package com.swtecnn.questionapp.ui.raisehand

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.swtecnn.questionapp.R
import com.swtecnn.questionapp.domain.Role
import com.swtecnn.questionapp.domain.model.RaiseHandRequestStatus
import com.swtecnn.questionapp.domain.routers.socialevent.client.SocialEventClientRouter
import com.swtecnn.questionapp.presentation.presenters.BasePresenter
import com.swtecnn.questionapp.presentation.presenters.PresenterFactory
import com.swtecnn.questionapp.presentation.presenters.raisehand.RaiseHandPresenter
import com.swtecnn.questionapp.ui.base.BaseActivity
import com.swtecnn.questionapp.ui.utils.DateFormatter
import org.androidannotations.annotations.*

@SuppressLint("Registered")
@EActivity(R.layout.activity_raise_hand)
open class RaiseHandActivity : BaseActivity(), RaiseHandPresenter.View {

    @ViewById(R.id.queue_number)
    lateinit var mQueueNumberText: TextView

    @ViewById(R.id.raise_hand_action_btn)
    lateinit var mActionButton: Button

    @ViewById(R.id.raise_hand_prompt_label)
    lateinit var mHeaderPromptText: TextView

    @ViewById(R.id.queue_number_label)
    lateinit var mMiddleLabel: TextView

    @ViewById(R.id.raise_hand_refresh_btn)
    lateinit var mRefreshButton: Button

    @Extra
    lateinit var mQueueNumber: String

    @Extra
    lateinit var mListenerName: String

    @Extra
    lateinit var mRequestTimestamp: String

    @Extra
    lateinit var mRole: Role

    @Extra
    lateinit var mRaiseHandRequestId: String

    @Extra
    lateinit var mRaiseHandStatus: RaiseHandRequestStatus.State

    private lateinit var mPresenter: RaiseHandPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.setTitle(R.string.raise_hand_activity_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mPresenter = PresenterFactory.getRaiseHandPresenter(this, this)
    }

    @AfterViews
    fun init() {
        when (mRole) {
            Role.CLIENT -> asClient()
            Role.ADMIN -> asLecturer()
        }
    }

    @Click(R.id.raise_hand_refresh_btn)
    fun onRefreshClicked() {
        mPresenter.refreshStatus(mRaiseHandRequestId)
    }

    override fun onRaiseHandStatusRefreshed(aRaiseHandStatus: RaiseHandRequestStatus) {
        onRaiseHandStatus(aRaiseHandStatus.status)
    }

    private fun onRaiseHandStatus(aRaiseHandStatus: RaiseHandRequestStatus.State) {
        if (aRaiseHandStatus == RaiseHandRequestStatus.State.ACTIVE) {
            mHeaderPromptText.text = getString(R.string.raise_hand_active_status)
        }
    }

    private fun asClient() {
        mQueueNumberText.text = mQueueNumber
        mActionButton.text = getString(R.string.lower_hand)
        mMiddleLabel.text = getString(R.string.queue_number)
        mHeaderPromptText.text = getString(R.string.raise_hand_prompt)
        mRefreshButton.visibility = View.VISIBLE

        onRaiseHandStatus(mRaiseHandStatus)
    }

    private fun asLecturer() {
        mQueueNumberText.text = mListenerName
        mActionButton.text = getString(R.string.has_answer)
        mMiddleLabel.text = getString(R.string.listener_capital)
        mHeaderPromptText.text = DateFormatter.formatDateForSocialEvent(mRequestTimestamp.toLong())
        mRefreshButton.visibility = View.GONE
    }

    override fun getPresenters(): MutableSet<BasePresenter> {
        presenters.add(mPresenter)
        return presenters
    }

    @Click(R.id.raise_hand_action_btn)
    fun onLowerHandClicked() {
        setResult(SocialEventClientRouter.RESULT_CODE_DELETE_RAISE_HAND_REQUEST,
                Intent().putExtra(SocialEventClientRouter.RAISE_HAND_REQUEST_ID_EXTRA, mRaiseHandRequestId))
        finish()
    }
}