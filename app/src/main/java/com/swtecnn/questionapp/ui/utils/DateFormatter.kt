package com.swtecnn.questionapp.ui.utils

import android.text.format.DateFormat
import java.util.*

object DateFormatter {

    private const val DATE_FORMAT = "dd.MM.yyyy HH:mm:ss"

    fun formatDateForSocialEvent(aDate: Long): String {
        return DateFormat.format(DATE_FORMAT, Date(aDate)).toString()
    }
}