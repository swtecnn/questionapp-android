package com.swtecnn.questionapp.ui.lections

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.swtecnn.questionapp.R
import com.swtecnn.questionapp.domain.model.SocialEvent
import com.swtecnn.questionapp.presentation.presenters.BasePresenter
import com.swtecnn.questionapp.ui.adapters.SocialEventsAdapter
import com.swtecnn.questionapp.ui.base.BaseActivity
import com.swtecnn.questionapp.ui.utils.MarginItemDecoration
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.EActivity
import org.androidannotations.annotations.Extra
import org.androidannotations.annotations.ViewById

@SuppressLint("Registered")
@EActivity(R.layout.activity_social_events)
open class LectionsActivity: BaseActivity(), SocialEventsAdapter.Callback {

    @ViewById(R.id.social_events_list)
    lateinit var mLectionsList: RecyclerView

    @Extra
    lateinit var mSocialEvents: ArrayList<SocialEvent>

    private lateinit var mLectionsAdapter: SocialEventsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.setTitle(R.string.lections_activity_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    @AfterViews
    fun init() {
        mLectionsAdapter = SocialEventsAdapter()
        mLectionsAdapter.setCallback(this)
        mLectionsAdapter.setShowRoleLabels(true)
        mLectionsList.addItemDecoration(MarginItemDecoration(
                resources.getDimensionPixelSize(R.dimen.small_margin)))
        mLectionsList.hasFixedSize()
        mLectionsList.layoutManager = LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false)
        mLectionsList.adapter = mLectionsAdapter

        mLectionsAdapter.updateData(mSocialEvents)
    }

    override fun getPresenters(): MutableSet<BasePresenter> {
        return presenters
    }

    override fun onSocialEventClicked(aSocialEventId: String) {
        val intent = Intent()
        intent.putExtra("social_event_id", aSocialEventId)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}