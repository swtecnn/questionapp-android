package com.swtecnn.questionapp.ui.askquestion

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils.isEmpty
import android.widget.EditText
import android.widget.Toast
import com.swtecnn.questionapp.R
import com.swtecnn.questionapp.domain.model.Question
import com.swtecnn.questionapp.domain.routers.socialevent.client.SocialEventClientRouter
import com.swtecnn.questionapp.presentation.presenters.BasePresenter
import com.swtecnn.questionapp.presentation.presenters.socialevent.client.AskQuestionPresenter
import com.swtecnn.questionapp.presentation.presenters.socialevent.client.AskQuestionPresenterImpl
import com.swtecnn.questionapp.ui.base.BaseActivity
import org.androidannotations.annotations.*

@EActivity(R.layout.activity_askquestion)
open class AskQuestionActivity : BaseActivity(), AskQuestionPresenter.View {

    @Extra
    lateinit var mExistingName: String

    @Extra
    lateinit var mSocialEventId: String

    private lateinit var mAskQuestionPresenter: AskQuestionPresenterImpl

    @ViewById(R.id.name_input)
    lateinit var mNameInput: EditText

    @ViewById(R.id.slide_number_input)
    lateinit var mSlideNumberInput: EditText

    @ViewById(R.id.question_input)
    lateinit var mQuestionTextInput: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mAskQuestionPresenter = AskQuestionPresenterImpl(this, resources)

        supportActionBar?.setTitle(R.string.ask_a_question_activity_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onBackPressed() {
        setResult(SocialEventClientRouter.RESULT_CODE_WITHOUT_RESULT)
        super.onBackPressed()
    }

    @AfterViews
    fun init() {
        mNameInput.setText(mExistingName)
    }

    @Click(R.id.ask_question_btn)
    fun onAskQuestionClicked() {
        val userName = mNameInput.text.trim().toString()
        val slideNumStr = mSlideNumberInput.text.trim().toString()
        val slideNumber = if (isEmpty(slideNumStr)) 0 else slideNumStr.toInt()
        val questionText = mQuestionTextInput.text.trim().toString()
        mAskQuestionPresenter.onAskQuestionClicked(Question(mSocialEventId, questionText,
                userName, slideNumber))
    }

    override fun getPresenters(): Set<BasePresenter> {
        return presenters //Should it be the base activity if we don't have appropriate presenter?
    }

    override fun showToastFillTheDescription() {
        Toast.makeText(this,
                resources.getText(R.string.social_event_client_description_toast),
                Toast.LENGTH_LONG)
                .show()
    }

    override fun onCorrectData(question: Question?) {
        val intent: Intent = Intent().putExtra(SocialEventClientRouter.QUESTION_EXTRA, question)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}