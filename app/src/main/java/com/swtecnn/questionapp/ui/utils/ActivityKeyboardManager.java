package com.swtecnn.questionapp.ui.utils;


import android.content.Context;
import android.graphics.Rect;
import android.util.Log;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;

import com.swtecnn.questionapp.R;
import com.swtecnn.questionapp.ui.base.BaseActivity;

import timber.log.Timber;

public class ActivityKeyboardManager {

    public interface View {
        void onShowKeyboard();
        void onHideKeyboard();
    }

    private static final String TAG = ActivityKeyboardManager.class.getName();

    private View mView;

    public ActivityKeyboardManager(View aView) {
        mView = aView;
    }

    private boolean keyboardListenersAttached = false;
    private ViewGroup rootLayout;

    public void attachKeyboardListeners(BaseActivity aContext) {
        if (keyboardListenersAttached) {
            return;
        }

        rootLayout = aContext.findViewById(R.id.root_layout);
        if (rootLayout != null) {
            rootLayout.getViewTreeObserver().addOnGlobalLayoutListener(keyboardLayoutListener);
            keyboardListenersAttached = true;
        }
    }

    public void hideKeyboard(BaseActivity aContext) {
        if (aContext.getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) aContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(aContext.getCurrentFocus().getWindowToken(), 0);
            }
        }
    }

    private ViewTreeObserver.OnGlobalLayoutListener keyboardLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {

        private final int KEYBOARD_HEIGHT_DIFFERENCE = 250;

        @Override
        public void onGlobalLayout() {
            Rect r = new Rect();
            rootLayout.getWindowVisibleDisplayFrame(r);
            if (rootLayout.getRootView().getHeight() - (r.bottom - r.top) > KEYBOARD_HEIGHT_DIFFERENCE) {
                mView.onShowKeyboard();
                Timber.d("Software Keyboard is shown");
            } else {
                mView.onHideKeyboard();
                Timber.d("Software Keyboard is not shown");
            }
        }
    };
}
