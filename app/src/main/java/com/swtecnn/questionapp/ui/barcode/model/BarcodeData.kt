package com.swtecnn.questionapp.ui.barcode.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class BarcodeData(@SerialName("socialEventId") val eventId: String, @SerialName("passwordHash") val password: String = "")