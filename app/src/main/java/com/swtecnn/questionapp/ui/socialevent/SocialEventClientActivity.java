package com.swtecnn.questionapp.ui.socialevent;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.swtecnn.questionapp.R;
import com.swtecnn.questionapp.domain.model.Question;
import com.swtecnn.questionapp.domain.model.SocialEvent;
import com.swtecnn.questionapp.domain.routers.socialevent.client.SocialEventClientRouter;
import com.swtecnn.questionapp.presentation.presenters.PresenterFactory;
import com.swtecnn.questionapp.presentation.presenters.socialevent.client.SocialEventClientPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class SocialEventClientActivity extends SocialEventBaseActivity implements SocialEventClientPresenter.View{

    private SocialEventClientPresenter mSocialEventClientPresenter;

    @BindView(R.id.titleOfSocialEventText)
    TextView mEventTitleTextView;

    @BindView(R.id.speakerName)
    TextView mSpeakerNameTextView;

    @Override
    public void onCreate(@Nullable Bundle aSavedInstanceState) {
        super.onCreate(aSavedInstanceState);
        setContentView(R.layout.activity_social_event_client);
        ButterKnife.bind(this);
        setScreenTitle(R.string.social_event_client_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mSocialEventClientPresenter = PresenterFactory.Companion.getSocialEventClientPresenter(this,  this);
        mSocialEventClientPresenter.getSocialEventInfo(mSocialEventId);
        mSocialEventClientPresenter.saveSocialEvent(mSocialEventId);
        presenters.add(mSocialEventClientPresenter);
    }

    @Override
    public void onQuestionPosted() {
        Toast.makeText(this,
                getResources().getText(R.string.social_event_client_question_posted),
                Toast.LENGTH_LONG)
                .show();
    }

    @Override
    public void onQuestionPostFailed() {
        Toast.makeText(this,
                getResources().getText(R.string.social_event_client_question_post_failed),
                Toast.LENGTH_LONG)
                .show();
    }

    @OnClick(R.id.askQuestionButton)
    public void onAskQuestionButtonClicked() {
        mSocialEventClientPresenter.onAskQuestionClicked();
    }

    @OnClick(R.id.raiseHandButton)
    public void onRaiseHand() {
        mSocialEventClientPresenter.onRaiseHandClicked();
    }

    @OnClick(R.id.eventInfoButton)
    public void onEventInfoClicked() {
        mSocialEventClientPresenter.onEventInfoClicked();
    }

    @OnClick(R.id.showAllQuestionsButton)
    public void onShowAllQuestionClicked() {
        mSocialEventClientPresenter.onShowAllQuestionClicked();
    }

    @Override
    public void onSocialEventLoaded(@NonNull SocialEvent aEvent) {
        mEventTitleTextView.setText(aEvent.title);
        mSpeakerNameTextView.setText(aEvent.lector);
    }

    @Override
    protected void onActivityResult(int aRequestCode, int aResultCode, @Nullable Intent aData) {
        super.onActivityResult(aRequestCode, aResultCode, aData);
        switch (aRequestCode) {
            case SocialEventClientRouter.ASK_QUESTION_REQUEST_CODE:
                if (aResultCode == Activity.RESULT_OK) {
                    Timber.d("Returned from AskQuestionActivity");
                    if (aData != null) {
                        final Question question = (Question) aData.getSerializableExtra(SocialEventClientRouter.QUESTION_EXTRA);
                        mSocialEventClientPresenter.postQuestion(question);
                    }
                }
                break;
            case SocialEventClientRouter.RAISE_HAND_REQUEST_CODE:
                if (aResultCode == SocialEventClientRouter.RESULT_CODE_DELETE_RAISE_HAND_REQUEST && aData != null) {
                    Timber.d("Returned from RaiseHandActivity: request should be removed");
                    mSocialEventClientPresenter.deleteRaiseHandRequest(aData.getStringExtra(SocialEventClientRouter.RAISE_HAND_REQUEST_ID_EXTRA));
                }
                break;
        }
    }

    @Override
    public void onRaiseHandRequestDeleted() {
        Toast.makeText(this,
                getResources().getText(R.string.social_event_client_raise_hand_deleted),
                Toast.LENGTH_LONG)
                .show();
    }

    @Override
    public void onRaiseHandRequestDeleteError() {
        Toast.makeText(this,
                getResources().getText(R.string.social_event_client_raise_hand_delete_error),
                Toast.LENGTH_LONG)
                .show();
    }
}
