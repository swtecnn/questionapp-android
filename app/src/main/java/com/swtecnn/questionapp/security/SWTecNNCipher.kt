package com.swtecnn.questionapp.security

import timber.log.Timber
import java.io.UnsupportedEncodingException
import java.security.GeneralSecurityException
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

class SWTecNNCipher : SecureCodeCipher {

    private val KEY = "812d3dc807cfd8ec"
    private val ALGORITHM = "AES"
    private val ALGORITHM_BLOCK_OFFSET = "AES/CBC/PKCS5Padding"

    override fun validate(aCode: String, aHash: String, aSocialEventPass: String): Boolean {
        val result = encrypt(aCode, aHash)
        return result == aSocialEventPass
    }

    private fun encrypt(aCode: String, aPasswordHash: String): String {
        return try {
            val cipher = Cipher.getInstance(ALGORITHM_BLOCK_OFFSET)
            val key = SecretKeySpec(KEY.toByteArray(Charsets.UTF_8), ALGORITHM)

            cipher.init(Cipher.ENCRYPT_MODE, key, IvParameterSpec(hexStringToByteArray(aPasswordHash)))
            val bytes = cipher.doFinal(aCode.toByteArray(Charsets.UTF_8))
            bytes.toHexString()
        } catch (e: GeneralSecurityException) {
            Timber.e(e,"Error decrypt Secure Code: length secure code")
            ""
        } catch (e: UnsupportedEncodingException) {
            Timber.e(e,"The Character Encoding is not supported")
            ""
        }
    }

    @ExperimentalUnsignedTypes
    fun ByteArray.toHexString() = asUByteArray().joinToString("") { it.toString(16).padStart(2, '0') }

    private fun hexStringToByteArray(s: String): ByteArray {
        val length = s.length
        val data = ByteArray(length / 2)

        var i = 0
        while (i < length) {
            data[i / 2] = ((Character.digit(s[i], 16) shl 4) + Character.digit(s[i + 1], 16)).toByte()
            i += 2
        }

        return data
    }
}