package com.swtecnn.questionapp.security

interface SecureCodeCipher {
    fun validate(aCode: String, aHash: String, aSocialEventPass: String): Boolean
}