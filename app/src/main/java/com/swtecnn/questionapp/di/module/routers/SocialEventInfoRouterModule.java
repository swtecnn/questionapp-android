package com.swtecnn.questionapp.di.module.routers;

import android.content.Context;

import com.swtecnn.questionapp.di.qualifier.ActivityContext;
import com.swtecnn.questionapp.di.scope.SocialEventInfoScope;
import com.swtecnn.questionapp.di.scope.SocialEventQuestionsScope;
import com.swtecnn.questionapp.domain.routers.socialevent.info.SocialEventInfoRouter;
import com.swtecnn.questionapp.domain.routers.socialevent.info.SocialEventInfoRouterImpl;
import com.swtecnn.questionapp.domain.routers.socialevent.question.SocialEventQuestionsRouter;
import com.swtecnn.questionapp.domain.routers.socialevent.question.SocialEventQuestionsRouterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class SocialEventInfoRouterModule {
    @Provides
    @SocialEventInfoScope
    SocialEventInfoRouter provideRouter(@ActivityContext Context context) {
        return new SocialEventInfoRouterImpl(context);
    }
}
