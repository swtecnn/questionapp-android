package com.swtecnn.questionapp.di.module.presenters;

import com.swtecnn.questionapp.di.scope.SocialEventInfoScope;
import com.swtecnn.questionapp.domain.interactors.socialevent.info.SocialEventInfoInteractor;
import com.swtecnn.questionapp.domain.routers.socialevent.info.SocialEventInfoRouter;
import com.swtecnn.questionapp.presentation.presenters.socialevent.info.SocialEventInfoPresenter;
import com.swtecnn.questionapp.presentation.presenters.socialevent.info.SocialEventInfoPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class SocialEventInfoPresenterModule {
    @Provides
    @SocialEventInfoScope
    public SocialEventInfoPresenter provideSocialEventInfoPresenter(SocialEventInfoPresenter.View aView,
                                                                    SocialEventInfoRouter aRouter,
                                                                    SocialEventInfoInteractor aSocialEventInfoInteractor) {
        return new SocialEventInfoPresenterImpl(aView, aRouter, aSocialEventInfoInteractor);
    }
}
