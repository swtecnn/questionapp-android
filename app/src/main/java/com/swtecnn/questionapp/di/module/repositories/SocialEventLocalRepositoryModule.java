package com.swtecnn.questionapp.di.module.repositories;

import android.content.SharedPreferences;

import com.swtecnn.questionapp.domain.repository.socialevent.ISocialEventLocalRepository;
import com.swtecnn.questionapp.domain.repository.socialevent.SocialEventLocalRepositoryImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class SocialEventLocalRepositoryModule {
    @Provides
    ISocialEventLocalRepository provideSocialEventLocalRepository(SharedPreferences aPreferences) {
        return new SocialEventLocalRepositoryImpl(aPreferences);
    }
}
