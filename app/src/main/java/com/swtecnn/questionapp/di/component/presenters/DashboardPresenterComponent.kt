package com.swtecnn.questionapp.di.component.presenters

import android.content.Context
import com.swtecnn.questionapp.di.module.interactors.DashboardInteractorModule
import com.swtecnn.questionapp.di.module.presenters.DashboardPresenterModule
import com.swtecnn.questionapp.di.module.routers.DashboardRouterModule
import com.swtecnn.questionapp.di.qualifier.ActivityContext
import com.swtecnn.questionapp.di.scope.DashboardScope
import com.swtecnn.questionapp.presentation.presenters.dashboard.DashboardPresenter
import dagger.BindsInstance
import dagger.Subcomponent

@Subcomponent(modules = [DashboardInteractorModule::class,
    DashboardRouterModule::class,
    DashboardPresenterModule::class])
@DashboardScope
interface DashboardPresenterComponent {
    val presenter: DashboardPresenter

    @Subcomponent.Builder
    interface Builder {
        fun build(): DashboardPresenterComponent

        @BindsInstance
        fun setView(view: DashboardPresenter.View): Builder

        @BindsInstance
        fun setActivityContext(@ActivityContext context: Context): Builder
    }
}