package com.swtecnn.questionapp.di.module.interactors;

import com.swtecnn.questionapp.domain.executor.Executor;
import com.swtecnn.questionapp.domain.executor.MainThread;
import com.swtecnn.questionapp.domain.interactors.authentication.AuthenticationInteractor;
import com.swtecnn.questionapp.domain.interactors.authentication.AuthenticationInteractorImpl;
import com.swtecnn.questionapp.domain.repository.firebase.FirebaseRepository;
import com.swtecnn.questionapp.domain.repository.socialevent.ISocialEventRepository;
import com.swtecnn.questionapp.domain.repository.user.UserPreferenceRepository;

import dagger.Module;
import dagger.Provides;

@Module
public class AuthenticationInteractorModule {
    @Provides
    public AuthenticationInteractor getAuthenticationInteractor(Executor aExecutor, MainThread aMainThread,
                                                                UserPreferenceRepository aUserRepository,
                                                                FirebaseRepository aFirebaseRepository,
                                                                ISocialEventRepository aSocialEventRepository) {
        return new AuthenticationInteractorImpl(aExecutor, aMainThread, aUserRepository,
                aFirebaseRepository, aSocialEventRepository);
    }
}
