package com.swtecnn.questionapp.di.module.presenters;

import com.swtecnn.questionapp.di.scope.SocialEventAdminScope;
import com.swtecnn.questionapp.domain.interactors.socialevent.admin.SocialEventAdminInteractor;
import com.swtecnn.questionapp.domain.routers.socialevent.admin.SocialEventAdminRouter;
import com.swtecnn.questionapp.presentation.presenters.socialevent.admin.SocialEventAdminPresenter;
import com.swtecnn.questionapp.presentation.presenters.socialevent.admin.SocialEventAdminPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class SocialEventAdminPresenterModule {
    @Provides
    @SocialEventAdminScope
    public SocialEventAdminPresenter provideSocialEventAdminPresenter(SocialEventAdminPresenter.View view,
                                                                      SocialEventAdminInteractor aSocialEventAdminInteractor,
                                                                      SocialEventAdminRouter aSocialAdminRouter) {
        return new SocialEventAdminPresenterImpl(view, aSocialEventAdminInteractor, aSocialAdminRouter);
    }
}
