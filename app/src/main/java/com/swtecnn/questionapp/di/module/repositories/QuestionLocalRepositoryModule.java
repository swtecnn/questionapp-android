package com.swtecnn.questionapp.di.module.repositories;

import android.content.SharedPreferences;

import com.swtecnn.questionapp.domain.repository.question.IQuestionLocalRepository;
import com.swtecnn.questionapp.domain.repository.question.QuestionLocalRepositoryImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class QuestionLocalRepositoryModule {
    @Provides
    IQuestionLocalRepository provideQuestionLocalRepository(SharedPreferences aPreferences) {
        return new QuestionLocalRepositoryImpl(aPreferences);
    }
}
