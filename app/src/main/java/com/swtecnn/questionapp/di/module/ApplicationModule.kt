package com.swtecnn.questionapp.di.module

import android.app.Application
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.content.res.Resources
import com.swtecnn.questionapp.di.component.presenters.*
import com.swtecnn.questionapp.domain.executor.Executor
import com.swtecnn.questionapp.domain.executor.MainThread
import com.swtecnn.questionapp.domain.executor.impl.ThreadExecutor
import com.swtecnn.questionapp.security.SWTecNNCipher
import com.swtecnn.questionapp.security.SecureCodeCipher
import com.swtecnn.questionapp.threading.MainThreadImpl
import dagger.Module
import dagger.Provides

@Module(subcomponents = [AuthenticationPresenterComponent::class,
    SocialEventClientPresenterComponent::class,
    SocialEventQuestionsPresenterComponent::class,
    SocialEventInfoPresenterComponent::class,
    SocialEventAdminPresenterComponent::class
])
class ApplicationModule(private var application: Application) {

    @Provides
    fun provideContext(): Context {
        return application.applicationContext
    }

    @Provides
    fun provideApplication(): Application {
        return application
    }

    @Provides
    fun providePreferences(): SharedPreferences {
        return application.getSharedPreferences("questionapp_prefs", MODE_PRIVATE)
    }

    @Provides
    fun provideResources(): Resources {
        return application.resources
    }

    @Provides
    fun provideCipher(): SecureCodeCipher {
        return SWTecNNCipher()
    }

    @Provides
    fun provideMainThread(): MainThread {
        return MainThreadImpl.getInstance()
    }

    @Provides
    fun provideThreadExecutor(): Executor {
        return ThreadExecutor.getInstance()
    }
}