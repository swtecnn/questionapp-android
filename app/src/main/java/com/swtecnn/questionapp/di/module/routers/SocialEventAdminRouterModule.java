package com.swtecnn.questionapp.di.module.routers;

import android.content.Context;

import com.swtecnn.questionapp.di.qualifier.ActivityContext;
import com.swtecnn.questionapp.di.scope.SocialEventAdminScope;
import com.swtecnn.questionapp.domain.routers.socialevent.admin.SocialEventAdminRouter;
import com.swtecnn.questionapp.domain.routers.socialevent.admin.SocialEventAdminRouterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class SocialEventAdminRouterModule {
    @Provides
    @SocialEventAdminScope
    SocialEventAdminRouter provideRouter(@ActivityContext Context context) {
        return new SocialEventAdminRouterImpl(context);
    }
}
