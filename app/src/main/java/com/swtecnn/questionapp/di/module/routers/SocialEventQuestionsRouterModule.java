package com.swtecnn.questionapp.di.module.routers;

import android.content.Context;

import com.swtecnn.questionapp.di.qualifier.ActivityContext;
import com.swtecnn.questionapp.di.scope.SocialEventQuestionsScope;
import com.swtecnn.questionapp.domain.routers.socialevent.question.SocialEventQuestionsRouter;
import com.swtecnn.questionapp.domain.routers.socialevent.question.SocialEventQuestionsRouterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class SocialEventQuestionsRouterModule {
    @Provides
    @SocialEventQuestionsScope
    SocialEventQuestionsRouter provideRouter(@ActivityContext Context context) {
        return new SocialEventQuestionsRouterImpl(context);
    }
}
