package com.swtecnn.questionapp.di.module.interactors;

import com.swtecnn.questionapp.domain.executor.Executor;
import com.swtecnn.questionapp.domain.executor.MainThread;
import com.swtecnn.questionapp.domain.interactors.socialevent.info.SocialEventInfoInteractor;
import com.swtecnn.questionapp.domain.interactors.socialevent.info.SocialEventInfoInteractorImpl;
import com.swtecnn.questionapp.domain.repository.socialevent.ISocialEventRepository;
import com.swtecnn.questionapp.domain.repository.user.UserPreferenceRepository;

import dagger.Module;
import dagger.Provides;

@Module
public class SocialEventInfoInteractorModule {
    @Provides
    public SocialEventInfoInteractor getSocialEventInfoInteractor(Executor aExecutor,
                                                                  MainThread aMainThread,
                                                                  UserPreferenceRepository aUserPrefsRepository,
                                                                  ISocialEventRepository aSocialEventRepository) {
        return new SocialEventInfoInteractorImpl(aExecutor, aMainThread, aUserPrefsRepository, aSocialEventRepository);
    }
}
