package com.swtecnn.questionapp.di.component.presenters

import android.content.Context
import com.swtecnn.questionapp.di.module.interactors.SocialEventClientInteractorModule
import com.swtecnn.questionapp.di.module.presenters.SocialEventClientPresenterModule
import com.swtecnn.questionapp.di.module.routers.SocialEventClientRouterModule
import com.swtecnn.questionapp.di.qualifier.ActivityContext
import com.swtecnn.questionapp.di.scope.SocialEventClientScope
import com.swtecnn.questionapp.presentation.presenters.socialevent.client.SocialEventClientPresenter
import dagger.BindsInstance
import dagger.Subcomponent

@Subcomponent(modules = [SocialEventClientPresenterModule::class,
    SocialEventClientRouterModule::class,
    SocialEventClientInteractorModule::class])
@SocialEventClientScope
interface SocialEventClientPresenterComponent {
    val presenter: SocialEventClientPresenter

    @Subcomponent.Builder
    interface Builder {
        fun build(): SocialEventClientPresenterComponent

        @BindsInstance
        fun setView(view: SocialEventClientPresenter.View): Builder

        @BindsInstance
        fun setActivityContext(@ActivityContext context: Context): Builder
    }
}