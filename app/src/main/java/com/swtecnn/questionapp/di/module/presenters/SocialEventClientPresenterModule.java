package com.swtecnn.questionapp.di.module.presenters;

import com.swtecnn.questionapp.di.scope.SocialEventClientScope;
import com.swtecnn.questionapp.domain.interactors.socialevent.client.SocialEventClientInteractor;
import com.swtecnn.questionapp.domain.routers.socialevent.client.SocialEventClientRouter;
import com.swtecnn.questionapp.presentation.presenters.socialevent.client.SocialEventClientPresenter;
import com.swtecnn.questionapp.presentation.presenters.socialevent.client.SocialEventClientPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class SocialEventClientPresenterModule {
    @Provides
    @SocialEventClientScope
    public SocialEventClientPresenter provideSocialEventClientPresenter(SocialEventClientPresenter.View view,
                                                                        SocialEventClientInteractor socialEventClientInteractor,
                                                                        SocialEventClientRouter socialEventClientRouter) {
        return new SocialEventClientPresenterImpl(view, socialEventClientInteractor, socialEventClientRouter);
    }
}
