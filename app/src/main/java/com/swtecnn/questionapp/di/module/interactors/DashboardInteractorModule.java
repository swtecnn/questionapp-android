package com.swtecnn.questionapp.di.module.interactors;

import com.swtecnn.questionapp.domain.executor.Executor;
import com.swtecnn.questionapp.domain.executor.MainThread;
import com.swtecnn.questionapp.domain.interactors.dashboard.DashboardInteractor;
import com.swtecnn.questionapp.domain.interactors.dashboard.DashboardInteractorImpl;
import com.swtecnn.questionapp.domain.repository.socialevent.ISocialEventLocalRepository;
import com.swtecnn.questionapp.domain.repository.socialevent.ISocialEventRepository;

import dagger.Module;
import dagger.Provides;

@Module
public class DashboardInteractorModule {
    @Provides
    public DashboardInteractor getDashboardInteractor(Executor aExecutor, MainThread aMainThread,
                                                      ISocialEventRepository aSocialEventRepository,
                                                      ISocialEventLocalRepository aSocialEventLocalRepository) {
        return new DashboardInteractorImpl(aExecutor, aMainThread, aSocialEventLocalRepository, aSocialEventRepository);
    }
}
