package com.swtecnn.questionapp.di.module.interactors;

import com.swtecnn.questionapp.domain.executor.Executor;
import com.swtecnn.questionapp.domain.executor.MainThread;
import com.swtecnn.questionapp.domain.interactors.socialevent.question.SocialEventQuestionsInteractor;
import com.swtecnn.questionapp.domain.interactors.socialevent.question.SocialEventQuestionsInteractorImpl;
import com.swtecnn.questionapp.domain.repository.question.IQuestionLocalRepository;
import com.swtecnn.questionapp.domain.repository.question.IQuestionRepository;
import com.swtecnn.questionapp.domain.repository.user.UserPreferenceRepository;

import dagger.Module;
import dagger.Provides;

@Module
public class SocialEventQuestionsInteractorModule {
    @Provides
    public SocialEventQuestionsInteractor getSocialEventQuestionsInteractor(Executor aExecutor,
                                                                            MainThread aMainThread,
                                                                            IQuestionRepository aQuestionRepository,
                                                                            IQuestionLocalRepository aQuestionLocalRepository,
                                                                            UserPreferenceRepository aUserPrefRepository) {
        return new SocialEventQuestionsInteractorImpl(aExecutor, aMainThread, aQuestionRepository,
                aQuestionLocalRepository, aUserPrefRepository);
    }
}
