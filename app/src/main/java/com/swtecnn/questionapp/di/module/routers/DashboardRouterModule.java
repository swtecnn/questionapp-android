package com.swtecnn.questionapp.di.module.routers;

import android.content.Context;

import com.swtecnn.questionapp.di.qualifier.ActivityContext;
import com.swtecnn.questionapp.di.scope.DashboardScope;
import com.swtecnn.questionapp.domain.routers.dashboard.DashboardRouter;
import com.swtecnn.questionapp.domain.routers.dashboard.DashboardRouterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class DashboardRouterModule {
    @Provides
    @DashboardScope
    DashboardRouter provideRouter(@ActivityContext Context context) {
        return new DashboardRouterImpl(context);
    }
}
