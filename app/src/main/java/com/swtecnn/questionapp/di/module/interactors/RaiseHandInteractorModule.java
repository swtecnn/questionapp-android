package com.swtecnn.questionapp.di.module.interactors;

import com.swtecnn.questionapp.domain.executor.Executor;
import com.swtecnn.questionapp.domain.executor.MainThread;
import com.swtecnn.questionapp.domain.interactors.raisehand.RaiseHandInteractor;
import com.swtecnn.questionapp.domain.interactors.raisehand.RaiseHandInteractorImpl;
import com.swtecnn.questionapp.domain.repository.raisehand.IRaiseHandRepository;

import dagger.Module;
import dagger.Provides;

@Module
public class RaiseHandInteractorModule {
    @Provides
    public RaiseHandInteractor getRaiseHandInteractor(Executor aExecutor, MainThread aMainThread,
                                                      IRaiseHandRepository aRaiseHandRepository) {
        return new RaiseHandInteractorImpl(aExecutor, aMainThread, aRaiseHandRepository);
    }
}
