package com.swtecnn.questionapp.di.component

import com.swtecnn.questionapp.di.component.presenters.*
import com.swtecnn.questionapp.di.module.ApplicationModule
import com.swtecnn.questionapp.di.module.repositories.*
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class,
    UserPreferencesRepositoryModule::class,
    FirebaseRepositoryModule::class,
    QuestionRepositoryModule::class,
    QuestionLocalRepositoryModule::class,
    SocialEventRepositoryModule::class,
    SocialEventLocalRepositoryModule::class,
    RaiseHandRepositoryModule::class,
    RaiseHandLocalRepositoryModule::class])
interface ApplicationComponent {
    fun getAuthenticationPresenterBuilder(): AuthenticationPresenterComponent.Builder
    fun getSocialEventClientPresenterBuilder(): SocialEventClientPresenterComponent.Builder
    fun getSocialEventInfoPresenterBuilder(): SocialEventInfoPresenterComponent.Builder
    fun getSocialEventAdminPresenterBuilder(): SocialEventAdminPresenterComponent.Builder
    fun getSocialEventQuestionsPresenterBuilder(): SocialEventQuestionsPresenterComponent.Builder
    fun getDashboardPresenterBuilder(): DashboardPresenterComponent.Builder
    fun getRaiseHandPresenterBuilder(): RaiseHandPresenterComponent.Builder
}