package com.swtecnn.questionapp.di.module.repositories;

import com.swtecnn.questionapp.domain.repository.socialevent.ISocialEventRepository;
import com.swtecnn.questionapp.domain.repository.socialevent.SocialEventRepositoryImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class SocialEventRepositoryModule {
    @Provides
    ISocialEventRepository provideSocialEventRepository() {
        return new SocialEventRepositoryImpl();
    }
}
