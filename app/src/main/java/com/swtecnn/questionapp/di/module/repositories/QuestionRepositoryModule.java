package com.swtecnn.questionapp.di.module.repositories;

import com.swtecnn.questionapp.domain.repository.question.IQuestionRepository;
import com.swtecnn.questionapp.domain.repository.question.QuestionRepositoryImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class QuestionRepositoryModule {
    @Provides
    IQuestionRepository provideQuestionRepository() {
        return new QuestionRepositoryImpl();
    }
}
