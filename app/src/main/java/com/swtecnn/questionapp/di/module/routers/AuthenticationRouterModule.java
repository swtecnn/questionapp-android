package com.swtecnn.questionapp.di.module.routers;

import android.content.Context;

import com.swtecnn.questionapp.di.qualifier.ActivityContext;
import com.swtecnn.questionapp.di.scope.AuthenticationScope;
import com.swtecnn.questionapp.domain.routers.authentication.AuthenticationRouter;
import com.swtecnn.questionapp.domain.routers.authentication.AuthenticationRouterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class AuthenticationRouterModule {
    @Provides
    @AuthenticationScope
    AuthenticationRouter provideRouter(@ActivityContext Context context) {
        return new AuthenticationRouterImpl(context);
    }
}
