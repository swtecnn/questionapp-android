package com.swtecnn.questionapp.di.module.repositories;

import android.content.SharedPreferences;

import com.swtecnn.questionapp.domain.repository.raisehand.IRaiseHandLocalRepository;
import com.swtecnn.questionapp.domain.repository.raisehand.RaiseHandLocalRepositoryImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class RaiseHandLocalRepositoryModule {
    @Provides
    IRaiseHandLocalRepository provideRaiseHandLocalRepository(SharedPreferences aPreferences) {
        return new RaiseHandLocalRepositoryImpl(aPreferences);
    }
}
