package com.swtecnn.questionapp.di.module.interactors;

import com.swtecnn.questionapp.domain.executor.Executor;
import com.swtecnn.questionapp.domain.executor.MainThread;
import com.swtecnn.questionapp.domain.interactors.socialevent.client.SocialEventClientInteractor;
import com.swtecnn.questionapp.domain.interactors.socialevent.client.SocialEventClientInteractorImpl;
import com.swtecnn.questionapp.domain.repository.firebase.FirebaseRepository;
import com.swtecnn.questionapp.domain.repository.question.IQuestionLocalRepository;
import com.swtecnn.questionapp.domain.repository.question.IQuestionRepository;
import com.swtecnn.questionapp.domain.repository.raisehand.IRaiseHandLocalRepository;
import com.swtecnn.questionapp.domain.repository.raisehand.IRaiseHandRepository;
import com.swtecnn.questionapp.domain.repository.socialevent.ISocialEventLocalRepository;
import com.swtecnn.questionapp.domain.repository.socialevent.ISocialEventRepository;
import com.swtecnn.questionapp.domain.repository.user.UserPreferenceRepository;

import dagger.Module;
import dagger.Provides;

@Module
public class SocialEventClientInteractorModule {
    @Provides
    public SocialEventClientInteractor getSocialEventClientInteractor(Executor aExecutor,
                                                                      MainThread aMainThread,
                                                                      IRaiseHandLocalRepository aRaiseHandLocalRepository,
                                                                      IRaiseHandRepository aRaiseHandRepository,
                                                                      IQuestionRepository aQuestionRepository,
                                                                      FirebaseRepository aFirebaseRepository,
                                                                      IQuestionLocalRepository aQuestionLocalRepository,
                                                                      UserPreferenceRepository aUserPrefRepository,
                                                                      ISocialEventRepository aSocialEventRepository,
                                                                      ISocialEventLocalRepository aSocialEventLocalRepository) {
        return new SocialEventClientInteractorImpl(aExecutor, aMainThread, aRaiseHandLocalRepository, aRaiseHandRepository,
                aQuestionRepository, aFirebaseRepository, aQuestionLocalRepository,
                aUserPrefRepository, aSocialEventRepository, aSocialEventLocalRepository);
    }
}
