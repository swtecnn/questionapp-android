package com.swtecnn.questionapp.di.module.interactors;

import com.swtecnn.questionapp.domain.executor.Executor;
import com.swtecnn.questionapp.domain.executor.MainThread;
import com.swtecnn.questionapp.domain.interactors.socialevent.admin.SocialEventAdminInteractor;
import com.swtecnn.questionapp.domain.interactors.socialevent.admin.SocialEventAdminInteractorImpl;
import com.swtecnn.questionapp.domain.repository.question.IQuestionRepository;
import com.swtecnn.questionapp.domain.repository.raisehand.IRaiseHandRepository;
import com.swtecnn.questionapp.domain.repository.socialevent.ISocialEventLocalRepository;
import com.swtecnn.questionapp.domain.repository.socialevent.ISocialEventRepository;

import dagger.Module;
import dagger.Provides;

@Module
public class SocialEventAdminInteractorModule {
    @Provides
    public SocialEventAdminInteractor getSocialEventAdminInteractor(Executor aExecutor,
                                                                    MainThread aMainThread,
                                                                    ISocialEventRepository aSocialEventRepository,
                                                                    IQuestionRepository aQuestionRepository,
                                                                    IRaiseHandRepository aRaiseHandRepository,
                                                                    ISocialEventLocalRepository aSocialEventLocalRepository) {
        return new SocialEventAdminInteractorImpl(aExecutor, aMainThread, aSocialEventRepository,
                aQuestionRepository, aRaiseHandRepository, aSocialEventLocalRepository);
    }
}
