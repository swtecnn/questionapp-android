package com.swtecnn.questionapp.di.component.presenters

import android.content.Context
import com.swtecnn.questionapp.di.module.interactors.SocialEventQuestionsInteractorModule
import com.swtecnn.questionapp.di.module.presenters.SocialEventQuestionsPresenterModule
import com.swtecnn.questionapp.di.module.routers.SocialEventQuestionsRouterModule
import com.swtecnn.questionapp.di.qualifier.ActivityContext
import com.swtecnn.questionapp.di.scope.SocialEventQuestionsScope
import com.swtecnn.questionapp.presentation.presenters.socialevent.question.SocialEventQuestionsPresenter
import dagger.BindsInstance
import dagger.Subcomponent

@Subcomponent(modules = [SocialEventQuestionsPresenterModule::class,
    SocialEventQuestionsRouterModule::class,
    SocialEventQuestionsInteractorModule::class])
@SocialEventQuestionsScope
interface SocialEventQuestionsPresenterComponent {
    val presenter: SocialEventQuestionsPresenter

    @Subcomponent.Builder
    interface Builder {
        fun build(): SocialEventQuestionsPresenterComponent

        @BindsInstance
        fun setView(view: SocialEventQuestionsPresenter.View): Builder

        @BindsInstance
        fun setActivityContext(@ActivityContext context: Context): Builder
    }
}