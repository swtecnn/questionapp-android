package com.swtecnn.questionapp.di.module.repositories;

import android.content.SharedPreferences;

import com.swtecnn.questionapp.domain.repository.firebase.FirebaseRepository;
import com.swtecnn.questionapp.domain.repository.firebase.FirebaseRepositoryImpl;
import com.swtecnn.questionapp.domain.repository.user.UserPreferenceRepository;
import com.swtecnn.questionapp.domain.repository.user.UserPreferenceRepositoryImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class FirebaseRepositoryModule {
    @Provides
    FirebaseRepository provideFirebaseRepository(SharedPreferences aPreferences) {
        return new FirebaseRepositoryImpl(aPreferences);
    }
}
