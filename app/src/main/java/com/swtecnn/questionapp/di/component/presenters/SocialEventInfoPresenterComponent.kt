package com.swtecnn.questionapp.di.component.presenters

import android.content.Context
import com.swtecnn.questionapp.di.module.interactors.SocialEventInfoInteractorModule
import com.swtecnn.questionapp.di.module.presenters.SocialEventInfoPresenterModule
import com.swtecnn.questionapp.di.module.routers.SocialEventInfoRouterModule
import com.swtecnn.questionapp.di.qualifier.ActivityContext
import com.swtecnn.questionapp.di.scope.SocialEventClientScope
import com.swtecnn.questionapp.di.scope.SocialEventInfoScope
import com.swtecnn.questionapp.presentation.presenters.socialevent.info.SocialEventInfoPresenter
import dagger.BindsInstance
import dagger.Subcomponent

@Subcomponent(modules = [SocialEventInfoPresenterModule::class,
    SocialEventInfoRouterModule::class,
    SocialEventInfoInteractorModule::class])
@SocialEventInfoScope
interface SocialEventInfoPresenterComponent {
    val presenter: SocialEventInfoPresenter

    @Subcomponent.Builder
    interface Builder {
        fun build(): SocialEventInfoPresenterComponent

        @BindsInstance
        fun setView(view: SocialEventInfoPresenter.View): Builder

        @BindsInstance
        fun setActivityContext(@ActivityContext context: Context): Builder
    }
}