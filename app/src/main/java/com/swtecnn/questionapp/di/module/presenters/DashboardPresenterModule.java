package com.swtecnn.questionapp.di.module.presenters;

import com.swtecnn.questionapp.di.scope.DashboardScope;
import com.swtecnn.questionapp.domain.interactors.dashboard.DashboardInteractor;
import com.swtecnn.questionapp.domain.routers.dashboard.DashboardRouter;
import com.swtecnn.questionapp.presentation.presenters.dashboard.DashboardPresenter;
import com.swtecnn.questionapp.presentation.presenters.dashboard.DashboardPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class DashboardPresenterModule {
    @Provides
    @DashboardScope
    public DashboardPresenter provideDashboardPresenter(DashboardPresenter.View view,
                                                        DashboardRouter aDashboardRouter,
                                                        DashboardInteractor aDashboardInteractor) {
        return new DashboardPresenterImpl(view, aDashboardRouter, aDashboardInteractor);
    }
}
