package com.swtecnn.questionapp.di.module.presenters;

import com.swtecnn.questionapp.di.scope.SocialEventClientScope;
import com.swtecnn.questionapp.domain.interactors.raisehand.RaiseHandInteractor;
import com.swtecnn.questionapp.presentation.presenters.raisehand.RaiseHandPresenter;
import com.swtecnn.questionapp.presentation.presenters.raisehand.RaiseHandPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class RaiseHandPresenterModule {
    @Provides
    @SocialEventClientScope
    public RaiseHandPresenter provideRaiseHandPresenter(RaiseHandPresenter.View view,
                                                        RaiseHandInteractor aRaiseHandInteractor) {
        return new RaiseHandPresenterImpl(view, aRaiseHandInteractor);
    }
}
