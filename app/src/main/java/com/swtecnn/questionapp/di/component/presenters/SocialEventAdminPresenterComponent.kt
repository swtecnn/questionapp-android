package com.swtecnn.questionapp.di.component.presenters

import android.content.Context
import com.swtecnn.questionapp.di.module.interactors.SocialEventAdminInteractorModule
import com.swtecnn.questionapp.di.module.presenters.SocialEventAdminPresenterModule
import com.swtecnn.questionapp.di.module.routers.SocialEventAdminRouterModule
import com.swtecnn.questionapp.di.qualifier.ActivityContext
import com.swtecnn.questionapp.di.scope.SocialEventAdminScope
import com.swtecnn.questionapp.presentation.presenters.socialevent.admin.SocialEventAdminPresenter
import dagger.BindsInstance
import dagger.Subcomponent

@Subcomponent(modules = [SocialEventAdminPresenterModule::class,
    SocialEventAdminInteractorModule::class,
    SocialEventAdminRouterModule::class])
@SocialEventAdminScope
interface SocialEventAdminPresenterComponent {
    val presenter: SocialEventAdminPresenter

    @Subcomponent.Builder
    interface Builder {
        fun build(): SocialEventAdminPresenterComponent

        @BindsInstance
        fun setView(view: SocialEventAdminPresenter.View): Builder

        @BindsInstance
        fun setActivityContext(@ActivityContext context: Context): Builder
    }
}