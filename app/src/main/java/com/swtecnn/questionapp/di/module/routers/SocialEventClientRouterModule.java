package com.swtecnn.questionapp.di.module.routers;

import android.content.Context;

import com.swtecnn.questionapp.di.qualifier.ActivityContext;
import com.swtecnn.questionapp.di.scope.SocialEventClientScope;
import com.swtecnn.questionapp.domain.routers.socialevent.client.SocialEventClientRouter;
import com.swtecnn.questionapp.domain.routers.socialevent.client.SocialEventClientRouterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class SocialEventClientRouterModule {
    @Provides
    @SocialEventClientScope
    SocialEventClientRouter provideRouter(@ActivityContext Context context) {
        return new SocialEventClientRouterImpl(context);
    }
}
