package com.swtecnn.questionapp.di.module.repositories;

import android.content.SharedPreferences;
import android.content.res.Resources;

import com.swtecnn.questionapp.domain.repository.user.UserPreferenceRepository;
import com.swtecnn.questionapp.domain.repository.user.UserPreferenceRepositoryImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class UserPreferencesRepositoryModule {
    @Provides
    UserPreferenceRepository provideUserPreferencesRepository(SharedPreferences aPreferences, Resources aResources) {
        return new UserPreferenceRepositoryImpl(aPreferences, aResources);
    }
}
