package com.swtecnn.questionapp.di.module.repositories;

import android.content.res.Resources;

import com.swtecnn.questionapp.domain.repository.raisehand.IRaiseHandRepository;
import com.swtecnn.questionapp.domain.repository.raisehand.RaiseHandRepositoryImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class RaiseHandRepositoryModule {
    @Provides
    IRaiseHandRepository provideRaiseHandRepository(Resources aResources) {
        return new RaiseHandRepositoryImpl(aResources);
    }
}
