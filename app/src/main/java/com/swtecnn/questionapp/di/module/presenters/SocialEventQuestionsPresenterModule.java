package com.swtecnn.questionapp.di.module.presenters;

import com.swtecnn.questionapp.di.scope.SocialEventQuestionsScope;
import com.swtecnn.questionapp.domain.interactors.socialevent.question.SocialEventQuestionsInteractor;
import com.swtecnn.questionapp.domain.routers.socialevent.question.SocialEventQuestionsRouter;
import com.swtecnn.questionapp.presentation.presenters.socialevent.question.SocialEventQuestionsPresenter;
import com.swtecnn.questionapp.presentation.presenters.socialevent.question.SocialEventQuestionsPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class SocialEventQuestionsPresenterModule {
    @Provides
    @SocialEventQuestionsScope
    public SocialEventQuestionsPresenter provideSocialEventQuestionsPresenter(SocialEventQuestionsPresenter.View view,
                                                                           SocialEventQuestionsInteractor socialEventQuestionsInteractor,
                                                                           SocialEventQuestionsRouter socialEventQuestionsRouter) {
        return new SocialEventQuestionsPresenterImpl(view, socialEventQuestionsInteractor, socialEventQuestionsRouter);
    }
}
