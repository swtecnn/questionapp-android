package com.swtecnn.questionapp.di.module.presenters;

import com.swtecnn.questionapp.di.scope.AuthenticationScope;
import com.swtecnn.questionapp.domain.interactors.authentication.AuthenticationInteractor;
import com.swtecnn.questionapp.domain.routers.authentication.AuthenticationRouter;
import com.swtecnn.questionapp.presentation.presenters.authentication.AuthenticationPresenter;
import com.swtecnn.questionapp.presentation.presenters.authentication.AuthenticationPresenterImpl;
import com.swtecnn.questionapp.security.SecureCodeCipher;

import dagger.Module;
import dagger.Provides;

@Module
public class AuthenticationPresenterModule {
    @Provides
    @AuthenticationScope
    public AuthenticationPresenter provideAuthenticationPresenter(AuthenticationPresenter.View view,
                                                                  AuthenticationInteractor aAuthenticationInteractor,
                                                                  AuthenticationRouter aAuthenticationRouter,
                                                                  SecureCodeCipher aCipher) {
        return new AuthenticationPresenterImpl(view, aAuthenticationInteractor, aAuthenticationRouter, aCipher);
    }
}
