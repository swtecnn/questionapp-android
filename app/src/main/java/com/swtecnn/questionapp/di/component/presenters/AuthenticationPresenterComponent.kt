package com.swtecnn.questionapp.di.component.presenters

import android.content.Context
import com.swtecnn.questionapp.di.module.interactors.AuthenticationInteractorModule
import com.swtecnn.questionapp.di.module.presenters.AuthenticationPresenterModule
import com.swtecnn.questionapp.di.module.routers.AuthenticationRouterModule
import com.swtecnn.questionapp.di.qualifier.ActivityContext
import com.swtecnn.questionapp.di.scope.AuthenticationScope
import com.swtecnn.questionapp.presentation.presenters.authentication.AuthenticationPresenter
import dagger.BindsInstance
import dagger.Subcomponent

@Subcomponent(modules = [AuthenticationInteractorModule::class,
    AuthenticationPresenterModule::class,
    AuthenticationRouterModule::class])
@AuthenticationScope
interface AuthenticationPresenterComponent {
    val presenter: AuthenticationPresenter

    @Subcomponent.Builder
    interface Builder {
        fun build(): AuthenticationPresenterComponent

        @BindsInstance
        fun setView(view: AuthenticationPresenter.View): Builder

        @BindsInstance
        fun setActivityContext(@ActivityContext context: Context): Builder
    }
}