package com.swtecnn.questionapp.di.component.presenters

import android.content.Context
import com.swtecnn.questionapp.di.module.interactors.RaiseHandInteractorModule
import com.swtecnn.questionapp.di.module.presenters.RaiseHandPresenterModule
import com.swtecnn.questionapp.di.qualifier.ActivityContext
import com.swtecnn.questionapp.di.scope.SocialEventClientScope
import com.swtecnn.questionapp.presentation.presenters.raisehand.RaiseHandPresenter
import dagger.BindsInstance
import dagger.Subcomponent

@Subcomponent(modules = [RaiseHandInteractorModule::class,
    RaiseHandPresenterModule::class])
@SocialEventClientScope
interface RaiseHandPresenterComponent {
    val presenter: RaiseHandPresenter

    @Subcomponent.Builder
    interface Builder {
        fun build(): RaiseHandPresenterComponent

        @BindsInstance
        fun setView(view: RaiseHandPresenter.View): Builder

        @BindsInstance
        fun setActivityContext(@ActivityContext context: Context): Builder
    }
}