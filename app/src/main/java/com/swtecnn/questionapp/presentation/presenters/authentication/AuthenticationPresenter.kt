package com.swtecnn.questionapp.presentation.presenters.authentication;

import com.google.android.gms.vision.barcode.Barcode
import com.swtecnn.questionapp.presentation.presenters.BasePresenter;
import com.swtecnn.questionapp.ui.BaseView;
import com.swtecnn.questionapp.ui.barcode.camera.BarcodeGraphicTracker

interface AuthenticationPresenter: BasePresenter {

    fun onPinCodeEntered(aPinCode: String)
    fun onEditNameClicked()
    fun scanQrCodeClicked()
    fun saveUserName(aName: String, aDefaultName: String)
    fun getUserName()
    fun onBarcode(aBarcode: Barcode)
    fun resetRole()

    interface View: BaseView {
        fun onInvalidQrCodeScanned()
        fun onLecturerPinNotValid()
        fun updateUserNameView(aUserName: String)
        fun showEditNameDialog()
    }
}
