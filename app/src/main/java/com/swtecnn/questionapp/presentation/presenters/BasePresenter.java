package com.swtecnn.questionapp.presentation.presenters;

public interface BasePresenter {
    void create();
    void start();
    void resume();
    void pause();
    void stop();
    void destroy();

    void onNetworkAvailable();
    void onNetworkUnavailable();

    void showProgress();
    void hideProgress();
}