package com.swtecnn.questionapp.presentation.presenters.socialevent.question

import com.swtecnn.questionapp.domain.Role
import com.swtecnn.questionapp.domain.model.Question
import com.swtecnn.questionapp.presentation.presenters.BasePresenter
import com.swtecnn.questionapp.ui.BaseView

interface SocialEventQuestionsPresenter: BasePresenter {

    fun fetchQuestions(aSocialEventId: String)
    fun deleteQuestion(aQuestion: Question)
    fun answerGiven(aQuestion: Question)
    fun withdrawQuestion(aQuestion: Question)
    fun voteQuestion(aQuestion: Question)
    fun unVoteQuestion(aQuestion: Question)
    fun setRole(role: Role)

    interface View: BaseView {
        fun onQuestionsDataReceived(aQuestions: List<Question>, aUserQuestions: Set<String>,
                                    aUserVotes: Set<String>)
        fun onFailedToDeleteQuestion()
        fun onFailedToVoteQuestion()
    }
}
