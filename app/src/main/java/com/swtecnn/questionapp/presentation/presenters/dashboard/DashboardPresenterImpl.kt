package com.swtecnn.questionapp.presentation.presenters.dashboard;

import com.swtecnn.questionapp.domain.Role
import com.swtecnn.questionapp.domain.interactors.dashboard.DashboardInteractor
import com.swtecnn.questionapp.domain.model.SocialEvent
import com.swtecnn.questionapp.domain.model.SocialEventRole
import com.swtecnn.questionapp.domain.routers.dashboard.DashboardRouter
import com.swtecnn.questionapp.presentation.presenters.AbstractPresenter
import timber.log.Timber
import java.lang.Exception

class DashboardPresenterImpl(aView: DashboardPresenter.View,
                             private val mRouter: DashboardRouter,
                             private val mDashboardInteractor: DashboardInteractor)
    : AbstractPresenter<DashboardPresenter.View>(aView),
        DashboardPresenter, DashboardInteractor.Callback {

    private lateinit var mUserArchivedSocialEventIds: Set<SocialEventRole>
    private val mAllUserSocialEvents = ArrayList<SocialEvent>()

    init {
        mDashboardInteractor.setCallback(this)
    }

    override fun resume() {
        if (isNetworkAvailable) {
            obtainArchivedSocialEvents()
        } else {
            view.showNoInternetAlert()
        }
    }

    private fun obtainArchivedSocialEvents() {
        showProgress()
        mDashboardInteractor.obtainArchivedSocialEvents()
    }

    override fun onNetworkAvailable() {
        obtainArchivedSocialEvents()
    }

    override fun onNetworkUnavailable() {

    }

    override fun onViewAllUserEvents() {
        mRouter.showUserSocialEvents(mAllUserSocialEvents)
    }

    override fun onSocialEventSelected(aSocialEventId: String) {
        mAllUserSocialEvents.forEach {
            if (it.id == aSocialEventId) {
                if (it.archived) {
                    mRouter.showSocialEventDetails(it.id)
                } else {
                    mRouter.showClientScreen(it.id)
                }
                return
            }
        }
    }

    override fun onArchivedEventsReceived(aSocialEvents: Set<SocialEventRole>) {
        mUserArchivedSocialEventIds = aSocialEvents
        mDashboardInteractor.obtainAllSocialEvents()
    }

    override fun onFailedToGetArchivedEvents(aException: Exception) {
        Timber.e(aException, "Failed to obtain archived social events: %s", aException.message)
        hideProgress()
    }

    override fun onAllEventsReceived(aSocialEvents: List<SocialEvent>) {
        val resultList = ArrayList<SocialEvent>()
        mAllUserSocialEvents.clear()
        aSocialEvents.forEach { socialEvent ->
            mUserArchivedSocialEventIds.forEach userEvents@{
                if (it.eventId == socialEvent.id) {
                    val event = SocialEvent(socialEvent, it.role)
                    if (Role.getRoleById(it.role) == Role.CLIENT) {
                        resultList.add(event)
                    }
                    mAllUserSocialEvents.add(event)
                    return@userEvents
                }
            }
        }
        hideProgress()
        if (resultList.size > 0) {
            view.onClientSocialEventsLoaded(resultList)
        }
    }

}
