package com.swtecnn.questionapp.presentation.presenters.raisehand

import com.swtecnn.questionapp.domain.interactors.raisehand.RaiseHandInteractor
import com.swtecnn.questionapp.domain.model.RaiseHandRequestStatus
import com.swtecnn.questionapp.presentation.presenters.AbstractPresenter

class RaiseHandPresenterImpl(aView: RaiseHandPresenter.View,
                             private val mRaiseHandInteractor: RaiseHandInteractor)
    : AbstractPresenter<RaiseHandPresenter.View>(aView),
        RaiseHandPresenter, RaiseHandInteractor.Callback {

    init {
        mRaiseHandInteractor.setCallback(this)
    }

    override fun refreshStatus(aRaiseHandRequestId: String) {
        showProgress()
        mRaiseHandInteractor.refreshRaiseHandStatus(aRaiseHandRequestId)
    }

    override fun onRaiseHandStatusRefreshed(aRaiseHandRequestStatus: RaiseHandRequestStatus) {
        hideProgress()
        view.onRaiseHandStatusRefreshed(aRaiseHandRequestStatus)
    }

}
