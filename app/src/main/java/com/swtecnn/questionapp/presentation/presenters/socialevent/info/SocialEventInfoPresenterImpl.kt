package com.swtecnn.questionapp.presentation.presenters.socialevent.info

import com.swtecnn.questionapp.domain.Role
import com.swtecnn.questionapp.domain.interactors.socialevent.info.SocialEventInfoInteractor
import com.swtecnn.questionapp.domain.model.SocialEvent
import com.swtecnn.questionapp.domain.routers.socialevent.info.SocialEventInfoRouter
import com.swtecnn.questionapp.presentation.presenters.AbstractPresenter

class SocialEventInfoPresenterImpl(aView: SocialEventInfoPresenter.View,
                                   private val mRouter: SocialEventInfoRouter,
                                   private val mSocialEventInfoInteractor: SocialEventInfoInteractor) : AbstractPresenter<SocialEventInfoPresenter.View>(aView),
        SocialEventInfoPresenter, SocialEventInfoInteractor.Callback {

    init {
        mSocialEventInfoInteractor.setCallback(this)
        mSocialEventInfoInteractor.obtainUserRole()
    }

    override fun getLectureInfo(aSocialEventId: String) {
        showProgress()
        mSocialEventInfoInteractor.obtainSocialEvent(aSocialEventId)
    }

    override fun onFailedToObtainSocialEvent() {
        hideProgress()
        view.onFailedToGetSocialEvent()
    }

    override fun onSocialEventReceived(aSocialEvent: SocialEvent) {
        hideProgress()
        view.onSocialEventReceived(aSocialEvent)
    }

    override fun showSocialEventQuestions(aSocialEventId: String) {
        mRouter.showSocialEventQuestions(aSocialEventId)
    }

    override fun onSocialEventAdded(aSocialEventId: String) {

    }

    override fun onSocialEventAddFailed(aSocialEventId: String) {

    }

    override fun onRoleReceived(aRole: Role) {
        view.onRoleReceived(aRole)
    }

}
