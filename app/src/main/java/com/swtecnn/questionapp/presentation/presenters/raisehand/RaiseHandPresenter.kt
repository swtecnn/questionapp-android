package com.swtecnn.questionapp.presentation.presenters.raisehand

import com.swtecnn.questionapp.domain.model.RaiseHandRequestStatus
import com.swtecnn.questionapp.domain.model.SocialEvent
import com.swtecnn.questionapp.presentation.presenters.BasePresenter
import com.swtecnn.questionapp.ui.BaseView

interface RaiseHandPresenter: BasePresenter {

    fun refreshStatus(aRaiseHandRequestId: String)

    interface View: BaseView {
        fun onRaiseHandStatusRefreshed(aRaiseHandStatus: RaiseHandRequestStatus)
    }
}
