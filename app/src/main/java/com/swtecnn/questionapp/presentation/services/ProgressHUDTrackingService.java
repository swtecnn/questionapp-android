package com.swtecnn.questionapp.presentation.services;

public class ProgressHUDTrackingService {

    private Integer mTrackedCount;

    public ProgressHUDTrackingService() { mTrackedCount = 0; }

    public void addTracking() { mTrackedCount++; }

    public void removeTracking() {
        if (mTrackedCount > 0) {
            mTrackedCount--;
        }
    }

    public boolean canHide() { return mTrackedCount == 0; }
}
