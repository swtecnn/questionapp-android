package com.swtecnn.questionapp.presentation.presenters.socialevent.client

import com.swtecnn.questionapp.domain.interactors.socialevent.client.SocialEventClientInteractor
import com.swtecnn.questionapp.domain.model.Question
import com.swtecnn.questionapp.domain.model.RaiseHandRequest
import com.swtecnn.questionapp.domain.model.RaiseHandRequestStatus
import com.swtecnn.questionapp.domain.model.SocialEvent
import com.swtecnn.questionapp.domain.routers.socialevent.client.SocialEventClientRouter
import com.swtecnn.questionapp.presentation.presenters.AbstractPresenter
import timber.log.Timber
import java.util.*

class SocialEventClientPresenterImpl(aView: SocialEventClientPresenter.View,
                                     private val mSocialEventClientInteractor: SocialEventClientInteractor,
                                     private val mRouter: SocialEventClientRouter) : AbstractPresenter<SocialEventClientPresenter.View>(aView),
        SocialEventClientPresenter, SocialEventClientInteractor.Callback {

    private lateinit var mSocialEventId: String
    private lateinit var mListenerName: String

    init {
        mSocialEventClientInteractor.setCallback(this)
    }

    override fun start() {
        getUserName()
    }

    override fun saveSocialEvent(aSocialEventId: String) {
        mSocialEventClientInteractor.saveSocialEventLocally(aSocialEventId)
    }

    override fun onAskQuestionClicked() {
        mRouter.startAskQuestion(mListenerName, mSocialEventId)
    }

    override fun onRaiseHandClicked() {
        showProgress()
        mSocialEventClientInteractor.obtainCachedRaiseHand()
    }

    override fun onEventInfoClicked() {
        mRouter.showEventInfo(mSocialEventId)
    }

    override fun onShowAllQuestionClicked() {
        mRouter.showAllQuestions(mSocialEventId)
    }

    override fun onRaiseHandPosted(aRaiseHandId: String) {
        mSocialEventClientInteractor.getRaiseHandStatus(aRaiseHandId)
    }

    override fun onCachedRaiseHandReceived(aRaiseHandId: String) {
        if (aRaiseHandId.isEmpty()) {
            mSocialEventClientInteractor.obtainRaiseHandData()
        } else {
            mSocialEventClientInteractor.getRaiseHandStatus(aRaiseHandId)
        }
    }

    override fun onRaiseHandStatusReceived(aRaiseHandStatus: RaiseHandRequestStatus) {
        if (aRaiseHandStatus.status == RaiseHandRequestStatus.State.ARCHIVED) {
            Timber.d("Our raise hand request: %s was deleted (answer given), can create a new one", aRaiseHandStatus.raiseHandRequestId)
            mSocialEventClientInteractor.obtainRaiseHandData()
        } else {
            hideProgress()
            Timber.d("Hand raised - order: %d, status: %s", aRaiseHandStatus.order, aRaiseHandStatus.status.name)
            mSocialEventClientInteractor.cacheRaiseHandReuqest(aRaiseHandStatus.raiseHandRequestId)
            mRouter.showRaiseHandRequest(aRaiseHandStatus.status, aRaiseHandStatus.order.toString(),
                    aRaiseHandStatus.raiseHandRequestId)
        }
    }

    override fun onRaiseHandDataReceived(aUserName: String, aFcmToken: String) {
        val raiseHandRequest = RaiseHandRequest(mSocialEventId, aFcmToken, aUserName, Date().time)
        mSocialEventClientInteractor.raiseHand(raiseHandRequest)
    }

    override fun onQuestionSuccessfullyPostedRemote(aQuestionId: String) {
        mSocialEventClientInteractor.saveQuestionLocally(aQuestionId)
    }

    override fun onQuestionSuccessfullyPostedLocal() {
        hideProgress()
        view.onQuestionPosted()
    }

    override fun onQuestionPostFailed() {
        hideProgress()
        view.onQuestionPostFailed()
    }

    override fun onSocialEventLoaded(event: SocialEvent) {
        hideProgress()
        view.onSocialEventLoaded(event)
    }

    override fun getSocialEventInfo(aSocialEventId: String) {
        showProgress()
        mSocialEventId = aSocialEventId
        mSocialEventClientInteractor.loadSocialEvent(mSocialEventId)
    }

    override fun postQuestion(aQuestion: Question) {
        showProgress()
        mSocialEventClientInteractor.postQuestion(aQuestion)
    }

    override fun getUserName() {
        mSocialEventClientInteractor.obtainUserName()
    }

    override fun onUserNameReceived(aUserName: String) {
        mListenerName = aUserName
    }

    override fun deleteRaiseHandRequest(aRaiseHandRequestId: String) {
        showProgress()
        mSocialEventClientInteractor.deleteRaiseHandRequest(aRaiseHandRequestId)
    }

    override fun onRaiseHandRequestDeleted(aSuccess: Boolean) {
        hideProgress()
        if (aSuccess) {
            view.onRaiseHandRequestDeleted()
        } else {
            view.onRaiseHandRequestDeleteError()
        }
    }
}
