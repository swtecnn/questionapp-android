package com.swtecnn.questionapp.presentation.presenters.authentication;

import android.util.Base64
import androidx.annotation.NonNull
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.android.gms.vision.barcode.Barcode
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.swtecnn.questionapp.domain.Role
import com.swtecnn.questionapp.domain.interactors.authentication.AuthenticationInteractor
import com.swtecnn.questionapp.domain.model.SocialEvent
import com.swtecnn.questionapp.domain.routers.authentication.AuthenticationRouter
import com.swtecnn.questionapp.presentation.presenters.AbstractPresenter
import com.swtecnn.questionapp.security.SecureCodeCipher
import com.swtecnn.questionapp.ui.barcode.model.BarcodeData
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import timber.log.Timber
import java.io.UnsupportedEncodingException

class AuthenticationPresenterImpl(aView: AuthenticationPresenter.View,
                                  private val mAuthenticationInteractor: AuthenticationInteractor,
                                  private val mRouter: AuthenticationRouter,
                                  private val mCipher: SecureCodeCipher) : AbstractPresenter<AuthenticationPresenter.View>(aView),
        AuthenticationPresenter, AuthenticationInteractor.Callback {

    private lateinit var mBarcodeData: BarcodeData
    private var mSocialEvent: SocialEvent? = null

    init {
        mAuthenticationInteractor.setCallback(this)
    }

    override fun onBarcode(aBarcode: Barcode) {
        var value = aBarcode.rawValue
        if (!value.isNullOrEmpty()) {
            try {
                value = String(Base64.decode(value, Base64.DEFAULT), Charsets.UTF_8)
            } catch (e: UnsupportedEncodingException) {
                Timber.e(e, "Failed to decode QR data")
                return
            }
            val json = Json(JsonConfiguration.Stable)
            try {
                mBarcodeData = json.parse(BarcodeData.serializer(), value)
            } catch (aException: Exception) {
                view.onInvalidQrCodeScanned()
                return
            }
            Timber.i("Recognized eventId: %s", mBarcodeData.eventId)
            showProgress()
            mAuthenticationInteractor.obtainSocialEvent(mBarcodeData.eventId)
        }
    }

    override fun onSocialEventReceived(aSocialEvent: SocialEvent) {
        hideProgress()
        mSocialEvent = aSocialEvent
        if (mBarcodeData.password.isEmpty()) {
            Timber.i("We act as a Listener")
            if (aSocialEvent.archived) {
                mAuthenticationInteractor.setRole(Role.VIEWER)
                mRouter.showEventInfo(mBarcodeData.eventId)
            } else {
                mAuthenticationInteractor.setRole(if (aSocialEvent.archived) Role.VIEWER else Role.CLIENT)
                mRouter.startListener(mBarcodeData.eventId)
            }
        } else {
            Timber.i("We act as a Lecturer")
            mAuthenticationInteractor.setRole(Role.ADMIN)
            mRouter.enterLecturerPin()
        }
    }

    override fun onPinCodeEntered(aPinCode: String) {
        Timber.i("Lecturer pin entered: %s", aPinCode)
        if (mSocialEvent != null && aPinCode.isNotEmpty() && mCipher.validate(aPinCode, mBarcodeData.password,
                        mSocialEvent!!.password)) {
            mRouter.startLecturer(mBarcodeData.eventId)
        } else {
            view.onLecturerPinNotValid()
        }
    }

    override fun create() {
        resetRole()
    }

    override fun start() {
        getUserName()
        fetchFcmToken()
    }

    override fun resetRole() {
        mAuthenticationInteractor.setRole(Role.VIEWER)
    }

    override fun onEditNameClicked() {
        view.showEditNameDialog()
    }

    override fun scanQrCodeClicked() {
        mRouter.startBarcodeScanner()
    }

    override fun saveUserName(aName: String, aDefaultName: String) {
        mAuthenticationInteractor.saveUserName(if (aName.isEmpty()) aDefaultName else aName)
    }

    override fun getUserName() {
        mAuthenticationInteractor.obtainUserName()
    }

    private fun fetchFcmToken() {
        FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(object : OnCompleteListener<InstanceIdResult> {
                    override fun onComplete(@NonNull task: Task<InstanceIdResult>) {
                        if (!task.isSuccessful) {
                            Timber.e(task.exception, "Failed to fetch FCM token: %s", task.exception.toString())
                            return
                        }

                        // Get new Instance ID token
                        val token = task.result?.token
                        if (token != null && token.isNotBlank()) {
                            mAuthenticationInteractor.saveFcmToken(token)
                        }
                    }
                })
    }

    override fun onFcmTokenReceived(aToken: String) {
        Timber.d("FCM token obtained: %s", aToken)
    }

    override fun onUserNameReceived(aUserName: String) {
        view.updateUserNameView(aUserName)
    }

    override fun onUserNameSavedSuccess(aUserName: String) {
        view.updateUserNameView(aUserName)
    }
}
