package com.swtecnn.questionapp.presentation.presenters

import android.content.Context
import com.swtecnn.questionapp.application.BaseApplication
import com.swtecnn.questionapp.presentation.presenters.authentication.AuthenticationPresenter
import com.swtecnn.questionapp.presentation.presenters.dashboard.DashboardPresenter
import com.swtecnn.questionapp.presentation.presenters.raisehand.RaiseHandPresenter
import com.swtecnn.questionapp.presentation.presenters.socialevent.admin.SocialEventAdminPresenter
import com.swtecnn.questionapp.presentation.presenters.socialevent.question.SocialEventQuestionsPresenter
import com.swtecnn.questionapp.presentation.presenters.socialevent.client.SocialEventClientPresenter
import com.swtecnn.questionapp.presentation.presenters.socialevent.info.SocialEventInfoPresenter

class PresenterFactory {

    companion object {
        fun getAuthenticationPresenter(view: AuthenticationPresenter.View, context: Context): AuthenticationPresenter {
            return BaseApplication.getAppComponent().getAuthenticationPresenterBuilder().setActivityContext(context).setView(view).build().presenter
        }

        fun getSocialEventClientPresenter(view: SocialEventClientPresenter.View, context: Context): SocialEventClientPresenter {
            return BaseApplication.getAppComponent().getSocialEventClientPresenterBuilder().setActivityContext(context).setView(view).build().presenter
        }

        fun getSocialEventAdminPresenter(view: SocialEventAdminPresenter.View, context: Context): SocialEventAdminPresenter {
            return BaseApplication.getAppComponent().getSocialEventAdminPresenterBuilder().setActivityContext(context).setView(view).build().presenter
        }

        fun getSocialEventInfoPresenter(view: SocialEventInfoPresenter.View, context: Context): SocialEventInfoPresenter {
            return BaseApplication.getAppComponent().getSocialEventInfoPresenterBuilder().setActivityContext(context).setView(view).build().presenter
        }

        fun getSocialEventQuestionsPresenter(view: SocialEventQuestionsPresenter.View, context: Context): SocialEventQuestionsPresenter {
            return BaseApplication.getAppComponent().getSocialEventQuestionsPresenterBuilder().setActivityContext(context).setView(view).build().presenter
        }

        fun getDashboardPresenter(view: DashboardPresenter.View, context: Context): DashboardPresenter {
            return BaseApplication.getAppComponent().getDashboardPresenterBuilder().setActivityContext(context).setView(view).build().presenter
        }

        fun getRaiseHandPresenter(view: RaiseHandPresenter.View, context: Context): RaiseHandPresenter {
            return BaseApplication.getAppComponent().getRaiseHandPresenterBuilder().setActivityContext(context).setView(view).build().presenter
        }
    }
}