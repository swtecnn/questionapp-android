package com.swtecnn.questionapp.presentation.presenters.socialevent.question

import com.swtecnn.questionapp.domain.Role
import com.swtecnn.questionapp.domain.interactors.socialevent.question.SocialEventQuestionsInteractor
import com.swtecnn.questionapp.domain.model.Question
import com.swtecnn.questionapp.domain.model.QuestionVote
import com.swtecnn.questionapp.domain.routers.socialevent.question.SocialEventQuestionsRouter
import com.swtecnn.questionapp.presentation.presenters.AbstractPresenter
import timber.log.Timber

class SocialEventQuestionsPresenterImpl(aView: SocialEventQuestionsPresenter.View,
                                        private val mSocialEventQuestionsInteractor: SocialEventQuestionsInteractor,
                                        private val mRouter: SocialEventQuestionsRouter): AbstractPresenter<SocialEventQuestionsPresenter.View>(aView),
        SocialEventQuestionsPresenter, SocialEventQuestionsInteractor.Callback {

    private var mQuestions: MutableList<Question> = mutableListOf()
    private lateinit var mQuestionsLocal: Set<String>
    private lateinit var mUserVotes: Set<String>
    private lateinit var mSocialEventId: String
    private lateinit var mRole: Role

    init {
        mSocialEventQuestionsInteractor.setCallback(this)
    }

    override fun fetchQuestions(aSocialEventId: String) {
        showProgress()
        mSocialEventId = aSocialEventId
        mSocialEventQuestionsInteractor.fetchQuestions(aSocialEventId)
    }

    override fun onQuestionsReceivedRemote(aQuestions: List<Question>) {
        mQuestions.clear()
        mQuestions.addAll(aQuestions)
        mSocialEventQuestionsInteractor.obtainQuestions()
    }

    override fun onQuestionsReceivedLocal(aQuestions: Set<String>) {
        mQuestionsLocal = aQuestions
        mSocialEventQuestionsInteractor.obtainVotedQuestions()
    }

    override fun onUserVotesReceived(aUserVotes: Set<String>) {
        hideProgress()
        mUserVotes = aUserVotes
        // we are all set here including the following: all social event questions,
        // user own questions ids, user votes and the current role
        view.onQuestionsDataReceived(mQuestions, mQuestionsLocal, mUserVotes)
    }

    override fun deleteQuestion(aQuestion: Question) {
        showProgress()
        mSocialEventQuestionsInteractor.deleteQuestion(aQuestion.id)
    }

    override fun answerGiven(aQuestion: Question) {
        showProgress()
        mSocialEventQuestionsInteractor.archiveQuestion(aQuestion.id)
    }

    override fun withdrawQuestion(aQuestion: Question) {
        showProgress()
        mSocialEventQuestionsInteractor.deleteQuestion(aQuestion.id)
    }

    override fun voteQuestion(aQuestion: Question) {
        showProgress()
        mSocialEventQuestionsInteractor.updateVoteQuestion(QuestionVote(aQuestion.id, 1))
    }

    override fun unVoteQuestion(aQuestion: Question) {
        showProgress()
        mSocialEventQuestionsInteractor.updateVoteQuestion(QuestionVote(aQuestion.id, -1))
    }

    override fun onQuestionUnVotedRemote(aQuestionId: String) {
        hideProgress()
        mSocialEventQuestionsInteractor.removeVoteQuestionLocal(aQuestionId)
        fetchQuestions(mSocialEventId)
    }

    override fun onQuestionDeletedSuccessfully(aQuestionId: String) {
        Timber.d("Question: %s was deleted on remote", aQuestionId)
        mSocialEventQuestionsInteractor.deleteQuestionLocal(aQuestionId, mRole == Role.ADMIN)
    }

    override fun onQuestionDeletedLocal(aQuestionId: String) {
        hideProgress()
        Timber.d("Question: %s was fully deleted", aQuestionId)
        mQuestions.removeAt(mQuestions.indexOfFirst { q -> q.id == aQuestionId })
        view.onQuestionsDataReceived(mQuestions, mQuestionsLocal, mUserVotes)
    }

    override fun onFailedToDeleteQuestion(aQuestionId: String) {
        hideProgress()
        Timber.e("Failed to delete question: %s", aQuestionId)
        view.onFailedToDeleteQuestion()
    }

    override fun onQuestionVotedRemote(aQuestionId: String) {
        Timber.d("Question: %s was voted on remote", aQuestionId)
        mSocialEventQuestionsInteractor.updateVoteQuestionLocal(QuestionVote(aQuestionId, 1))
    }

    override fun onQuestionVotedSuccessfully(aQuestionId: String) {
        Timber.d("Question: %s was voted successfully", aQuestionId)
        hideProgress()
        fetchQuestions(mSocialEventId)
    }

    override fun onFailedToVoteQuestion(aQuestionId: String) {
        hideProgress()
    }

    override fun onQuestionArchivedSuccessfully(aQuestionId: String) {
        hideProgress()
        val index :Int = mQuestions.indexOfFirst { q -> q.id == aQuestionId }
        if (index > 0) {
            val question: Question = mQuestions[index]
            question.archived = true;
            view.onQuestionsDataReceived(mQuestions, mQuestionsLocal, mUserVotes)
        }
    }

    override fun onFailedToArchiveQuestion(aQuestionId: String) {
        hideProgress()
    }

    override fun setRole(role: Role) {
        mRole = role
    }

}
