package com.swtecnn.questionapp.presentation.presenters.socialevent.client

import com.swtecnn.questionapp.domain.model.Question
import com.swtecnn.questionapp.domain.model.SocialEvent
import com.swtecnn.questionapp.presentation.presenters.BasePresenter
import com.swtecnn.questionapp.ui.BaseView

interface SocialEventClientPresenter : BasePresenter {

    fun saveSocialEvent(aSocialEventId: String)
    fun onAskQuestionClicked()
    fun postQuestion(aQuestion: Question)
    fun onRaiseHandClicked()
    fun onEventInfoClicked()
    fun onShowAllQuestionClicked()
    fun getSocialEventInfo(aSocialEventId: String)
    fun getUserName()
    fun deleteRaiseHandRequest(aRaiseHandRequestId: String)

    interface View : BaseView {
        fun onQuestionPosted()
        fun onQuestionPostFailed()
        fun onSocialEventLoaded(aEvent: SocialEvent)
        fun onRaiseHandRequestDeleted()
        fun onRaiseHandRequestDeleteError()
    }
}
