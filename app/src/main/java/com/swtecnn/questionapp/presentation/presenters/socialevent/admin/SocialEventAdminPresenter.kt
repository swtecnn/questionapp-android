package com.swtecnn.questionapp.presentation.presenters.socialevent.admin

import com.swtecnn.questionapp.domain.model.SocialEvent
import com.swtecnn.questionapp.presentation.presenters.BasePresenter
import com.swtecnn.questionapp.ui.BaseView

interface SocialEventAdminPresenter : BasePresenter {

    fun showLectureInfo(aSocialEventId: String)
    fun archiveSocialEvent(aSocialEventId: String)
    fun getSocialEventInfo(socialEventId: String)
    fun saveSocialEvent(aSocialEventId: String)
    fun onListenQuestionClicked()
    fun onShowAllQuestionClicked()
    fun deleteRaiseHandRequest(aRaiseHandRequestId: String)

    interface View : BaseView {
        fun showAllQuestions(aSocialInfoId: String)
        fun onSocialEventArchiveFailed()
        fun onSocialEventArchived()
        fun showNoQuestionsToListen()
        fun showLecturerRaiseHand(aListenerName: String, aRaiseHandRequestId: String, aTimestamp: Long)
        fun onSocialEventLoaded(event: SocialEvent)
        fun onQuestionCountChanged(count: Int)
        fun onRaiseHandRequestsCountChanged(count: Int)
    }
}
