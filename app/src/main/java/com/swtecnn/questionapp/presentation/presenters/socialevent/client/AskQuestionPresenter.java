package com.swtecnn.questionapp.presentation.presenters.socialevent.client;

import com.swtecnn.questionapp.domain.model.Question;

public interface AskQuestionPresenter {
    void onAskQuestionClicked(Question question);

    interface View {
        void onCorrectData(Question question);
        void showToastFillTheDescription();
    }
}
