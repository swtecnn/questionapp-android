package com.swtecnn.questionapp.presentation.presenters.dashboard

import com.swtecnn.questionapp.domain.model.SocialEvent
import com.swtecnn.questionapp.presentation.presenters.BasePresenter
import com.swtecnn.questionapp.ui.BaseView

interface DashboardPresenter: BasePresenter {

    fun onViewAllUserEvents()
    fun onSocialEventSelected(aSocialEventId: String)

    interface View: BaseView {
        fun onClientSocialEventsLoaded(aSocialEvents: List<SocialEvent>)
    }
}
