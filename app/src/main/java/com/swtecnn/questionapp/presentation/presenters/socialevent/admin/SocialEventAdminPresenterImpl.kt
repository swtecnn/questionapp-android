package com.swtecnn.questionapp.presentation.presenters.socialevent.admin

import com.swtecnn.questionapp.domain.interactors.socialevent.admin.SocialEventAdminInteractor
import com.swtecnn.questionapp.domain.model.SocialEvent
import com.swtecnn.questionapp.domain.repository.question.IQuestionRepositoryListener
import com.swtecnn.questionapp.domain.repository.raisehand.IRaiseHandRepositoryListener
import com.swtecnn.questionapp.domain.routers.socialevent.admin.SocialEventAdminRouter
import com.swtecnn.questionapp.presentation.presenters.AbstractPresenter
import com.swtecnn.questionapp.domain.model.RaiseHandRequest

class SocialEventAdminPresenterImpl(aView: SocialEventAdminPresenter.View,
                                    private val mSocialEventAdminInteractor: SocialEventAdminInteractor,
                                    private val mSocialEventAdminRouter: SocialEventAdminRouter) : AbstractPresenter<SocialEventAdminPresenter.View>(aView),
        SocialEventAdminPresenter, SocialEventAdminInteractor.Callback {

    lateinit var socialEventId: String
    private var mRaiseHandCount: Int = 0

    private val questionCountListener : IQuestionRepositoryListener
    private val raiseHandCountListener: IRaiseHandRepositoryListener

    init {
        questionCountListener = IQuestionRepositoryListener { _: String, i: Int ->
            view.onQuestionCountChanged(i)
        }
        raiseHandCountListener = IRaiseHandRepositoryListener { _: String, i: Int ->
            mRaiseHandCount = i
            view.onRaiseHandRequestsCountChanged(i)
        }
        mSocialEventAdminInteractor.setCallback(this)
    }

    override fun resume() {
        super.resume()
        mSocialEventAdminInteractor.addQuestionCountListener(questionCountListener, socialEventId)
        mSocialEventAdminInteractor.addRaiseHandCountListener(raiseHandCountListener, socialEventId)
    }

    override fun pause() {
        super.pause()
        mSocialEventAdminInteractor.removeQuestionCountListener(questionCountListener)
        mSocialEventAdminInteractor.removeRaiseHandListener(raiseHandCountListener)
    }

    override fun showLectureInfo(aSocialEventId: String) {
        mSocialEventAdminRouter.showLectureInformation(aSocialEventId)
    }

    override fun saveSocialEvent(aSocialEventId: String) {
        mSocialEventAdminInteractor.archiveSocialEventLocal(aSocialEventId)
    }

    override fun archiveSocialEvent(aSocialEventId: String) {
        showProgress()
        mSocialEventAdminInteractor.archiveSocialEventRemote(aSocialEventId)
    }

    override fun onSocialEventArchiveFailed(aSocialEventId: String) {
        hideProgress()
        view.onSocialEventArchiveFailed()
    }

    override fun onSocialEventArchivedRemote(aSocialEventId: String) {
        hideProgress()
        view.onSocialEventArchived()
    }

    override fun onSocialEventArchivedLocal(aSocialEventId: String) {

    }

    override fun deleteRaiseHandRequest(aRaiseHandRequestId: String) {
        showProgress()
        mSocialEventAdminInteractor.removeRaiseHandRequest(aRaiseHandRequestId)
    }

    override fun getSocialEventInfo(aSocialEventId: String) {
        socialEventId = aSocialEventId
        mSocialEventAdminInteractor.loadSocialEvent(socialEventId)
    }

    override fun onSocialEventLoaded(event: SocialEvent) {
        view.onSocialEventLoaded(event)
    }

    override fun onShowAllQuestionClicked() {
        mSocialEventAdminRouter.showAllQuestions(socialEventId)
    }

    override fun onListenQuestionClicked() {
        showProgress()
        if (mRaiseHandCount == 0) {
            hideProgress()
            view.showNoQuestionsToListen()
        } else {
            mSocialEventAdminInteractor.getRaiseHandRequests(socialEventId)
        }
    }

    override fun onRaiseHandRequestsLoaded(requests: List<RaiseHandRequest>) {
        if (requests.isNotEmpty()) {
            requests.sortedBy { request -> request.timestamp }
            val request = requests[0]
            mSocialEventAdminInteractor.sendPush(request)
            view.showLecturerRaiseHand(request.name, request.id, request.timestamp)
        }
    }

    override fun onRaiseHandRequestDeleted() {
        hideProgress()
    }

    override fun onRaiseHandRequestDeleteFailed() {
        hideProgress()
    }

    override fun onPushSent() {
        hideProgress()
    }

    override fun onPushFailed() {
        hideProgress()
    }
}
