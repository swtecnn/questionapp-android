package com.swtecnn.questionapp.presentation.presenters;

import com.swtecnn.questionapp.ui.BaseView;

public abstract class AbstractPresenter<T extends BaseView> implements BasePresenter {

    protected T view;
    private boolean progressBarEnabled = true;

    public AbstractPresenter(T view) {
        this.view = view;
    }

    @Override
    public void create() {

    }

    @Override
    public void start() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onNetworkAvailable() {

    }

    @Override
    public void onNetworkUnavailable() {
        view.showNoInternetAlert();
    }

    protected boolean isNetworkAvailable() {
        return view.isNetworkAvailable();
    }

    @Override
    public void showProgress() {
        if (progressBarEnabled) {
            view.getProgressTrackingService().addTracking();
            view.showProgress();
        }
    }

    @Override
    public void hideProgress() {
        if (progressBarEnabled) {
            view.getProgressTrackingService().removeTracking();

            if (view.getProgressTrackingService().canHide()) {
                view.hideProgress();
            }
        }
    }
}
