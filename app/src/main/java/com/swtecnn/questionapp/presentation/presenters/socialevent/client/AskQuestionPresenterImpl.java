package com.swtecnn.questionapp.presentation.presenters.socialevent.client;

import android.content.res.Resources;

import androidx.annotation.NonNull;

import com.swtecnn.questionapp.R;
import com.swtecnn.questionapp.domain.model.Question;

import org.apache.commons.lang3.StringUtils;

import timber.log.Timber;

public class AskQuestionPresenterImpl implements AskQuestionPresenter {

    private AskQuestionPresenter.View view;
    private Resources resources;

    public AskQuestionPresenterImpl(AskQuestionPresenter.View view, Resources resources) {
        this.view = view;
        this.resources = resources;
    }

    @Override
    public void onAskQuestionClicked(@NonNull Question question) {
        boolean resultSuccess = true;
        if (StringUtils.isEmpty(question.socialEventId)) {
            Timber.w("onAskQuestionClicked mSocialEventId is empty!");
            resultSuccess = false;
        } else if (StringUtils.isEmpty(question.description)) {
            view.showToastFillTheDescription();
            resultSuccess = false;
        } else if (StringUtils.isEmpty(question.name)) {
            question.name = resources.getString(R.string.incognito);
        }
        if (resultSuccess) {
            view.onCorrectData(question);
        }
    }
}
