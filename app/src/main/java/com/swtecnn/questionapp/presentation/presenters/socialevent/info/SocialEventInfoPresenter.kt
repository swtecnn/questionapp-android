package com.swtecnn.questionapp.presentation.presenters.socialevent.info

import com.swtecnn.questionapp.domain.Role
import com.swtecnn.questionapp.domain.model.SocialEvent
import com.swtecnn.questionapp.presentation.presenters.BasePresenter
import com.swtecnn.questionapp.ui.BaseView

interface SocialEventInfoPresenter : BasePresenter {

    fun getLectureInfo(aSocialEventId: String)
    fun showSocialEventQuestions(aSocialEventId: String)

    interface View : BaseView {
        fun onSocialEventReceived(aSocialEvent: SocialEvent)
        fun onRoleReceived(aRole: Role)
        fun onFailedToGetSocialEvent()
    }
}
