package com.swtecnn.questionapp.domain.model;

import com.swtecnn.questionapp.domain.model.base.BaseModel;

import java.util.HashMap;
import java.util.Map;

public class RaiseHandRequest implements BaseModel, Comparable<RaiseHandRequest>  {

    public static String SOCIAL_EVENT_ID = "socialEventId";
    private static String TOKEN = "pushNotificationToken";
    private static String NAME = "name";
    private static String TIMESTAMP = "timestamp";

    public String id;
    public String socialEventId;
    public String pushNotificationToken;
    public String name;
    public Long timestamp;

    public RaiseHandRequest() {

    }

    public RaiseHandRequest(String socialEventId, String pushNotificationToken, String name, Long timestamp) {
        this.socialEventId = socialEventId;
        this.pushNotificationToken = pushNotificationToken;
        this.name = name;
        this.timestamp = timestamp;
    }

    @Override
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put(SOCIAL_EVENT_ID, socialEventId);
        result.put(TOKEN, pushNotificationToken);
        result.put(NAME, name);
        result.put(TIMESTAMP, timestamp);

        return result;
    }

    @Override
    public RaiseHandRequest setId(String id) {
        this.id = id;
        return this;
    }

    @Override
    public int compareTo(RaiseHandRequest o) {
        return this.timestamp.compareTo(o.timestamp);
    }
}
