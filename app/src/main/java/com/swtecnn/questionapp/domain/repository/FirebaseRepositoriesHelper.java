package com.swtecnn.questionapp.domain.repository;

import android.os.Build;

import com.google.android.gms.tasks.OnCanceledListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.swtecnn.questionapp.domain.model.base.BaseModel;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.RequiresApi;
import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import timber.log.Timber;

public class FirebaseRepositoriesHelper {

    public static String ARCHIVED = "archived";

    public static Single<String> addObjectToDatabase(DatabaseReference reference, BaseModel model) {
        SingleOnSubscribe<String> onSubscribe = emitter -> {
            String key = reference.push().getKey();
            Map<String, Object> values = model.toMap();
            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put("/" + key, values);
            reference.updateChildren(childUpdates).addOnSuccessListener(aVoid -> {
                // Write was successful!
                Timber.d("addObjectToDatabase Successfully add data");
                emitter.onSuccess(key);
            }).addOnFailureListener(e -> {
                // Write failed
                Timber.w(e, "addObjectToDatabase Failed to add value.");
                emitter.onSuccess(null);
            });
        };
        return Single.create(onSubscribe);
    }

    public static Single<DataSnapshot> getSingleData(DatabaseReference reference, String id) {
        SingleOnSubscribe<DataSnapshot> onSubscribe =
                e -> reference.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NotNull DataSnapshot dataSnapshot) {
                        // This method is called once with the initial value and again
                        // whenever data at this location is updated.
                        Timber.d("getSingleData Successfully read data");
                        e.onSuccess(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(@NotNull DatabaseError error) {
                        // Failed to read value
                        Timber.w(error.toException(), "Failed to read value.");
                        e.onSuccess(null);
                    }
                });
        return Single.create(onSubscribe);
    }

    public static Single<Object> deleteObjectFromDatabase(DatabaseReference reference, String id) {
        final SingleOnSubscribe<Object> singleOnSubscribe = e -> reference.child(id)
                .removeValue().addOnCompleteListener(task -> {
                    final Exception ex = task.getException();
                    e.onSuccess(ex == null ? new Object() : ex);
                });
        return Single.create(singleOnSubscribe);
    }

    public static Single<Object> archiveObject(DatabaseReference reference, String id) {
        final SingleOnSubscribe<Object> singleOnSubscribe = e -> {
            reference.child(id).child(ARCHIVED)
                    .setValue(true).addOnCompleteListener(task -> {
                final Exception ex = task.getException();
                e.onSuccess(ex == null ? new Object() : ex);
            });
        };
        return Single.create(singleOnSubscribe);
    }

    public static Single<Iterable<DataSnapshot>> getListData(DatabaseReference reference, String id, String name) {
        SingleOnSubscribe<Iterable<DataSnapshot>> onSubscribe = e -> {
            Query query = id != null && name != null ? reference.orderByChild(name).equalTo(id)
                    : reference.orderByKey();
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onDataChange(@NotNull DataSnapshot dataSnapshot) {
                    e.onSuccess(dataSnapshot.getChildren());
                }

                @Override
                public void onCancelled(@NotNull DatabaseError error) {
                    // Failed to read value
                    Timber.w(error.toException(), "getListData Failed to read value.");
                    e.onSuccess(null);
                }
            });
        };
        return Single.create(onSubscribe);
    }

    public static Single<Iterable<DataSnapshot>> getListData(DatabaseReference reference) {
        return getListData(reference, null, null);
    }

    public static Single<Integer> getChildrenCount(DatabaseReference reference, String id, String name) {
        SingleOnSubscribe<Integer> onSubscribe = e -> {
            Query query = reference.orderByChild(name).equalTo(id);
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NotNull DataSnapshot dataSnapshot) {
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.
                    e.onSuccess((int) dataSnapshot.getChildrenCount());
                }

                @Override
                public void onCancelled(@NotNull DatabaseError error) {
                    // Failed to read value
                    Timber.w(error.toException(), "getChildrenCount Failed to read value.");
                    e.onSuccess(null);
                }
            });
        };
        return Single.create(onSubscribe);
    }
}
