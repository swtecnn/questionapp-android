package com.swtecnn.questionapp.domain.repository.raisehand;

import com.swtecnn.questionapp.domain.model.RaiseHandRequest;
import com.swtecnn.questionapp.domain.model.RaiseHandRequestStatus;

import java.util.List;

public interface IRaiseHandRepository {
    String addRaiseHandRequest(RaiseHandRequest request);
    boolean deleteRaiseHandRequest(String raiseHandRequestId);
    Integer getRaiseHandRequestCount(String socialEventId);
    RaiseHandRequestStatus getRaiseHandRequestStatus(String raiseHandRequestId);
    List<RaiseHandRequest> getRaiseHandRequests(String socialEventId);
    void addListener(IRaiseHandRepositoryListener listener, String socialEventId);
    void removeListener(IRaiseHandRepositoryListener listener);
    boolean sendPush(String aToken, String aListenerName);
}
