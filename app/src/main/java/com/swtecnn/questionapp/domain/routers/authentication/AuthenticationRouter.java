package com.swtecnn.questionapp.domain.routers.authentication;

public interface AuthenticationRouter {
    int ENTER_PIN_CODE_REQUEST_CODE = 2003;
    int BARCODE_SCANNER_REQUEST_CODE = 2002;
    void startListener(String aSocialEventId);
    void startLecturer(String aSocialEventId);
    void showEventInfo(String aSocialEventId);
    void enterLecturerPin();
    void startBarcodeScanner();
}
