package com.swtecnn.questionapp.domain.interactors.base;

import com.swtecnn.questionapp.domain.executor.Executor;
import com.swtecnn.questionapp.domain.executor.MainThread;

public abstract class AbstractInteractor {

    protected Executor threadExecutor;
    protected MainThread mainThread;

    public AbstractInteractor(Executor executor, MainThread mainThread) {
        threadExecutor = executor;
        this.mainThread = mainThread;
    }
}