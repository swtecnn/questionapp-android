package com.swtecnn.questionapp.domain.interactors.socialevent.admin

import com.swtecnn.questionapp.domain.model.RaiseHandRequest
import com.swtecnn.questionapp.domain.model.SocialEvent
import com.swtecnn.questionapp.domain.repository.question.IQuestionRepositoryListener
import com.swtecnn.questionapp.domain.repository.raisehand.IRaiseHandRepositoryListener

interface SocialEventAdminInteractor {
    fun setCallback(aCallback: Callback)

    fun archiveSocialEventRemote(aSocialEventId: String)
    fun archiveSocialEventLocal(aSocialEventId: String)

    fun loadSocialEvent(socialEventId: String)

    fun addQuestionCountListener(listener: IQuestionRepositoryListener, socialEventId: String)
    fun removeQuestionCountListener(listener: IQuestionRepositoryListener)

    fun addRaiseHandCountListener(listener: IRaiseHandRepositoryListener, socialEventId: String)
    fun removeRaiseHandListener(listener: IRaiseHandRepositoryListener)

    fun getRaiseHandRequests(socialEventId: String)
    fun removeRaiseHandRequest(requestId: String)
    fun sendPush(request: RaiseHandRequest)

    interface Callback {
        fun onSocialEventArchiveFailed(aSocialEventId: String)
        fun onSocialEventArchivedRemote(aSocialEventId: String)
        fun onSocialEventArchivedLocal(aSocialEventId: String)
        fun onSocialEventLoaded(event: SocialEvent)
        fun onRaiseHandRequestsLoaded(requests: List<RaiseHandRequest>)
        fun onRaiseHandRequestDeleted()
        fun onRaiseHandRequestDeleteFailed()
        fun onPushSent()
        fun onPushFailed()
    }
}