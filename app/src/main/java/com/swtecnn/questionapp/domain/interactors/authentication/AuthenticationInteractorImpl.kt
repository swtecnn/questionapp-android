package com.swtecnn.questionapp.domain.interactors.authentication

import com.swtecnn.questionapp.domain.Role
import com.swtecnn.questionapp.domain.executor.Executor
import com.swtecnn.questionapp.domain.executor.MainThread
import com.swtecnn.questionapp.domain.interactors.base.AbstractInteractor
import com.swtecnn.questionapp.domain.repository.firebase.FirebaseRepository
import com.swtecnn.questionapp.domain.repository.socialevent.ISocialEventRepository
import com.swtecnn.questionapp.domain.repository.user.UserPreferenceRepository

class AuthenticationInteractorImpl(aExecutor: Executor, aMainThread: MainThread,
                                   private val mUserRepository: UserPreferenceRepository,
                                   private val mFirebaseRepository: FirebaseRepository,
                                   private val mSocialEventRepository: ISocialEventRepository) : AbstractInteractor(aExecutor, aMainThread), AuthenticationInteractor {

    private lateinit var mCallback: AuthenticationInteractor.Callback

    override fun setCallback(aCallback: AuthenticationInteractor.Callback) {
        mCallback = aCallback
    }

    override fun saveUserName(aUserName: String) {
        threadExecutor.execute {
            mUserRepository.userName = aUserName
            mainThread.post { mCallback.onUserNameSavedSuccess(aUserName) }
        }
    }

    override fun obtainUserName() {
        threadExecutor.execute {
            val name = mUserRepository.userName
            mainThread.post { mCallback.onUserNameReceived(name) }
        }
    }

    override fun saveFcmToken(aToken: String) {
        threadExecutor.execute {
            mFirebaseRepository.setFcmToken(aToken)
        }
    }

    override fun obtainFcmToken() {
        threadExecutor.execute {
            val token = mFirebaseRepository.getFcmToken()
            mainThread.post { mCallback.onFcmTokenReceived(token) }
        }
    }

    override fun setRole(aRole: Role) {
        threadExecutor.execute {
            mUserRepository.userRole = aRole
        }
    }

    override fun obtainSocialEvent(aSocialEventId: String) {
        threadExecutor.execute {
            val socialEvent = mSocialEventRepository.getSocialEvent(aSocialEventId)
            mainThread.post { mCallback.onSocialEventReceived(socialEvent) }
        }
    }
}