package com.swtecnn.questionapp.domain.interactors.raisehand

import com.swtecnn.questionapp.domain.model.RaiseHandRequestStatus
import com.swtecnn.questionapp.domain.model.SocialEvent
import com.swtecnn.questionapp.domain.model.SocialEventRole
import java.lang.Exception

interface RaiseHandInteractor {
    fun setCallback(aCallback: Callback)

    interface Callback {
        fun onRaiseHandStatusRefreshed(aRaiseHandRequestStatus: RaiseHandRequestStatus)
    }

    fun refreshRaiseHandStatus(aRaiseHandRequestId: String)
}