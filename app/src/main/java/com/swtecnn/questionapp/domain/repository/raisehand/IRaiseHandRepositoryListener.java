package com.swtecnn.questionapp.domain.repository.raisehand;

public interface IRaiseHandRepositoryListener {
    void requestCountChanged(String socialEventId, int count);
}
