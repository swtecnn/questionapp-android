package com.swtecnn.questionapp.domain.executor;

public interface Executor {

    void execute(Runnable runnable);
}