package com.swtecnn.questionapp.domain.interactors.raisehand

import com.swtecnn.questionapp.domain.executor.Executor
import com.swtecnn.questionapp.domain.executor.MainThread
import com.swtecnn.questionapp.domain.interactors.base.AbstractInteractor
import com.swtecnn.questionapp.domain.repository.raisehand.IRaiseHandRepository

class RaiseHandInteractorImpl(aExecutor: Executor, aMainThread: MainThread,
                              private val mRaiseHandRepository: IRaiseHandRepository)
    : AbstractInteractor(aExecutor, aMainThread), RaiseHandInteractor {

    private lateinit var mCallback: RaiseHandInteractor.Callback

    override fun setCallback(aCallback: RaiseHandInteractor.Callback) {
        mCallback = aCallback
    }

    override fun refreshRaiseHandStatus(aRaiseHandRequestId: String) {
        threadExecutor.execute {
            val status = mRaiseHandRepository.getRaiseHandRequestStatus(aRaiseHandRequestId)
            mainThread.post { mCallback.onRaiseHandStatusRefreshed(status) }
        }
    }

}