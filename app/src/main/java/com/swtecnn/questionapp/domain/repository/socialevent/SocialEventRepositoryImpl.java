package com.swtecnn.questionapp.domain.repository.socialevent;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.swtecnn.questionapp.domain.model.SocialEvent;
import com.swtecnn.questionapp.domain.repository.FirebaseRepositoriesHelper;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class SocialEventRepositoryImpl implements ISocialEventRepository {

    private static final String SOCIAL_INFO_TABLE = "social_events";

    private DatabaseReference myRef;

    public SocialEventRepositoryImpl() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference().child(SOCIAL_INFO_TABLE);
    }

    @Override
    public String addSocialEvent(SocialEvent event) {
        return FirebaseRepositoriesHelper.addObjectToDatabase(myRef, event).blockingGet();
    }

    @Override
    public boolean archiveSocialEvent(String key) {
        final Object result = FirebaseRepositoriesHelper
                .archiveObject(myRef, key).blockingGet();
        return !(result instanceof Exception);
    }

    @Override
    public SocialEvent getSocialEvent(String id) {
        return FirebaseRepositoriesHelper.getSingleData(myRef, id)
                .map(snapshot -> {
                    SocialEvent event = snapshot.getValue(SocialEvent.class);
                    if (event == null) {
                        Timber.d("getSocialEvent event = null!");
                        event = new SocialEvent();
                    }
                    return event.setId(id);
                })
                .blockingGet();
    }

    @Override
    public List<SocialEvent> getAllSocialEvents() {
        return FirebaseRepositoriesHelper.getListData(myRef)
                .map(snapshots -> {
                    final List<SocialEvent> socialEvents = new ArrayList<>();
                    for (final DataSnapshot snapshot : snapshots) {
                        if (snapshot != null) {
                            SocialEvent event = snapshot.getValue(SocialEvent.class);
                            if (event == null) {
                                Timber.d("getAllSocialEvents event = null!");
                                event = new SocialEvent();
                            }
                            socialEvents.add(event.setId(snapshot.getKey()));
                        }
                    }
                    return socialEvents;
                })
                .blockingGet();
    }
}
