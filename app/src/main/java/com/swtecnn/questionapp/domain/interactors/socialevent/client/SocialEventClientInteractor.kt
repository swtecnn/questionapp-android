package com.swtecnn.questionapp.domain.interactors.socialevent.client

import com.swtecnn.questionapp.domain.model.Question
import com.swtecnn.questionapp.domain.model.RaiseHandRequest
import com.swtecnn.questionapp.domain.model.RaiseHandRequestStatus
import com.swtecnn.questionapp.domain.model.SocialEvent

interface SocialEventClientInteractor {
    fun setCallback(aCallback: Callback)

    fun saveSocialEventLocally(aSocialEventId: String)
    fun postQuestion(aQuestion: Question)
    fun obtainRaiseHandData()
    fun obtainCachedRaiseHand()
    fun cacheRaiseHandReuqest(aRaiseHandId: String)
    fun getRaiseHandStatus(aRaiseHandId: String)
    fun raiseHand(aRaiseHandRequest: RaiseHandRequest)
    fun saveQuestionLocally(aQuestionId: String)
    fun deleteRaiseHandRequest(aRaiseHandId: String)

    fun loadSocialEvent(socialEventId: String)
    fun obtainUserName()

    interface Callback {
        fun onCachedRaiseHandReceived(aRaiseHandId: String)
        fun onRaiseHandStatusReceived(aRaiseHandStatus: RaiseHandRequestStatus)
        fun onRaiseHandPosted(aRaiseHandId: String)
        fun onRaiseHandDataReceived(aUserName: String, aFcmToken: String)
        fun onQuestionSuccessfullyPostedRemote(aQuestionId: String)
        fun onQuestionSuccessfullyPostedLocal()
        fun onQuestionPostFailed()
        fun onSocialEventLoaded(event: SocialEvent)
        fun onUserNameReceived(aUserName: String)
        fun onRaiseHandRequestDeleted(aSuccess: Boolean)
    }
}