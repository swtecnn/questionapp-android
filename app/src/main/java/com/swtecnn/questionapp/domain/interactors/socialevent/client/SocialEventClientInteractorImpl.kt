package com.swtecnn.questionapp.domain.interactors.socialevent.client

import com.swtecnn.questionapp.domain.Role
import com.swtecnn.questionapp.domain.executor.Executor
import com.swtecnn.questionapp.domain.executor.MainThread
import com.swtecnn.questionapp.domain.interactors.base.AbstractInteractor
import com.swtecnn.questionapp.domain.model.Question
import com.swtecnn.questionapp.domain.model.RaiseHandRequest
import com.swtecnn.questionapp.domain.repository.firebase.FirebaseRepository
import com.swtecnn.questionapp.domain.repository.question.IQuestionLocalRepository
import com.swtecnn.questionapp.domain.repository.question.IQuestionRepository
import com.swtecnn.questionapp.domain.repository.raisehand.IRaiseHandLocalRepository
import com.swtecnn.questionapp.domain.repository.raisehand.IRaiseHandRepository
import com.swtecnn.questionapp.domain.repository.socialevent.ISocialEventLocalRepository
import com.swtecnn.questionapp.domain.repository.socialevent.ISocialEventRepository
import com.swtecnn.questionapp.domain.repository.user.UserPreferenceRepository
import timber.log.Timber
import java.lang.Exception

class SocialEventClientInteractorImpl(aExecutor: Executor,
                                      aMainThread: MainThread,
                                      private val mRaiseHandLocalRepository: IRaiseHandLocalRepository,
                                      private val mRaiseHandRepository: IRaiseHandRepository,
                                      private val mQuestionsRepository: IQuestionRepository,
                                      private val mFirebaseRepository: FirebaseRepository,
                                      private val mQuestionsLocalRepository: IQuestionLocalRepository,
                                      private val mUserRepository: UserPreferenceRepository,
                                      private val mSocialEventRepository: ISocialEventRepository,
                                      private val mSocialEventLocalRepository: ISocialEventLocalRepository) :
        AbstractInteractor(aExecutor, aMainThread), SocialEventClientInteractor {

    private lateinit var mCallback: SocialEventClientInteractor.Callback

    override fun setCallback(aCallback: SocialEventClientInteractor.Callback) {
        mCallback = aCallback
    }

    override fun saveSocialEventLocally(aSocialEventId: String) {
        threadExecutor.execute {
            try {
                mSocialEventLocalRepository.archiveSocialEvent(aSocialEventId, Role.CLIENT)
            } catch (aException: Exception) {
                Timber.e(aException, "Failed to save social event locally: %s", aException.message)
            }
        }
    }

    override fun postQuestion(aQuestion: Question) {
        threadExecutor.execute {
            val questionId = mQuestionsRepository.addQuestion(aQuestion)
            mainThread.post {
                if (questionId.isNullOrEmpty()) {
                    mCallback.onQuestionPostFailed()
                } else {
                    mCallback.onQuestionSuccessfullyPostedRemote(questionId)
                }
            }
        }
    }

    override fun obtainCachedRaiseHand() {
        threadExecutor.execute {
            val raiseHandId = mRaiseHandLocalRepository.getCachedRaiseHand()
            mainThread.post { mCallback.onCachedRaiseHandReceived(raiseHandId) }
        }
    }

    override fun cacheRaiseHandReuqest(aRaiseHandId: String) {
        threadExecutor.execute {
            mRaiseHandLocalRepository.cacheRaiseHand(aRaiseHandId)
        }
    }

    override fun obtainRaiseHandData() {
        threadExecutor.execute {
            val userName = mUserRepository.userName
            val token = mFirebaseRepository.getFcmToken()
            mainThread.post { mCallback.onRaiseHandDataReceived(userName, token) }
        }
    }

    override fun raiseHand(aRaiseHandRequest: RaiseHandRequest) {
        threadExecutor.execute {
            val raiseHandId = mRaiseHandRepository.addRaiseHandRequest(aRaiseHandRequest)
            mainThread.post { mCallback.onRaiseHandPosted(raiseHandId) }
        }
    }

    override fun getRaiseHandStatus(aRaiseHandId: String) {
        threadExecutor.execute {
            val status = mRaiseHandRepository.getRaiseHandRequestStatus(aRaiseHandId)
            mainThread.post { mCallback.onRaiseHandStatusReceived(status) }
        }
    }

    override fun saveQuestionLocally(aQuestionId: String) {
        threadExecutor.execute {
            val result = mQuestionsLocalRepository.addQuestion(aQuestionId)
            mainThread.post {
                if (result) {
                    mCallback.onQuestionSuccessfullyPostedLocal()
                } else {
                    mCallback.onQuestionPostFailed()
                }
            }
        }
    }

    override fun obtainUserName() {
        threadExecutor.execute {
            val name = mUserRepository.userName
            mainThread.post { mCallback.onUserNameReceived(name) }
        }
    }

    override fun loadSocialEvent(socialEventId: String) {
        threadExecutor.execute {
            val event = mSocialEventRepository.getSocialEvent(socialEventId)
            mainThread.post {
                mCallback.onSocialEventLoaded(event)
            }
        }
    }

    override fun deleteRaiseHandRequest(aRaiseHandId: String) {
        threadExecutor.execute {
            val success = mRaiseHandRepository.deleteRaiseHandRequest(aRaiseHandId)
            mainThread.post { mCallback.onRaiseHandRequestDeleted(success) }
        }
    }
}