package com.swtecnn.questionapp.domain.repository.socialevent

import android.content.SharedPreferences
import com.swtecnn.questionapp.domain.Role
import com.swtecnn.questionapp.domain.model.SocialEventRole
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration

class SocialEventLocalRepositoryImpl(private val mPreferences: SharedPreferences): ISocialEventLocalRepository {

    companion object {
        private const val SOCIAL_EVENT_KEY = "social_event"
    }

    @Throws(Exception::class)
    override fun archiveSocialEvent(aSocialEventId: String, aRole: Role): Boolean {
        val socialEventRole = SocialEventRole(aSocialEventId, aRole.id)
        val json = Json(JsonConfiguration.Stable)
        val storedEvents = mPreferences.getStringSet(SOCIAL_EVENT_KEY, HashSet<String>())
        storedEvents!!.add(json.stringify(SocialEventRole.serializer(), socialEventRole))
        return mPreferences.edit().putStringSet(SOCIAL_EVENT_KEY, storedEvents).commit()
    }

    @Throws(Exception::class)
    override fun getArchivedSocialEvents(): Set<SocialEventRole> {
        val events = mPreferences.getStringSet(SOCIAL_EVENT_KEY, HashSet<String>())!!
        val json = Json(JsonConfiguration.Stable)
        val eventsSet = HashSet<SocialEventRole>()
        events.forEach {
            eventsSet.add(json.parse(SocialEventRole.serializer(), it))
        }
        return eventsSet
    }
}