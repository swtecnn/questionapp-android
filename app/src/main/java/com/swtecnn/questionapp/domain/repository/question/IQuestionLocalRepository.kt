package com.swtecnn.questionapp.domain.repository.question

interface IQuestionLocalRepository {
    fun addQuestion(aQuestionId: String): Boolean
    fun getQuestions(): Set<String>
    fun deleteQuestion(aQuestionId: String, isAdmin: Boolean): Boolean
    fun addVotedQuestion(aQuestionId: String): Boolean
    fun removeVotedQuestion(aQuestionId: String): Boolean
    fun getVotedQuestions(): Set<String>
}