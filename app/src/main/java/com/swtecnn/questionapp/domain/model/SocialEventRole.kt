package com.swtecnn.questionapp.domain.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SocialEventRole(@SerialName("socialEventId") val eventId: String, @SerialName("role") val role: Int)