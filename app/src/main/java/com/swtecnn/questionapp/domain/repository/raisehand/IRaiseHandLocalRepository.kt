package com.swtecnn.questionapp.domain.repository.raisehand

interface IRaiseHandLocalRepository {
    fun cacheRaiseHand(aRaiseHandRequestId: String)
    fun getCachedRaiseHand(): String
}