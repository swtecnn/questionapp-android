package com.swtecnn.questionapp.domain.routers.socialevent.info;

public interface SocialEventInfoRouter {
    void showSocialEventQuestions(String aSocialEventId);
}
