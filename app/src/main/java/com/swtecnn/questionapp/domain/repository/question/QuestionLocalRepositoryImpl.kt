package com.swtecnn.questionapp.domain.repository.question

import android.annotation.SuppressLint
import android.content.SharedPreferences

class QuestionLocalRepositoryImpl(private val mPreferences: SharedPreferences) : IQuestionLocalRepository {

    companion object {
        private const val QUESTIONS_KEY = "questions"
        private const val VOTED_QUESTIONS_KEY = "voted_questions"
    }

    @SuppressLint("ApplySharedPref")
    override fun addQuestion(aQuestionId: String): Boolean {
        val storedQuestions = mPreferences.getStringSet(QUESTIONS_KEY, HashSet<String>())
        storedQuestions!!.add(aQuestionId)
        return mPreferences.edit().putStringSet(QUESTIONS_KEY, storedQuestions).commit()
    }

    override fun deleteQuestion(aQuestionId: String, isAdmin: Boolean): Boolean {
        val storedQuestions = mPreferences.getStringSet(QUESTIONS_KEY, HashSet<String>())
        var result = storedQuestions!!.remove(aQuestionId)
        if (result) {
            result = mPreferences.edit().putStringSet(QUESTIONS_KEY, storedQuestions).commit()
        }
        return result || isAdmin
    }

    override fun getQuestions(): Set<String> {
        return mPreferences.getStringSet(QUESTIONS_KEY, HashSet<String>())!!
    }

    override fun addVotedQuestion(aQuestionId: String): Boolean {
        val storedQuestions = mPreferences.getStringSet(VOTED_QUESTIONS_KEY, HashSet<String>())
        storedQuestions!!.add(aQuestionId)
        return mPreferences.edit().putStringSet(VOTED_QUESTIONS_KEY, storedQuestions).commit()
    }

    override fun getVotedQuestions(): Set<String> {
        return mPreferences.getStringSet(VOTED_QUESTIONS_KEY, HashSet<String>())!!
    }

    override fun removeVotedQuestion(aQuestionId: String): Boolean {
        val storedQuestions = mPreferences.getStringSet(VOTED_QUESTIONS_KEY, HashSet<String>())
        storedQuestions!!.remove(aQuestionId)
        return mPreferences.edit().putStringSet(VOTED_QUESTIONS_KEY, storedQuestions).commit()
    }
}