package com.swtecnn.questionapp.domain.interactors.dashboard

import com.swtecnn.questionapp.domain.model.SocialEvent
import com.swtecnn.questionapp.domain.model.SocialEventRole
import java.lang.Exception

interface DashboardInteractor {
    fun setCallback(aCallback: Callback)

    interface Callback {
        fun onArchivedEventsReceived(aSocialEvents: Set<SocialEventRole>)
        fun onFailedToGetArchivedEvents(aException: Exception)
        fun onAllEventsReceived(aSocialEvents: List<SocialEvent>)
    }

    fun obtainArchivedSocialEvents()
    fun obtainAllSocialEvents()
}