package com.swtecnn.questionapp.domain.interactors.socialevent.info

import com.swtecnn.questionapp.domain.executor.Executor
import com.swtecnn.questionapp.domain.executor.MainThread
import com.swtecnn.questionapp.domain.interactors.base.AbstractInteractor
import com.swtecnn.questionapp.domain.model.SocialEvent
import com.swtecnn.questionapp.domain.repository.socialevent.ISocialEventRepository
import com.swtecnn.questionapp.domain.repository.user.UserPreferenceRepository

class SocialEventInfoInteractorImpl(aExecutor: Executor,
                                    aMainThread: MainThread,
                                    private val mUserPreferenceRepository: UserPreferenceRepository,
                                    private val mSocialEventRepository: ISocialEventRepository) :
        AbstractInteractor(aExecutor, aMainThread), SocialEventInfoInteractor {

    private lateinit var mCallback: SocialEventInfoInteractor.Callback

    override fun setCallback(aCallback: SocialEventInfoInteractor.Callback) {
        mCallback = aCallback
    }

    override fun obtainUserRole() {
        threadExecutor.execute {
            val role = mUserPreferenceRepository.userRole
            mainThread.post {
                mCallback.onRoleReceived(role)
            }
        }
    }

    override fun addSocialEvent(aSocialEvent: SocialEvent) {
        threadExecutor.execute {
            val socialEventId = mSocialEventRepository.addSocialEvent(aSocialEvent)
            mainThread.post {
                if (socialEventId.isNullOrEmpty()) {
                    mCallback.onSocialEventAdded(socialEventId)
                } else {
                    mCallback.onSocialEventAdded(socialEventId)
                }
            }
        }
    }

    override fun obtainSocialEvent(aSocialEventId: String) {
        threadExecutor.execute {
            val socialEvent = mSocialEventRepository.getSocialEvent(aSocialEventId)
            mainThread.post {
                if (socialEvent != null) {
                    mCallback.onSocialEventReceived(socialEvent)
                } else {
                    mCallback.onFailedToObtainSocialEvent()
                }
            }
        }
    }

}