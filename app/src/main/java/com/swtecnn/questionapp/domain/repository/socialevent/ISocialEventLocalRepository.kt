package com.swtecnn.questionapp.domain.repository.socialevent

import com.swtecnn.questionapp.domain.Role
import com.swtecnn.questionapp.domain.model.SocialEventRole

interface ISocialEventLocalRepository {
    fun archiveSocialEvent(aSocialEventId: String, aRole: Role): Boolean
    fun getArchivedSocialEvents(): Set<SocialEventRole>
}