package com.swtecnn.questionapp.domain.model;

import com.swtecnn.questionapp.domain.model.base.BaseModel;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import static com.swtecnn.questionapp.domain.repository.FirebaseRepositoriesHelper.ARCHIVED;

public class Question implements BaseModel, Serializable, Comparable<Question> {

    public static String SOCIAL_EVENT_ID = "socialEventId";
    private static String DESCRIPTION = "description";
    private static String NAME = "name";
    private static String SLIDE_NUMBER = "slideNum";
    private static String VOTES = "votes";


    public String id;
    public String socialEventId;
    public String description;
    public String name;
    public Integer slideNum;
    public boolean archived;
    public Integer votes;

    public Question() {

    }

    public Question(String socialEventId, String description, String name, Integer slideNum, boolean archived, Integer votes) {
        this.socialEventId = socialEventId;
        this.description = description;
        this.name = name;
        this.slideNum = slideNum;
        this.archived = archived;
        this.votes = votes;
    }

    public Question(String socialEventId, String body, String name, Integer slideNum) {
        this(socialEventId, body, name, slideNum, false, 0);
    }

    @Override
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put(SOCIAL_EVENT_ID, socialEventId);
        result.put(DESCRIPTION, description);
        result.put(NAME, name);
        result.put(SLIDE_NUMBER, slideNum);
        result.put(ARCHIVED, archived);
        result.put(VOTES, votes);

        return result;
    }

    @Override
    public BaseModel setId(String id) {
        this.id = id;
        return this;
    }

    @Override
    public int compareTo(Question aQuestion) {
        return aQuestion.votes.compareTo(votes);
    }
}
