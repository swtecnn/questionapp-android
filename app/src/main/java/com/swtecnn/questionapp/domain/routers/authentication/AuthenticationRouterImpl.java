package com.swtecnn.questionapp.domain.routers.authentication;

import android.content.Context;
import android.content.Intent;

import com.swtecnn.questionapp.ui.barcode.BarcodeActivity_;
import com.swtecnn.questionapp.ui.pincode.EnterPinCodeActivity_;
import com.swtecnn.questionapp.ui.socialevent.SocialEventAdminActivity;
import com.swtecnn.questionapp.ui.socialevent.SocialEventClientActivity;
import com.swtecnn.questionapp.ui.socialevent.info.SocialEventInfoActivity_;

public class AuthenticationRouterImpl implements AuthenticationRouter {

    private Context mContext;

    public AuthenticationRouterImpl(Context context) {
        mContext = context;
    }

    @Override
    public void startListener(String aSocialEventId) {
        Intent intent = new Intent(mContext, SocialEventClientActivity.class);
        intent.putExtra(SocialEventClientActivity.SOCIAL_EVENT_ID_EXTRA, aSocialEventId);
        mContext.startActivity(intent);
    }

    @Override
    public void startLecturer(String aSocialEventId) {
        Intent intent = new Intent(mContext, SocialEventAdminActivity.class);
        intent.putExtra(SocialEventClientActivity.SOCIAL_EVENT_ID_EXTRA, aSocialEventId);
        mContext.startActivity(intent);
    }

    @Override
    public void showEventInfo(String aSocialEventId) {
        SocialEventInfoActivity_.intent(mContext)
                .mSocialEventId(aSocialEventId)
                .start();
    }

    @Override
    public void enterLecturerPin() {
        EnterPinCodeActivity_.intent(mContext).startForResult(ENTER_PIN_CODE_REQUEST_CODE);
    }

    @Override
    public void startBarcodeScanner() {
        BarcodeActivity_.intent(mContext).startForResult(BARCODE_SCANNER_REQUEST_CODE);
    }
}
