package com.swtecnn.questionapp.domain.interactors.socialevent.info

import com.swtecnn.questionapp.domain.Role
import com.swtecnn.questionapp.domain.model.SocialEvent

interface SocialEventInfoInteractor {
    fun setCallback(aCallback: Callback)

    fun addSocialEvent(aSocialEvent: SocialEvent)
    fun obtainSocialEvent(aSocialEventId: String)
    fun obtainUserRole()

    interface Callback {
        fun onRoleReceived(aRole: Role)
        fun onSocialEventReceived(aSocialEvent: SocialEvent)
        fun onFailedToObtainSocialEvent()
        fun onSocialEventAdded(aSocialEventId: String)
        fun onSocialEventAddFailed(aSocialEventId: String)
    }
}