package com.swtecnn.questionapp.domain.routers.socialevent.info;

import android.content.Context;

import com.swtecnn.questionapp.domain.Role;
import com.swtecnn.questionapp.ui.questions.QuestionListActivity_;

public class SocialEventInfoRouterImpl implements SocialEventInfoRouter {

    private Context mContext;

    public SocialEventInfoRouterImpl(Context context) {
        mContext = context;
    }

    @Override
    public void showSocialEventQuestions(String aSocialEventId) {
        QuestionListActivity_.intent(mContext)
                .mSocialEventId(aSocialEventId)
                .mRole(Role.VIEWER)
                .start();
    }
}
