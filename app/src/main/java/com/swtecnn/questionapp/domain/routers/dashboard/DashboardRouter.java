package com.swtecnn.questionapp.domain.routers.dashboard;

import com.swtecnn.questionapp.domain.model.SocialEvent;

import java.util.List;

public interface DashboardRouter {
    int ALL_LECTIONS_REQUEST_CODE = 2004;

    void showSocialEventDetails(String aSocialEventId);
    void showClientScreen(String aSocialEventId);
    void showUserSocialEvents(List<SocialEvent> aSocialEventList);
}
