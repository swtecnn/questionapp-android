package com.swtecnn.questionapp.domain.routers.socialevent.client;

import com.swtecnn.questionapp.domain.model.RaiseHandRequestStatus;

public interface SocialEventClientRouter {
    int ASK_QUESTION_REQUEST_CODE = 3003;
    int RAISE_HAND_REQUEST_CODE = 3001;
    int RESULT_CODE_WITHOUT_RESULT = 102;
    int RESULT_CODE_DELETE_RAISE_HAND_REQUEST = 3004;
    String QUESTION_EXTRA = "question";
    String RAISE_HAND_REQUEST_ID_EXTRA = "raiseHandRequestId";

    void startAskQuestion(String name, String aSocialEventId);
    void showEventInfo(String aSocialEventId);
    void showAllQuestions(String aSocialInfoId);
    void showRaiseHandRequest(RaiseHandRequestStatus.State aRaiseHandStatus, String aRaiseHandOrder,
                              String aRaiseHandRequestId);
}
