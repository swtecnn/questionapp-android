package com.swtecnn.questionapp.domain.routers.socialevent.client;

import android.content.Context;

import com.swtecnn.questionapp.domain.Role;
import com.swtecnn.questionapp.domain.model.RaiseHandRequestStatus;
import com.swtecnn.questionapp.ui.askquestion.AskQuestionActivity_;
import com.swtecnn.questionapp.ui.questions.QuestionListActivity_;
import com.swtecnn.questionapp.ui.raisehand.RaiseHandActivity_;
import com.swtecnn.questionapp.ui.socialevent.info.SocialEventInfoActivity_;

public class SocialEventClientRouterImpl implements SocialEventClientRouter {

    private Context mContext;

    public SocialEventClientRouterImpl(Context context) {
        mContext = context;
    }

    @Override
    public void startAskQuestion(String name, String aSocialEventId) {
        AskQuestionActivity_.intent(mContext).mExistingName(name)
                .mSocialEventId(aSocialEventId).startForResult(ASK_QUESTION_REQUEST_CODE);
    }

    @Override
    public void showEventInfo(String aSocialEventId) {
        SocialEventInfoActivity_.intent(mContext)
                .mSocialEventId(aSocialEventId)
                .start();
    }

    @Override
    public void showAllQuestions(String aSocialInfoId) {
        QuestionListActivity_.intent(mContext)
                .mSocialEventId(aSocialInfoId)
                .mRole(Role.CLIENT)
                .start();
    }

    @Override
    public void showRaiseHandRequest(RaiseHandRequestStatus.State aRaiseHandStatus,
                                     String aRaiseHandOrder, String aRaiseHandRequestId) {
        RaiseHandActivity_.intent(mContext)
                .mRole(Role.CLIENT)
                .mQueueNumber(aRaiseHandOrder)
                .mRaiseHandStatus(aRaiseHandStatus)
                .mRaiseHandRequestId(aRaiseHandRequestId)
                .startForResult(RAISE_HAND_REQUEST_CODE);
    }
}
