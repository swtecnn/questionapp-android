package com.swtecnn.questionapp.domain.interactors.authentication

import com.swtecnn.questionapp.domain.Role
import com.swtecnn.questionapp.domain.model.SocialEvent

interface AuthenticationInteractor {
    fun setCallback(aCallback: Callback)

    interface Callback {
        fun onSocialEventReceived(aSocialEvent: SocialEvent)
        fun onUserNameReceived(aUserName: String)
        fun onUserNameSavedSuccess(aUserName: String)
        fun onFcmTokenReceived(aToken: String)
    }

    fun obtainSocialEvent(aSocialEventId: String)
    fun setRole(aRole: Role)
    fun saveUserName(aUserName: String)
    fun obtainUserName()
    fun saveFcmToken(aToken: String)
    fun obtainFcmToken()
}