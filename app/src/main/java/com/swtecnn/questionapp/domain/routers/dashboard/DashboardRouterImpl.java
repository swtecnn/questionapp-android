package com.swtecnn.questionapp.domain.routers.dashboard;

import android.content.Context;
import android.content.Intent;

import com.swtecnn.questionapp.domain.model.SocialEvent;
import com.swtecnn.questionapp.ui.lections.LectionsActivity_;
import com.swtecnn.questionapp.ui.socialevent.SocialEventClientActivity;
import com.swtecnn.questionapp.ui.socialevent.info.SocialEventInfoActivity_;

import java.util.ArrayList;
import java.util.List;

public class DashboardRouterImpl implements DashboardRouter {

    private Context mContext;

    public DashboardRouterImpl(Context context) {
        mContext = context;
    }

    @Override
    public void showSocialEventDetails(String aSocialEventId) {
        SocialEventInfoActivity_.intent(mContext)
                .mSocialEventId(aSocialEventId)
                .start();
    }

    @Override
    public void showClientScreen(String aSocialEventId) {
        Intent intent = new Intent(mContext, SocialEventClientActivity.class);
        intent.putExtra(SocialEventClientActivity.SOCIAL_EVENT_ID_EXTRA, aSocialEventId);
        mContext.startActivity(intent);
    }

    @Override
    public void showUserSocialEvents(List<SocialEvent> aSocialEventList) {
        LectionsActivity_.intent(mContext)
                .mSocialEvents(new ArrayList<>(aSocialEventList))
                .startForResult(ALL_LECTIONS_REQUEST_CODE);
    }
}
