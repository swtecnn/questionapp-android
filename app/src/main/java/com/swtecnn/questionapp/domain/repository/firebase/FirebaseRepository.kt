package com.swtecnn.questionapp.domain.repository.firebase

interface FirebaseRepository {
    fun setFcmToken(aToken: String)
    fun getFcmToken(): String
}