package com.swtecnn.questionapp.domain.repository.socialevent;

import com.swtecnn.questionapp.domain.model.SocialEvent;

import java.util.List;

public interface ISocialEventRepository {

    String addSocialEvent(SocialEvent event);

    boolean archiveSocialEvent(String id);

    SocialEvent getSocialEvent(String id);
    List<SocialEvent> getAllSocialEvents();

}
