package com.swtecnn.questionapp.domain

enum class Role(val id: Int) {
    ADMIN(1001), CLIENT(1002), VIEWER(1003);

    companion object {
        fun getRoleById(aRole: Int): Role {
            return when (aRole) {
                ADMIN.id -> ADMIN
                CLIENT.id -> CLIENT
                VIEWER.id -> VIEWER
                else -> VIEWER
            }
        }
    }
}