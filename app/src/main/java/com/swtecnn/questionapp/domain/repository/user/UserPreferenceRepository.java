package com.swtecnn.questionapp.domain.repository.user;

import com.swtecnn.questionapp.domain.Role;

public interface UserPreferenceRepository {
    String getUserName();
    void setUserName(String aValue);
    Role getUserRole();
    void setUserRole(Role aRole);
}
