package com.swtecnn.questionapp.domain.repository.user

import android.content.SharedPreferences
import android.content.res.Resources

import com.swtecnn.questionapp.R
import com.swtecnn.questionapp.domain.Role
import io.reactivex.Single
import io.reactivex.SingleEmitter
import io.reactivex.SingleOnSubscribe

class UserPreferenceRepositoryImpl(private val mPreferences: SharedPreferences,
                                   private val mResources: Resources) : UserPreferenceRepository {

    override fun getUserName(): String? {
        return mPreferences.getString(USER_NAME_KEY, mResources.getString(R.string.incognito))
    }

    override fun setUserName(aValue: String) {
        mPreferences.edit().putString(USER_NAME_KEY, aValue).commit()
    }

    override fun getUserRole(): Role {
        return Role.getRoleById(mPreferences.getInt(USER_ROLE_KEY, -1))
    }

    override fun setUserRole(aValue: Role) {
        mPreferences.edit().putInt(USER_ROLE_KEY, aValue.id).commit()
    }

    companion object {

        private const val USER_NAME_KEY = "user_name"
        private const val USER_ROLE_KEY = "user_role"
    }
}
