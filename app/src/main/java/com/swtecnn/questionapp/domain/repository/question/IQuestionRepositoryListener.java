package com.swtecnn.questionapp.domain.repository.question;

public interface IQuestionRepositoryListener {
    void questionsCountChanged(String socialEventId, int count);
}
