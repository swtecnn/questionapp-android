package com.swtecnn.questionapp.domain.repository.question;

import com.swtecnn.questionapp.domain.model.Question;
import com.swtecnn.questionapp.domain.model.QuestionVote;

import java.util.List;

public interface IQuestionRepository {
    String addQuestion(Question question);
    boolean updateVoteQuestion(QuestionVote vote);
    boolean deleteQuestion(String aQuestionId);
    boolean archiveQuestion(String questionId);
    List<Question> getQuestions(String socialEventId);
    void addListener(IQuestionRepositoryListener listener, String socialEventId);
    void removeListener(IQuestionRepositoryListener listener);
}
