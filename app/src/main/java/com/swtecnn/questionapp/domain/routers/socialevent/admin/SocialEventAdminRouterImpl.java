package com.swtecnn.questionapp.domain.routers.socialevent.admin;

import android.content.Context;

import com.swtecnn.questionapp.domain.Role;
import com.swtecnn.questionapp.ui.questions.QuestionListActivity_;
import com.swtecnn.questionapp.ui.socialevent.info.SocialEventInfoActivity_;

public class SocialEventAdminRouterImpl implements SocialEventAdminRouter {

    private Context mContext;

    public SocialEventAdminRouterImpl(Context context) {
        mContext = context;
    }

    @Override
    public void showLectureInformation(String aSocialEventId) {
        SocialEventInfoActivity_.intent(mContext)
                .mSocialEventId(aSocialEventId)
                .start();
    }

    @Override
    public void showAllQuestions(String aSocialInfoId) {
        QuestionListActivity_.intent(mContext)
                .mSocialEventId(aSocialInfoId)
                .mRole(Role.ADMIN)
                .start();
    }
}
