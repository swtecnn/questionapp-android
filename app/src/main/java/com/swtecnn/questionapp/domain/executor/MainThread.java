package com.swtecnn.questionapp.domain.executor;

public interface MainThread {

    void post(final Runnable runnable);
}
