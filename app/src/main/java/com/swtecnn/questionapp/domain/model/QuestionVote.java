package com.swtecnn.questionapp.domain.model;

public class QuestionVote {
    public String questionId;
    public Integer value;

    public QuestionVote(String questionId, Integer value) {
        this.questionId = questionId;
        this.value = value;
    }
}
