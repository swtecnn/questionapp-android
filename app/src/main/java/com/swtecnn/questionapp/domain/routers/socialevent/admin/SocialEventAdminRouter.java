package com.swtecnn.questionapp.domain.routers.socialevent.admin;

public interface SocialEventAdminRouter {
    void showLectureInformation(String aSocialEventId);
    void showAllQuestions(String aSocialInfoId);
}
