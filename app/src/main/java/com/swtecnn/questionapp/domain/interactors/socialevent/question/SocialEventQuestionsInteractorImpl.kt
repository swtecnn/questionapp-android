package com.swtecnn.questionapp.domain.interactors.socialevent.question

import com.swtecnn.questionapp.domain.executor.Executor
import com.swtecnn.questionapp.domain.executor.MainThread
import com.swtecnn.questionapp.domain.interactors.base.AbstractInteractor
import com.swtecnn.questionapp.domain.model.QuestionVote
import com.swtecnn.questionapp.domain.repository.question.IQuestionLocalRepository
import com.swtecnn.questionapp.domain.repository.question.IQuestionRepository
import com.swtecnn.questionapp.domain.repository.user.UserPreferenceRepository

class SocialEventQuestionsInteractorImpl(aExecutor: Executor,
                                         aMainThread: MainThread,
                                         private val mQuestionsRepository: IQuestionRepository,
                                         private val mQuestionsLocalRepository: IQuestionLocalRepository,
                                         private val mUserRepository: UserPreferenceRepository) :
        AbstractInteractor(aExecutor, aMainThread), SocialEventQuestionsInteractor {

    private lateinit var mCallback: SocialEventQuestionsInteractor.Callback

    override fun setCallback(aCallback: SocialEventQuestionsInteractor.Callback) {
        mCallback = aCallback
    }

    override fun fetchQuestions(aSocialEventId: String) {
        threadExecutor.execute {
            val questions = mQuestionsRepository.getQuestions(aSocialEventId)
            mainThread.post { mCallback.onQuestionsReceivedRemote(questions) }
        }
    }

    override fun obtainQuestions() {
        threadExecutor.execute {
            val questions = mQuestionsLocalRepository.getQuestions()
            mainThread.post { mCallback.onQuestionsReceivedLocal(questions) }
        }
    }

    override fun obtainVotedQuestions() {
        threadExecutor.execute {
            val questions = mQuestionsLocalRepository.getVotedQuestions()
            mainThread.post { mCallback.onUserVotesReceived(questions) }
        }
    }

    override fun updateVoteQuestion(aVote: QuestionVote) {
        threadExecutor.execute {
            val success = mQuestionsRepository.updateVoteQuestion(aVote)
            mainThread.post {
                if (success) {
                    if (aVote.value > 0) {
                        mCallback.onQuestionVotedRemote(aVote.questionId)
                    } else {
                        mCallback.onQuestionUnVotedRemote(aVote.questionId)
                    }
                } else {
                    mCallback.onFailedToVoteQuestion(aVote.questionId)
                }
            }
        }
    }

    override fun updateVoteQuestionLocal(aVote: QuestionVote) {
        threadExecutor.execute {
            val success = mQuestionsLocalRepository.addVotedQuestion(aVote.questionId)
            mainThread.post {
                if (success) {
                    mCallback.onQuestionVotedSuccessfully(aVote.questionId)
                } else {
                    mCallback.onFailedToVoteQuestion(aVote.questionId)
                }
            }
        }
    }

    override fun removeVoteQuestionLocal(aVoteId: String) {
        threadExecutor.execute {
            val success = mQuestionsLocalRepository.removeVotedQuestion(aVoteId)
        }
    }

    override fun deleteQuestion(aQuestionId: String) {
        threadExecutor.execute {
            val success = mQuestionsRepository.deleteQuestion(aQuestionId)
            mainThread.post {
                if (success) {
                    mCallback.onQuestionDeletedSuccessfully(aQuestionId)
                } else {
                    mCallback.onFailedToDeleteQuestion(aQuestionId)
                }
            }
        }
    }

    override fun deleteQuestionLocal(aQuestionId: String, isAdmin: Boolean) {
        threadExecutor.execute {
            val success = mQuestionsLocalRepository.deleteQuestion(aQuestionId, isAdmin)
            mainThread.post {
                if (success) {
                    mCallback.onQuestionDeletedLocal(aQuestionId)
                } else {
                    mCallback.onFailedToDeleteQuestion(aQuestionId)
                }
            }
        }
    }

    override fun archiveQuestion(aQuestionId: String) {
        threadExecutor.execute {
            val success = mQuestionsRepository.archiveQuestion(aQuestionId)
            mainThread.post {
                if (success) {
                    mCallback.onQuestionArchivedSuccessfully(aQuestionId)
                } else {
                    mCallback.onFailedToArchiveQuestion(aQuestionId)
                }
            }
        }
    }

}