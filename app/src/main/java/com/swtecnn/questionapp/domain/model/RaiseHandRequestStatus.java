package com.swtecnn.questionapp.domain.model;

public class RaiseHandRequestStatus {

    public String raiseHandRequestId;
    public Integer order;
    public State status;

    public enum State {
        IN_QUEUE,
        ACTIVE,
        ARCHIVED
    }

    public RaiseHandRequestStatus(String raiseHandRequestId, Integer order, State status) {
        this.raiseHandRequestId = raiseHandRequestId;
        this.order = order;
        this.status = status;
    }

}
