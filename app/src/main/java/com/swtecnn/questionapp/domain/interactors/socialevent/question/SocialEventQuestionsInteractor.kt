package com.swtecnn.questionapp.domain.interactors.socialevent.question

import com.swtecnn.questionapp.domain.model.Question
import com.swtecnn.questionapp.domain.model.QuestionVote

interface SocialEventQuestionsInteractor {
    fun setCallback(aCallback: Callback)

    fun fetchQuestions(aSocialEventId: String)
    fun updateVoteQuestion(aVote: QuestionVote)
    fun updateVoteQuestionLocal(aVote: QuestionVote)
    fun removeVoteQuestionLocal(aVoteId: String)
    fun deleteQuestion(aQuestionId: String)
    fun deleteQuestionLocal(aQuestionId: String, isAdmin: Boolean)
    fun archiveQuestion(aQuestionId: String)
    fun obtainQuestions()
    fun obtainVotedQuestions()

    interface Callback {
        fun onQuestionsReceivedRemote(aQuestions: List<Question>)
        fun onQuestionsReceivedLocal(aQuestions: Set<String>)
        fun onUserVotesReceived(aUserVotes: Set<String>)
        fun onQuestionDeletedSuccessfully(aQuestionId: String)
        fun onQuestionDeletedLocal(aQuestionId: String)
        fun onFailedToDeleteQuestion(aQuestionId: String)
        fun onQuestionVotedRemote(aQuestionId: String)
        fun onQuestionUnVotedRemote(aQuestionId: String)
        fun onQuestionVotedSuccessfully(aQuestionId: String)
        fun onFailedToVoteQuestion(aQuestionId: String)
        fun onQuestionArchivedSuccessfully(aQuestionId: String)
        fun onFailedToArchiveQuestion(aQuestionId: String)
    }
}