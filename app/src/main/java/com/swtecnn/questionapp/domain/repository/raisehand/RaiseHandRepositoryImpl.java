package com.swtecnn.questionapp.domain.repository.raisehand;

import android.content.res.Resources;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.FirebaseFunctionsException;
import com.swtecnn.questionapp.R;
import com.swtecnn.questionapp.domain.model.RaiseHandRequest;
import com.swtecnn.questionapp.domain.model.RaiseHandRequestStatus;
import com.swtecnn.questionapp.domain.model.RaiseHandRequestStatus.State;
import com.swtecnn.questionapp.domain.repository.FirebaseRepositoriesHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import timber.log.Timber;

public class RaiseHandRepositoryImpl implements IRaiseHandRepository {

    private static final String TAG = RaiseHandRepositoryImpl.class.getSimpleName();

    private static final String RAISE_HAND_REQUEST_TABLE = "raise_hand_requests";
    private static final String REQUEST_ID = "raiseHandRequestId";

    private DatabaseReference myRef;
    private Map<IRaiseHandRepositoryListener, ValueEventListener> listenersMap;
    private FirebaseFunctions mFunctions;
    private Resources mResources;

    public RaiseHandRepositoryImpl(Resources aResources) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        listenersMap = new HashMap<>();
        myRef = database.getReference().child(RAISE_HAND_REQUEST_TABLE);
        mFunctions = FirebaseFunctions.getInstance();
        mResources = aResources;
    }

    @Override
    public String addRaiseHandRequest(RaiseHandRequest request) {
        return FirebaseRepositoriesHelper.addObjectToDatabase(myRef, request).blockingGet();
    }

    @Override
    public boolean deleteRaiseHandRequest(String raiseHandRequestId) {
        final Object result = FirebaseRepositoriesHelper
                .deleteObjectFromDatabase(myRef, raiseHandRequestId).blockingGet();
        return !(result instanceof Exception);
    }

    @Override
    public Integer getRaiseHandRequestCount(String socialEventId) {
        return FirebaseRepositoriesHelper.getChildrenCount(myRef, socialEventId, RaiseHandRequest.SOCIAL_EVENT_ID).blockingGet();
    }

    private Single<RaiseHandRequest> getRaiseHandRequest(String raiseHandRequestId) {
        return FirebaseRepositoriesHelper.getSingleData(myRef, raiseHandRequestId)
                .map(dataSnapshot -> {
                    final RaiseHandRequest request = dataSnapshot.getValue(RaiseHandRequest.class);
                    if (request == null) {
                        // request was deleted, so we pass dummy request to avoid null passing
                        return new RaiseHandRequest(raiseHandRequestId, null, null, 0L);
                    } else {
                        return request;
                    }
                });
    }

    @Override
    public RaiseHandRequestStatus getRaiseHandRequestStatus(String raiseHandRequestId) {
        return getRaiseHandRequest(raiseHandRequestId)
                .flatMap(request -> FirebaseRepositoriesHelper
                        .getListData(myRef, request.socialEventId, RaiseHandRequest.SOCIAL_EVENT_ID))
                .map(iterator -> {
                    List<RaiseHandRequest> requests = new ArrayList<>();
                    int position = -1;

                    for (DataSnapshot snapshot : iterator) {
                        requests.add(snapshot.getValue(RaiseHandRequest.class).setId(snapshot.getKey()));
                    }

                    if (!requests.isEmpty()) {
                        Collections.sort(requests);
                        for (int i = 0; i < requests.size(); i++) {
                            if (requests.get(i).id.equals(raiseHandRequestId)) {
                                position = i;
                                break;
                            }
                        }
                    }

                    State status = position == 0 ? State.ACTIVE :
                            position == -1 ? State.ARCHIVED : State.IN_QUEUE;
                    return new RaiseHandRequestStatus(raiseHandRequestId, ++position, status);
                }).blockingGet();
    }

    @Override
    public List<RaiseHandRequest> getRaiseHandRequests(String socialEventId) {
        return FirebaseRepositoriesHelper.getListData(myRef, socialEventId, RaiseHandRequest.SOCIAL_EVENT_ID)
                .map(snapshots -> {
                    final List<RaiseHandRequest> requests = new ArrayList<>();
                    for (final DataSnapshot snapshot : snapshots) {
                        if (snapshot != null) {
                            RaiseHandRequest request = snapshot.getValue(RaiseHandRequest.class);
                            if (request != null) {
                                requests.add(request.setId(snapshot.getKey()));
                            }
                        }
                    }
                    return requests;
                })
                .blockingGet();
    }

    @Override
    public void addListener(IRaiseHandRepositoryListener listener, String socialEventId) {
        Query query = myRef.orderByChild(RaiseHandRequest.SOCIAL_EVENT_ID).equalTo(socialEventId);
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listener.requestCountChanged(socialEventId, (int) dataSnapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Timber.w(databaseError.toException(), "getListData Failed to read value.");
            }
        };
        listenersMap.put(listener, valueEventListener);
        query.addValueEventListener(valueEventListener);
    }

    @Override
    public void removeListener(IRaiseHandRepositoryListener listener) {
        if (listenersMap.containsKey(listener)) {
            final ValueEventListener eventListener = listenersMap.get(listener);
            if (eventListener != null) {
                myRef.removeEventListener(eventListener);
            }
            listenersMap.remove(listener);
        }
    }

    @Override
    public boolean sendPush(String aToken, String aListenerName) {
        SingleOnSubscribe<Boolean> onSubscribe = e -> {
            Map<String, Object> data = new HashMap<>();
            data.put("token", aToken);
            data.put("title", mResources.getString(R.string.raise_hand_push_title));
            data.put("message", mResources.getString(R.string.raise_hand_push_message, aListenerName));
            mFunctions
                    .getHttpsCallable("sendQuestion")
                    .call(data)
                    .addOnCompleteListener(task -> {
                        if (!task.isSuccessful()) {
                            Exception ex = task.getException();
                            if (ex instanceof FirebaseFunctionsException) {
                                FirebaseFunctionsException ffe = (FirebaseFunctionsException) ex;
                                FirebaseFunctionsException.Code code = ffe.getCode();
                                Object details = ffe.getDetails();
                            }
                            Timber.e(ex, "Failed to send push: %s", ex.getMessage());
                            e.onSuccess(false);
                        } else {
                            e.onSuccess(true);
                            Timber.d("Sent push successfully");
                        }
                    });
        };
        return Single.create(onSubscribe).blockingGet();
    }
}
