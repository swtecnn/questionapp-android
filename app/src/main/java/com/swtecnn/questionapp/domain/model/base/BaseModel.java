package com.swtecnn.questionapp.domain.model.base;

import java.util.Map;

public interface BaseModel {
    Map<String, Object> toMap();
    BaseModel setId(String id);
}
