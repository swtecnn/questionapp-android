package com.swtecnn.questionapp.domain.model;

import com.swtecnn.questionapp.domain.model.base.BaseModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.swtecnn.questionapp.domain.repository.FirebaseRepositoriesHelper.ARCHIVED;

public class SocialEvent implements BaseModel, Serializable {

    private static String TITLE = "title";
    private static String LECTOR = "lector";
    private static String DESCRIPTION = "description";
    private static String AGENDA = "agenda";
    private static String PASSWORD = "role";
    private static String DATE = "date";
    private static String LINKS = "usefulLinks";

    public String id;
    public String title;
    public String lector;
    public String description;
    public String agenda;
    public String password;
    public Long date;
    public List<String> usefulLinks;
    public boolean archived;
    private int role; //internal field

    public SocialEvent() {

    }

    public SocialEvent(String title, String lector, String description, String agenda,
                       String password, Long date, List<String> usefulLinks, boolean archived) {

        this.title = title;
        this.lector = lector;
        this.description = description;
        this.agenda = agenda;
        this.password = password;
        this.date = date;
        this.usefulLinks = usefulLinks;
        this.archived = archived;
    }

    public SocialEvent(String title, String lector, String description, String agenda,
                       String password, Long date, List<String> usefulLinks) {
        this(title, lector, description, agenda, password, date, usefulLinks, false);
    }

    public SocialEvent(String title, String lector, String description, String agenda,
                       String password, Long date, Boolean archived) {
        this(title, lector, description, agenda, password, date, new ArrayList<>(), archived);
    }

    public SocialEvent(SocialEvent other, int role) {
        this.id = other.id;
        this.title = other.title;
        this.lector = other.lector;
        this.description = other.description;
        this.agenda = other.agenda;
        this.password = other.password;
        this.date = other.date;
        this.usefulLinks = other.usefulLinks;
        this.archived = other.archived;
        this.role = role;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public Map<String, Object> toMap() {

        HashMap<String, Object> result = new HashMap<>();
        result.put(TITLE, title);
        result.put(LECTOR, lector);
        result.put(DESCRIPTION, description);
        result.put(AGENDA, agenda);
        result.put(PASSWORD, password);
        result.put(DATE, date);
        result.put(LINKS, usefulLinks);
        result.put(ARCHIVED, archived);

        return result;
    }

    @Override
    public SocialEvent setId(String id) {
        this.id = id;
        return this;
    }
}
