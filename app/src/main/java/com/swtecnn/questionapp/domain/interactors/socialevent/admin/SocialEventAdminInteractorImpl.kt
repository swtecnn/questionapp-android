package com.swtecnn.questionapp.domain.interactors.socialevent.admin

import com.google.firebase.functions.FirebaseFunctions
import com.swtecnn.questionapp.domain.Role
import com.swtecnn.questionapp.domain.executor.Executor
import com.swtecnn.questionapp.domain.executor.MainThread
import com.swtecnn.questionapp.domain.interactors.base.AbstractInteractor
import com.swtecnn.questionapp.domain.model.RaiseHandRequest
import com.swtecnn.questionapp.domain.repository.question.IQuestionRepository
import com.swtecnn.questionapp.domain.repository.question.IQuestionRepositoryListener
import com.swtecnn.questionapp.domain.repository.raisehand.IRaiseHandRepository
import com.swtecnn.questionapp.domain.repository.raisehand.IRaiseHandRepositoryListener
import com.swtecnn.questionapp.domain.repository.socialevent.ISocialEventLocalRepository
import com.swtecnn.questionapp.domain.repository.socialevent.ISocialEventRepository
import timber.log.Timber
import java.lang.Exception

class SocialEventAdminInteractorImpl(aExecutor: Executor,
                                     aMainThread: MainThread,
                                     private val mSocialEventRepository: ISocialEventRepository,
                                     private val mQuestionRepository: IQuestionRepository,
                                     private val mRaiseHandRepository: IRaiseHandRepository,
                                     private val mSocialEventLocalRepository: ISocialEventLocalRepository) :
        AbstractInteractor(aExecutor, aMainThread), SocialEventAdminInteractor {

    private lateinit var mCallback: SocialEventAdminInteractor.Callback

    private val mFunctions: FirebaseFunctions = FirebaseFunctions.getInstance()

    override fun setCallback(aCallback: SocialEventAdminInteractor.Callback) {
        mCallback = aCallback
    }

    override fun archiveSocialEventRemote(aSocialEventId: String) {
        threadExecutor.execute {
            val result = mSocialEventRepository.archiveSocialEvent(aSocialEventId)
            mainThread.post {
                if (result) {
                    mCallback.onSocialEventArchivedRemote(aSocialEventId)
                } else {
                    mCallback.onSocialEventArchiveFailed(aSocialEventId)
                }
            }
        }
    }

    override fun archiveSocialEventLocal(aSocialEventId: String) {
        threadExecutor.execute {
            try {
                mSocialEventLocalRepository.archiveSocialEvent(aSocialEventId, Role.ADMIN)
            } catch (aException: Exception) {
                Timber.e(aException, "Failed to save social event locally: %s", aException.message)
            }
        }
    }

    override fun loadSocialEvent(socialEventId: String) {
        threadExecutor.execute {
            val event = mSocialEventRepository.getSocialEvent(socialEventId)
            mainThread.post {
                mCallback.onSocialEventLoaded(event)
            }
        }
    }

    override fun removeQuestionCountListener(listener: IQuestionRepositoryListener) {
        mQuestionRepository.removeListener(listener)
    }

    override fun addQuestionCountListener(listener: IQuestionRepositoryListener, socialEventId: String) {
        mQuestionRepository.addListener(listener, socialEventId)
    }

    override fun addRaiseHandCountListener(listener: IRaiseHandRepositoryListener, socialEventId: String) {
        mRaiseHandRepository.addListener(listener, socialEventId)
    }

    override fun removeRaiseHandListener(listener: IRaiseHandRepositoryListener) {
        mRaiseHandRepository.removeListener(listener)
    }

    override fun getRaiseHandRequests(socialEventId: String) {
        threadExecutor.execute {
            val requests =  mRaiseHandRepository.getRaiseHandRequests(socialEventId)
            mainThread.post { mCallback.onRaiseHandRequestsLoaded(requests) }
        }
    }

    override fun removeRaiseHandRequest(requestId: String) {
        threadExecutor.execute {
            val success = mRaiseHandRepository.deleteRaiseHandRequest(requestId)
            if (success) {
                mainThread.post { mCallback.onRaiseHandRequestDeleted() }
            } else {
                mainThread.post { mCallback.onRaiseHandRequestDeleteFailed() }
            }
        }
    }

    override fun sendPush(request: RaiseHandRequest) {
        threadExecutor.execute {
            val success = mRaiseHandRepository.sendPush(request.pushNotificationToken, request.name)
            if (success) {
                mainThread.post { mCallback.onPushSent() }
            } else {
                mainThread.post { mCallback.onPushFailed() }
            }
        }
    }
}