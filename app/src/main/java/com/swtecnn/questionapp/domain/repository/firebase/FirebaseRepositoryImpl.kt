package com.swtecnn.questionapp.domain.repository.firebase

import android.annotation.SuppressLint
import android.content.SharedPreferences

class FirebaseRepositoryImpl(private val mPreferences: SharedPreferences): FirebaseRepository {

    companion object{
        const val FCM_TOKEN_KEY = "fcm_token"
    }

    @SuppressLint("ApplySharedPref")
    override fun setFcmToken(aToken: String) {
        mPreferences.edit().putString(FCM_TOKEN_KEY, aToken).commit()
    }

    override fun getFcmToken(): String {
        return mPreferences.getString(FCM_TOKEN_KEY, "")!!
    }
}