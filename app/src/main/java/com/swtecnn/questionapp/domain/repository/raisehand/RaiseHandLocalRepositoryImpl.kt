package com.swtecnn.questionapp.domain.repository.raisehand

import android.annotation.SuppressLint
import android.content.SharedPreferences

class RaiseHandLocalRepositoryImpl(private val mPreferences: SharedPreferences): IRaiseHandLocalRepository {

    @SuppressLint("ApplySharedPref")
    override fun cacheRaiseHand(aRaiseHandRequestId: String) {
        mPreferences.edit().putString(RAISE_HAND_ID_KEY, aRaiseHandRequestId).commit()
    }

    override fun getCachedRaiseHand(): String {
        return mPreferences.getString(RAISE_HAND_ID_KEY, "")!!
    }

    companion object {

        private const val RAISE_HAND_ID_KEY = "raise_hand_id"
    }
}