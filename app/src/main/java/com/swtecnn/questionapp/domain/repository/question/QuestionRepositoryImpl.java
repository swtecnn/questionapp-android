package com.swtecnn.questionapp.domain.repository.question;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.swtecnn.questionapp.domain.model.Question;
import com.swtecnn.questionapp.domain.model.QuestionVote;
import com.swtecnn.questionapp.domain.repository.FirebaseRepositoriesHelper;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import timber.log.Timber;

public class QuestionRepositoryImpl implements IQuestionRepository {

    private static final String QUESTIONS_TABLE = "questions";

    private DatabaseReference myRef;
    private Map<IQuestionRepositoryListener, ValueEventListener> listenersMap;

    public QuestionRepositoryImpl() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference().child(QUESTIONS_TABLE);
        listenersMap = new HashMap<>();
    }

    @Override
    public String addQuestion(Question question) {
        return FirebaseRepositoriesHelper.addObjectToDatabase(myRef, question).blockingGet();
    }

    @Override
    public boolean updateVoteQuestion(QuestionVote vote) {
        final SingleOnSubscribe<Boolean> onSubscribe =
                emitter -> myRef.child(vote.questionId).runTransaction(new Transaction.Handler() {
                    @NotNull
                    @Override
                    public Transaction.Result doTransaction(@NotNull MutableData mutableData) {
                        Question question = mutableData.getValue(Question.class);
                        if (question == null) {
                            return Transaction.success(mutableData);
                        }

                        question.votes += vote.value;

                        // Set value and report transaction success
                        mutableData.setValue(question);
                        return Transaction.success(mutableData);
                    }

                    @Override
                    public void onComplete(DatabaseError databaseError, boolean b,
                                           DataSnapshot dataSnapshot) {
                        // Transaction completed
                        Timber.w("updateVoteQuestion completed databaseError = %s", databaseError);
                        emitter.onSuccess(b);
                    }
                });
        return Single.create(onSubscribe).blockingGet();
    }

    @Override
    public boolean deleteQuestion(String aQuestionId) {
        final Object result = FirebaseRepositoriesHelper
                .deleteObjectFromDatabase(myRef, aQuestionId).blockingGet();
        return !(result instanceof Exception);
    }

    @Override
    public boolean archiveQuestion(String questionId) {
        final Object result = FirebaseRepositoriesHelper
                .archiveObject(myRef, questionId).blockingGet();
        return !(result instanceof Exception);
    }

    @Override
    public List<Question> getQuestions(String socialEventId) {
        return FirebaseRepositoriesHelper.getListData(myRef, socialEventId, Question.SOCIAL_EVENT_ID)
                .map(iterable -> {
                    final List<Question> list = new ArrayList<>();
                    final List<Question> archived = new ArrayList<>();
                    for (DataSnapshot item : iterable) {
                        Question q = item.getValue(Question.class);
                        if (q != null) {
                            final Question question = (Question) q.setId(item.getKey());
                            if (q.archived) {
                                archived.add(question);
                            } else {
                                list.add(question);
                            }
                        }
                    }
                    Collections.sort(list);
                    list.addAll(archived);
                    return list;
                })
                .blockingGet();
    }

    @Override
    public void addListener(IQuestionRepositoryListener listener, String socialEventId) {
        Query query = myRef.orderByChild(Question.SOCIAL_EVENT_ID).equalTo(socialEventId);
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Timber.d("onDataChange + count = %s", dataSnapshot.getChildrenCount());
                listener.questionsCountChanged(socialEventId, (int) dataSnapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Timber.w(databaseError.toException(), "getListData Failed to read value.");
            }
        };
        listenersMap.put(listener, valueEventListener);
        query.addValueEventListener(valueEventListener);
    }

    @Override
    public void removeListener(IQuestionRepositoryListener listener) {
        if (listenersMap.containsKey(listener)) {
            final ValueEventListener eventListener = listenersMap.get(listener);
            if (eventListener != null) {
                myRef.removeEventListener(eventListener);
            }
            listenersMap.remove(listener);
        }
    }
}
