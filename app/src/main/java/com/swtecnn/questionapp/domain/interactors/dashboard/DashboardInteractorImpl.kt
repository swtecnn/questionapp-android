package com.swtecnn.questionapp.domain.interactors.dashboard

import com.swtecnn.questionapp.domain.executor.Executor
import com.swtecnn.questionapp.domain.executor.MainThread
import com.swtecnn.questionapp.domain.interactors.base.AbstractInteractor
import com.swtecnn.questionapp.domain.repository.socialevent.ISocialEventLocalRepository
import com.swtecnn.questionapp.domain.repository.socialevent.ISocialEventRepository

class DashboardInteractorImpl(aExecutor: Executor, aMainThread: MainThread,
                                   private val mSocialEventsLocalRepository: ISocialEventLocalRepository,
                                   private val mSocialEventsRepository: ISocialEventRepository)
    : AbstractInteractor(aExecutor, aMainThread), DashboardInteractor {

    private lateinit var mCallback: DashboardInteractor.Callback

    override fun setCallback(aCallback: DashboardInteractor.Callback) {
        mCallback = aCallback
    }

    override fun obtainArchivedSocialEvents() {
        threadExecutor.execute {
            try {
                val archivedLocalEvents = mSocialEventsLocalRepository.getArchivedSocialEvents()
                mainThread.post { mCallback.onArchivedEventsReceived(archivedLocalEvents) }
            } catch (aException: Exception) {
                mainThread.post { mCallback.onFailedToGetArchivedEvents(aException) }
            }
        }
    }

    override fun obtainAllSocialEvents() {
        threadExecutor.execute {
            val allLocalEvents = mSocialEventsRepository.allSocialEvents
            mainThread.post { mCallback.onAllEventsReceived(allLocalEvents) }
        }
    }
}