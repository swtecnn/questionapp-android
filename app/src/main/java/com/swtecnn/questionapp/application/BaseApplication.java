package com.swtecnn.questionapp.application;

import android.app.Application;
import android.util.Log;

import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;
import com.swtecnn.questionapp.BuildConfig;
import com.swtecnn.questionapp.di.component.ApplicationComponent;
import com.swtecnn.questionapp.di.component.DaggerApplicationComponent;
import com.swtecnn.questionapp.di.module.ApplicationModule;

import timber.log.Timber;

public class BaseApplication extends Application {
    private static final String TAG = BaseApplication.class.getName();

    private static volatile BaseApplication sApplication;

    private static ApplicationComponent sApplicationComponent;

    public static String getAppVersion() {
        return BuildConfig.VERSION_NAME + "(" + BuildConfig.VERSION_CODE + ")" + BuildConfig.BUILD_TYPE;
    }

    public static BaseApplication getInstance() {
        return sApplication;
    }

    public static ApplicationComponent getAppComponent() {
        return sApplicationComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sApplication = this;

        Timber.plant(new Timber.DebugTree());

        sApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();

        initAppCenter();
    }

    private void initAppCenter() {
        if (!BuildConfig.DEBUG) {
            AppCenter.start(this, BuildConfig.APPCENTER_APP_ID, Analytics.class, Crashes.class);
            Timber.d(TAG, "AppCenter has been initialized");
        }
    }

}
